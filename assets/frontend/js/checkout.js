/*$("#billing_email").change(function(){
    var email = $("#billing_email").val();
    $.ajax({
        url: "/check/username?username="+email,
        dataType: 'json',
        success: function(data){
            if(!data["isOk"]){ // utilisateur déjà utilisé
                if(checkPersonnalDatas(false)){
                    showPayBtns();
                }
                else{
                    hidePayBtns();
                }
            }
            else{
                if(checkPersonnalDatas(true)){
                    showPayBtns();
                }
                else{
                    hidePayBtns();
                }
            }
        },
        error: function(err){

        }
    });
});

$("#select-create-account").click(function(){
   if($(this).is(":checked")){
        var email = $("#billing_email").val();
        $.ajax({
            url: "/check/username?username="+email,
            dataType: 'json',
            success: function(data){
                if(!data["isOk"]){ // utilisateur déjà utilisé
                    if(checkPersonnalDatas(false)){
                        showPayBtns();
                    }
                    else{
                        hidePayBtns();
                    }
                }
                else{
                    if(checkPersonnalDatas(true)){
                        showPayBtns();
                    }
                    else{
                        hidePayBtns();
                    }
                }
            },
            error: function(err){

            }
        });
    }
    else{
        if(checkPersonnalDatas(null)){
            showPayBtns();
        }
        else{
            hidePayBtns();
        }
    }
});*/

var error_case = false;
function checkPersonnalDatas(emailOk){
    error_case = false;

    if($("#billing_lastname").val()==""){
        $('#billing_lastname').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_lastname").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_lastname').removeClass("error_custom_info").addClass("success_custom_info");
    }
    if($("#billing_firstname").val()==""){
        $('#billing_firstname').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_firstname").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_firstname').removeClass("error_custom_info").addClass("success_custom_info");
    }

    if($("#billing_country").val()==""){
        $('#billing_country').addClass("error_custom_info").removeClass("success_custom_info");
        error_case = true;
    }
    else{
        $('#billing_country').removeClass("error_custom_info").addClass("success_custom_info");
    }

    if($("#billing_address").val()==""){
        $('#billing_address').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_address").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_address').removeClass("error_custom_info").addClass("success_custom_info");
    }
    if($("#billing_city").val()==""){
        $('#billing_city').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_city").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_city').removeClass("error_custom_info").addClass("success_custom_info");
    }
    if($("#billing_npa").val()==""){
        $('#billing_npa').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_npa").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_npa').removeClass("error_custom_info").addClass("success_custom_info");
    }
    if($("#billing_phone").val()==""){
        $('#billing_phone').addClass("error_custom_info").removeClass("success_custom_info");
        //if(error_case==false)
        //    $("html, body").animate({ scrollTop: $("#billing_phone").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_phone').removeClass("error_custom_info").addClass("success_custom_info");
    }
    if(emailOk == null) {
        if ($("#billing_email").val() == "" || validateEmail($("#billing_email").val()) == false) {
            $('#billing_email').addClass("error_custom_info").removeClass("success_custom_info");
            //if(error_case==false)
            //    $("html, body").animate({ scrollTop: $("#billing_email").offset().top -50 }, 1);
            error_case = true;
        }
        else {
            $('#billing_email').removeClass("error_custom_info").addClass("success_custom_info");
        }
    }

    if(emailOk == false) {
        $('#billing_email').addClass("error_custom_info").removeClass("success_custom_info");
        error_case = true;
    }

    if(emailOk == true) {
        $('#billing_email').removeClass("error_custom_info").addClass("success_custom_info");
    }








    if($("#accept_cond_check").is(":checked")){
        $('#accept_cond_check').css("border: 1px solid red");
    }
    else{
        $('#accept_cond_check').css("border: 1px solid #ccc");
        error_case = true;
    }

    if($("#select-create-account").is(":checked")){
        // pwd 1
        if($("#billing_pwd_1").val()==""){
            $('#billing_pwd_1').addClass("error_custom_info").removeClass("success_custom_info");
            //   if(error_case==false)
            //        $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_pwd_1').removeClass("error_custom_info").addClass("success_custom_info");
        }
        // pwd 1
        if($("#billing_pwd_2").val()==""){
            $('#billing_pwd_2').addClass("error_custom_info").removeClass("success_custom_info");
            //    if(error_case==false)
            //        $("html, body").animate({ scrollTop: $("#billing_pwd_2").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_pwd_2').removeClass("error_custom_info").addClass("success_custom_info");
        }
        if($("#billing_pwd_2").val()!=$("#billing_pwd_1").val()){
            $('#billing_pwd_2').addClass("error_custom_info").removeClass("success_custom_info");
            $('#billing_pwd_1').addClass("error_custom_info").removeClass("success_custom_info");
            //    if(error_case==false)
            //       $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
            error_case = true;
        }
    }else{
        $('#billing_pwd_1').removeClass("error_custom_info");
        $('#billing_pwd_2').removeClass("error_custom_info");
    }


    if(error_case){
        return false;
    }
    else{
        return true;
    }

}





function showPayBtns(){
    $(".saferPay").css("margin-top", "105px");
    $("#iframe_sp").show();
    $(".payment-choices").show();
    $("#before_pay_label").hide();
}
function hidePayBtns(){
    $(".payment-choices").hide();
    $("#iframe_sp").hide();
    $("#before_pay_label").show();
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// WHEN document is ready
$(document).ready(function() {

    if($("#select-create-account").is(":checked")){
        $(".gc-create-account").show();
    }



    $(".form-control").on("change", function(){
        if(checkPersonnalDatas(null)){
            //showPayBtns();
        }
        else{
            //hidePayBtns();
        }
    });
    $("#accept_cond_check").on("change", function(){
        if(checkPersonnalDatas(null)){
            //showPayBtns();
        }
        else{
            //hidePayBtns();
        }
    });



    $("#btnPay").click(function(event){
        error_case = false;

        if($("#billing_lastname").val()==""){
            $('#billing_lastname').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_lastname").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_lastname').removeClass("error_custom_info");
        }
        if($("#billing_firstname").val()==""){
            $('#billing_firstname').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_firstname").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_firstname').removeClass("error_custom_info");
        }

        if($("#billing_country").val()==""){
            $('#billing_country').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_country").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_country').removeClass("error_custom_info");
        }

        if($("#billing_address").val()==""){
            $('#billing_address').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_address").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_address').removeClass("error_custom_info");
        }
        if($("#billing_city").val()==""){
            $('#billing_city').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_city").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_city').removeClass("error_custom_info");
        }
        if($("#billing_npa").val()==""){
            $('#billing_npa').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_npa").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_npa').removeClass("error_custom_info");
        }
        if($("#billing_phone").val()==""){
            $('#billing_phone').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_phone").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_phone').removeClass("error_custom_info");
        }

        if($("#billing_email").val()==""  || validateEmail($("#billing_email").val())==false){
            $('#billing_email').addClass("error_custom_info");
            if(error_case==false)
                $("html, body").animate({ scrollTop: $("#billing_email").offset().top -50 }, 1);
            error_case = true;
        }
        else{
            $('#billing_email').removeClass("error_custom_info");
        }

        /*if($("#name-on-card").val()==""){
         $('#name-on-card').addClass("error_custom_info");
         if(error_case==false)
         $("html, body").animate({ scrollTop: $("#name-on-card").offset().top -50 }, 1);
         error_case = true;
         }
         else{
         $('#name-on-card').removeClass("error_custom_info");
         }*/

        if($("#select-create-account").is(":checked")){
            if($("#billing_pwd_1").val()==""){
                $('#billing_pwd_1').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
                error_case = true;
            }
            else{
                $('#billing_pwd_1').removeClass("error_custom_info");
            }
            // pwd 1
            if($("#billing_pwd_2").val()==""){
                $('#billing_pwd_2').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_2").offset().top -50 }, 1);
                error_case = true;
            }
            else{
                $('#billing_pwd_2').removeClass("error_custom_info");
            }
            if($("#billing_pwd_2").val()!=$("#billing_pwd_1").val()){
                $('#billing_pwd_2').addClass("error_custom_info");
                $('#billing_pwd_1').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
                error_case = true;
            }
        }
        /*else{
            error_case = false;
        }*/

        if($("#accept_cond_check").is(":checked")){
            $('#accept_cond_check').removeClass("error_custom_info_check");
            //error_case = false;
        }
        else{
            $('#accept_cond_check').addClass("error_custom_info_check");
            error_case = true;
        }

        /*if($(".gc-create-account").is(":visible")){
            // pwd 1
            if($("#billing_pwd_1").val()==""){
                $('#billing_pwd_1').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
                error_case = true;
            }
            else{
                $('#billing_pwd_1').removeClass("error_custom_info");
            }
            // pwd 1
            if($("#billing_pwd_2").val()==""){
                $('#billing_pwd_2').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_2").offset().top -50 }, 1);
                error_case = true;
            }
            else{
                $('#billing_pwd_2').removeClass("error_custom_info");
            }
            if($("#billing_pwd_2").val()!=$("#billing_pwd_1").val()){
                $('#billing_pwd_2').addClass("error_custom_info");
                $('#billing_pwd_1').addClass("error_custom_info");
                if(error_case==false)
                    $("html, body").animate({ scrollTop: $("#billing_pwd_1").offset().top -50 }, 1);
                error_case = true;
            }
        }*/

        if(error_case){
            event.preventDefault();
            //return false;
        }else{
           return true;
        }

    });


    $(".select-product-complementary").on("change", function(){
        if($(".table_complementary_products").is(":hidden")){
            $(".table_complementary_products").show();
            $("#gc-panier-total-2").show();

        }
        var target = "#prod_comp_line_"+$(".select-product-complementary option:selected").val();
        $(target).show();

    });

    $(".prod_comp_nb").on("change", function(){
        var id  = $(this).data("id");
        var cost = $(this).data("cost");

        var total = cost * $(this).val();
        $("#prod_comp_total_"+id).html(parseFloat(total).toFixed(2) + "");
        $("#prod_comp_total_"+id).data("cost", total);

        $(".btnAddProductToOrder").show();
        // displayNewTotal();
    });

    $(".plusBtn").click(function(){
        var id  = $(this).data("id");
        var cost = $(this).data("cost");

        var target = "#prod_comp_nb_" + id;
        var value = $(target).val();

        var new_value = parseInt(value)+1;
        $(target).val(new_value);

        if(new_value<11){
            $(target).val(new_value);
            $(target).change();
        }
    });

    $(".minusBtn").click(function(){
        var id  = $(this).data("id");
        var cost = $(this).data("cost");

        var target = "#prod_comp_nb_" + id;
        var value = $(target).val();

        var new_value = parseInt(value)-1;
        if(new_value>-1){
            $(target).val(new_value);
            $(target).change();
        }

    });


    // WHEN language is changed
    $(".language_link").click(function(e){
        e.preventDefault();
        //console.log($(this).data('lng'));
        document.location.href="../"+$(this).data("lng")+"/checkout";
    });


    //displayNewTotal();

});


$(".pay-on-site").click(function(){
    $(".active-payment").removeClass("active-payment");
    $(".saferPay").hide(); // new
    $(".cc_pay_on_site_onglet").hide();
    $(".pay_on_site_onglet").show();
    $(".admin_pay_by_card_onglet").hide();
    $(".admin_pay_by_cash_onglet").hide();
    $(".pay_on_bill_onglet").hide();
    $(this).addClass("active-payment");
    $(".payment-choice").first().removeClass("active-payment").on("click", function(){
        $(".active-payment").removeClass("active-payment");
        $(this).addClass("active-payment");
        $(".saferPay").show(); // new
        $(".pay_on_site_onglet").hide();
    });

    $("#pay_on_site_btn").click(function(){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=cash';
        form.submit();
    });




});


$(".cc-pay-on-site").click(function(){

    $(".active-payment").removeClass("active-payment");
    $(".saferPay").hide(); // new
    $(".cc_pay_on_site_onglet").show();
    $(".pay_on_site_onglet").hide();
    $(".admin_pay_by_card_onglet").hide();
    $(".admin_pay_by_cash_onglet").hide();
    $(".pay_on_bill_onglet").hide();
    $(this).addClass("active-payment");
    $(".payment-choice").first().removeClass("active-payment").on("click", function(){
        $(".active-payment").removeClass("active-payment");
        $(this).addClass("active-payment");
        $(".saferPay").show(); // new
        $(".pay_on_site_onglet").hide();
    });

    $("#cc_pay_on_site_btn").click(function(){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=cc';
        form.submit();
    });
});

$(".admin-pay-by-cash").click(function(){

    $(".active-payment").removeClass("active-payment");
    $(".saferPay").hide(); // new
    $(".admin_pay_by_cash_onglet").show();
    $(".admin_pay_by_card_onglet").hide();
    $(".pay_on_site_onglet").hide();
    $(".pay_on_bill_onglet").hide();
    $(this).addClass("active-payment");
    $(".payment-choice").first().removeClass("active-payment").on("click", function(){
        $(".active-payment").removeClass("active-payment");
        $(this).addClass("active-payment");
        $(".saferPay").show(); // new
        $(".pay_on_site_onglet").hide();
    });

    $("#admin_pay_by_cash_btn").click(function(){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=bycash';
        form.submit();
    });
});

$(".admin-pay-by-card").click(function(){

    $(".active-payment").removeClass("active-payment");
    $(".saferPay").hide(); // new
    $(".admin_pay_by_card_onglet").show();
    $(".admin_pay_by_cash_onglet").hide();
    $(".pay_on_site_onglet").hide();
    $(".pay_on_bill_onglet").hide();
    $(this).addClass("active-payment");
    $(".payment-choice").first().removeClass("active-payment").on("click", function(){
        $(".active-payment").removeClass("active-payment");
        $(this).addClass("active-payment");
        $(".saferPay").show(); // new
        $(".pay_on_site_onglet").hide();
    });

    $("#admin_pay_by_card_btn").click(function(){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=bycard';
        form.submit();
    });
});


$(".pay-on-bill").click(function(){
    $(".active-payment").removeClass("active-payment");
    $(".saferPay").hide(); // new
    $(".cc_pay_on_site_onglet").hide();
    $(".admin_pay_by_card_onglet").hide();
    $(".admin_pay_by_cash_onglet").hide();
    $(".pay_on_site_onglet").hide();
    $(".pay_on_bill_onglet").show();
    $(this).addClass("active-payment");
    $(".payment-choice").first().removeClass("active-payment").on("click", function(){
        $(".active-payment").removeClass("active-payment");
        $(this).addClass("active-payment");
        $(".saferPay").show(); // new
        $(".pay_on_bill_onglet").hide();
    });

    $("#pay_on_bill_btn").click(function(){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=bill';
        form.submit();
    });
});

$(".checkout-menu-btn").hide();

/*function displayNewTotal(){
 var subtotal = 0;
 $(".td_comp_prod").each(function(){
 var cost = $(this).data("cost");
 if(typeof cost!=='undefined')
 subtotal += $(this).data("cost");
 });
 var total = $(".prix-total").data("cost")+subtotal;
 $("#prix-total-2").html(parseFloat(total).toFixed(2) + " CHF").data("finalTotal", total);
 }*/

function offChecking(){on_checking=false;}

var alredy_checked_bookings = false;
var on_checking = false;
/*$("body").on("mousemove", function(){
    if(on_checking==false){
        on_checking = true;
        setTimeout("offChecking()", 10000);
        if($(this).is(":visible")){

            var url = "/en/check-checkout?orderids=-24452";

            $.ajax({
                url: url,
                dataType: 'json',
                success: function(data){
                    if(!data["isOk"]){
                        $("#iframe_sp").hide();
                        var checkout_error_cron = "One of your order has been cancelled. Page will automatically refresh to load new datas.";
                        var checkout_error_add = "You have added a new order. Page will automatically refresh to load new datas";
                        if(data["error"] == "checkout_error_cron" && alredy_checked_bookings == false){
                            alredy_checked_bookings = true;
                            alert(checkout_error_cron);
                        }
                        else if(data["error"] == "checkout_error_add" && alredy_checked_bookings == false){
                            alredy_checked_bookings = true;
                            alert(checkout_error_add);
                        }
                        else{}
                        // seems like a booking doesn't exist anymore
                        document.location.reload();
                    }
                },
                error: function(err){

                }
            });
        }
    }

});*/

function sendZ(){
    var error_case = false;
    if($("#billing_lastname").val()==""){
        $('#billing_lastname').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_lastname").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_lastname').removeClass("error_custom_info");
    }
    if($("#billing_firstname").val()==""){
        $('#billing_firstname').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_firstname").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_firstname').removeClass("error_custom_info");
    }
    if($("#billing_address").val()==""){
        $('#billing_address').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_address").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_address').removeClass("error_custom_info");
    }
    if($("#billing_city").val()==""){
        $('#billing_city').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_city").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_city').removeClass("error_custom_info");
    }
    if($("#billing_npa").val()==""){
        $('#billing_npa').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_npa").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_npa').removeClass("error_custom_info");
    }
    if($("#billing_phone").val()==""){
        $('#billing_phone').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_phone").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_phone').removeClass("error_custom_info");
    }

    if($("#billing_email").val()==""  || validateEmail($("#billing_email").val())==false){
        $('#billing_email').addClass("error_custom_info");
        if(error_case==false)
            $("html, body").animate({ scrollTop: $("#billing_email").offset().top -50 }, 1);
        error_case = true;
    }
    else{
        $('#billing_email').removeClass("error_custom_info");
    }

    if($("#accept_cond_check").is(":checked")){
        $('#accept_cond_check').removeClass("error_custom_info_check");
    }
    else{
        $('#accept_cond_check').addClass("error_custom_info_check");
        error_case = true;
    }

    if(error_case==false){
        var form = document.getElementById("my-sample-form");
        form.action = '/en/checkout-success?pay_on_site=true&method=cash';
        form.submit();
    }


}