<div class="content-main">

  <style>
    .gc-onglets a{
      margin-right: 5px;
    }
  </style>

  <div class="gc-onglets clearfix" style=" padding-left: 2px; cursor:pointer; z-index: 100;" >
    <a href="/en/" id="home_link" class="gc-onglet clearfix" style="cursor:pointer;padding-top: 15px; float:left; text-align:left; margin-left: 5px;visibility:hidden; ">
      <div style="float:left; margin-left: 25px; font-weight:normal">
        TEE TIME<br />
        <span>Booking</span>
      </div>
    </a>
    <a style="display:none" href="/admin?locale=en">Entry <span>Members</span> and <br/><span>Partners clubs</span></a>
  </div>

  <section class="gc-search clearfix">

    <div class="gc-titre-coin col-md-5 col-lg-4" style=" ">
      <strong>Tee time</strong> <br class="hidden-xs hidden-sm"/>Booking <?php //echo $this->lang->line("Phonenumberinternationalformat"); ?>
			<img src="<?php echo base_url(); ?>assets/frontend/img/after-titre.png" class="after-titre" style=""/>
    </div>

    <form class="form-inline form-search col-md-16 col-lg-18">
      <div class="form-group col-sm-12  col-md-10 col-lg-7 clearfix" style="">
        <div class="col-xs-8 col-md-5"><label>Date</label></div>
        <div class="col-xs-16 col-md-19">
          <input class="btn btn-default datepicker" style="" id="dp1" type="text" value="<?=$booking_date?>">
        </div>
      </div>

      <div class="form-group col-sm-12  col-md-14 col-lg-9 clearfix search-hide-mobile" style="visibility:hidden">
        <div class="col-xs-8 col-md-7"><label>Players</label></div>
        <div class="col-xs-16 col-md-17">
          <button class="btn btn-default  nb_players_btn" id="nb_players_1">1</button>
          <button class="btn btn-default  nb_players_btn" id="nb_players_2">2</button>
          <button class="btn btn-default  nb_players_btn" id="nb_players_3">3</button>
          <button class="btn btn-default  nb_players_btn" id="nb_players_4">4</button>
        </div>
      </div>
      <div class="form-group col-sm-12 col-md-12 col-lg-8 clearfix">
        <div class="col-xs-8 col-md-10"><label>Course</label></div>
        <div class="col-xs-16 col-md-14">
          <select class="form-control" id="parcours" style="min-width: 185px">
<!--            <option value="0" selected>Severiano Ballesteros</option>
            <option value="1" >Jack Nicklaus</option>-->
            <?php foreach($courses_list as $row):?>
                    <option <?php echo $this->session->userdata("home_booking_course_id") == $row['id'] ? 'selected':''?> value="<?php echo $row['id'];?>" ><?php echo $row['name'];?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
    </form>
    <div class="display hidden-xs col-md-3 col-lg-2" style="float:right; ">
      <a style="cursor:pointer; display:none;" class="btnDisplay" data-target="row"><img src="<?php echo base_url(); ?>assets/frontend/img/icon-list-active.png"/></a>
      <a style="cursor:pointer; display:none;" class="btnDisplay" data-target="grid"><img src="<?php echo base_url(); ?>assets/frontend/img/icon-grid.png"/></a>
    </div>
  </section>


  <div class="gc-planning-journ">

    <table class="gc-table-planning hidden-xs">
    <?php $today_date = date("d-m-Y"); ?>
      <tr>
        <th class="gc-col-defil ">
          <a style="cursor:pointer;" class="defil-left<?php if(strtotime($pre_week_date) < strtotime($today_date) && strtotime($today_date . "last monday") != strtotime($pre_week_date)) { echo " is_past"; }else{ echo ''; } ?>" data-target="<?php echo date('d/m/Y', strtotime($pre_week_date)); ?>">
            <span class="icon icon-chevron-left"></span> 
          </a>
        </th>
        <?php 
          if(!empty($weeks_date_list)){
              $lp_key = 0;
            foreach($weeks_date_list as $date){
        ?>
            <th class="gc-col-day" style="<?php if(strtotime($today_date) > strtotime($date)){ echo 'background-color: #ddd;'; }else{ echo ''; } ?>">

              <a <?php if(date('d/m/Y', strtotime($date)) == $booking_date){echo 'class="active"';} ?> <?php if(strtotime($date) < strtotime($today_date)) {echo "oncliock='return dateInPast();'";} ?> data-day="<?php echo date('d/m/Y', strtotime($date));?>" style="cursor:pointer;">
                <time><?=date('M d', strtotime($date))?></time>
                <span class="day"><?=date('D', strtotime($date))?></span>
                <div class="meteo" style="visibility:hidden">
                  <img src="<?php echo base_url(); ?>assets/frontend/img/meteo/soleil.svg" style="visibility:hidden"/>
                </div>
                <div class="prix1"  style="visibility:hidden;">Date in past </div>
                  <div class="text-center"><small>From</small> <?php echo $current_date_lowest_price[$lp_key];?>.-</div>
              </a>
            </th>
        <?php $lp_key++; } } ?>
        <th class="gc-col-defil">
          <a style="cursor:pointer" class="defil-right" data-target="<?php echo date('d/m/Y', strtotime($next_week_date));?>"> <span class="icon icon-chevron-right"></span> </a>
        </th>
      </tr>
    </table>

    <style>
        .occupe{
          visibility: hidden;
        }
    </style>
    <?php
      $i = 0;
    if(!empty($courses_data_new)){
			foreach($courses_data_new as $k => $v){
                if(($booking_date != date('d/m/Y')) ||  (($booking_date == date('d/m/Y')) && $v['end_time'] > date('H'))){
		?>
			<div class="gc-creaneau-horaire" id="creneau_5">
            <div class="gc-titre-heure">
				<?php echo $v['start_time'].'h00'; ?> - <?php echo $v['end_time']."h00"; ?>
			</div>
            <div class="clearfix">
				<?php foreach($v['total_blocks_in_eachslot'] as $a => $b){
                    $availability = $b['block_player'] - $b['total_booked_players'];?>
                <div class="col-xs-12 col-md-8 col-lg-4" id="<?php echo $b['block_id'];?>">
                    <div class="gc-creaneau-heure <?php if($availability == 0 || $b['price'] == ''){echo ' complet ';}if(isset($b['offer_price']) && $b['price'] != $b['offer_price']){echo 'offre';}?>" style="">
                        <time>
							<?php
								$tapkoo = date('H:i', strtotime($b['block_start_data']));
								echo substr($tapkoo,0,2)."H".substr($tapkoo,3,2);
                                $data_start = substr($tapkoo,0,2)."H".substr($tapkoo,3,2);
							?>
                        </time><br/>
						<?php if(isset($b['offer_price']) && $b['price'] != $b['offer_price']){ ?>
							<del> <?php echo $b['price'];?>.-</del> / Player
							<span class="prix-offre" ><?php echo $b['offer_price']; $data_realcost = $b['offer_price']; $maincost = $b['price'];?>.- </span>
						<?php }else{ ?>
							<?php echo $b['price']; $data_realcost = $b['price']; $maincost = '';?>.- / Player
						<?php } ?>
                        <div class="gc-players">
							<?php
                            if($availability == 0){
                                for($j = 0;$j<$b['block_player'];$j++){?>
                                    <span class="ico-player " id="ico_player_<?php echo $j;?>"></span>
                                <?php }
                            }else{
                                for($j = 0;$j<$availability;$j++){?>
                                    <span class="ico-player " id="ico_player_<?php echo $j;?>"></span>
                                <?php }
                            }?>

                        </div>
                        <div class="">
							<?php echo $b['holes'];?>
							<!-- <img src="<?php echo base_url(); ?>assets/frontend/img/drapeau.png"/> -->
						</div>
                        <?php $btn_html = ''; $btn_key = 1;
                        foreach($b['total_booking_data'] as $key => $value){
                            $btn_html .=' data-member'.$btn_key.'firstname ="'.$value['firstname'].'"';
                            $btn_html .=' data-member'.$btn_key.'lastname ="'.$value['lastname'].'"';
                            $btn_html .=' data-member'.$btn_key.'handicap ="'.$value['hcp'].'"';
                            $btn_html .=' data-member'.$btn_key.'pricegroupid ="'.$value['pricegroup_id'].'"';
                            $btn_html .=' data-member'.$btn_key.'price ="'.$value['price'].'"';
                            $btn_key++;
                        }
                        $total_booking_data = '';
                        if(!empty($b['total_booking_data'])){
                            $total_booking_data = json_encode($b['total_booking_data']);
                        }
                        ?>
                        <div id="booking_member_data_<?php echo $b['block_id'];?>" class="hidden"><?php echo $total_booking_data;?></div>
                        <div id="booking_member_option_data_<?php echo $b['block_id'];?>" class="hidden"><?php echo json_encode($b['member_data']);?></div>
                        <div id="booking_option_data_<?php echo $b['block_id'];?>" class="hidden"><?php echo json_encode($b['option_data']);?></div>

                        <?php if($availability == 0){?>
                            <button class="btn btn-default" disabled="disabled"><span class="icon icon-checkmark"></span> Booked</button>
                        <?php }elseif($b['price'] == '') {?>
                            <button class="btn btn-default" disabled="disabled"><span class="icon icon-checkmark"></span> Closed</button>
                        <?php }else{?>
                            <button class="btn btn-default bookingBtn" <?php //echo $btn_html;?>
                                    data-block_id="<?php echo $b['block_id'];?>"
                                    data-order_master_id="<?php echo $b['order_master_id'];?>"
                                    data-total_player="<?php echo $b['block_player'];?>"
                                    data-availability="<?php echo $availability;?>"
                                    data-maincost="<?php echo $maincost;?>"
                                    data-costs=""
                                    data-realcost="<?php echo $data_realcost;?>"
                                    data-date="<?php echo $booking_date;?>"
                                    data-start="<?php echo $data_start;?>"
                                    data-dateoriginal="2016-11-03 11:00:00"
                                    data-holes="<?php echo $b['holes'];?>"

                                    data-toggle="modal" data-target="#modal-form"
                                    data-already="" <?php if($b['price'] == ''){echo 'disabled="disabled"';}?>><span class="icon icon-checkmark">
                        </span> Book</button>
                            <!-- <a href="<?php echo base_url();?>frontend/checkout/<?php echo $b['block_id'];?>" class="btn btn-default"></span> Book</a> -->
                        <?php }?>

                    </div>
                </div>
                <?php } ?>
                <script>today_low_price = 60;</script>
            </div>
        </div>
		<?php }}}else{ ?>
        <div class="schedule_error_msg">No booking available for this date.</div>
      <?php }?>
        <select class="gc-select-jour-planning form-control visible-xs">
            <option>JUN 25 - JEU - à partir de 89.-</option>
            <option>JUN 26 - VE - à partir de 89.-</option>
            <option>JUN 27 - SA - à partir de 95.-</option>
            <option>JUN 28 - DI - à partir de 98.-</option>
            <option>JUN 29 - LUN - à partir de 90.-</option>
            <option>JUN 30 - MA - à partir de 89.-</option>
            <option>JUL 01 - ME - à partir de 89.-</option>
        </select>
    </div>
</div>
