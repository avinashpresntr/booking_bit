<header>
  <!------------  BOX LOGIN MOBILE ------------>
  <!--<div class="nav-lang disconnect mobile-connect mobile-connect-2" style="padding: 6px; border: 1px solid #f8cc06;">
    <form name="login_form" action="<?php /*echo base_url('login_check')*/?>" method="post">
      <span style="margin-right: 15px; margin-left: 10px; margin-top: 4px; color: #333; text-transform: uppercase; font-weight:bold;"><?php /*echo $this->lang->line("login"); */?></span>
      <input type="text" name="username" placeholder="Username" required style="padding-left: 5px;">
      <input type="password" name="password" placeholder="Password" required style="padding-left: 5px;">
      <input type="submit" name="login_submit_btn" value="ok">
    </form>
  </div>-->
  <!------------  END BOX LOGIN MOBILE ------------>

  <nav class="navbar navbar-default">
    <div class="container">
      <div class="navbar-header">
        <a href="" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/frontend/img/Golf-Crans-logo.png" /> </a>
        <a class="mobile title" href=""><h1><span>Golf-Club Crans-sur-Sierre</span></h1></a>

        <div class="nav-lang">
            <select id="change_language" name="change_language" onchange="window.location = this.value;">
                <?php foreach($this->lang_m->getLanguages() as $row):?>
                <option value='<?=base_url()?>LanguageSwitcher/switchLang/<?php echo $row['name']?>/<?php echo $row['id']?>' <?php echo $this->session->userdata('site_lang_id') == $row['id'] ? 'selected':''; ?>><?php echo $row['name'];?></option>
                <?php endforeach;?>
            </select>
<?php /*			
<!--			<a href='<?=base_url()?>LanguageSwitcher/switchLang/french' class="<?php if($this->session->userdata('site_lang') == 'french') {echo 'active';} ?>">Fr</a>
			<a href='<?=base_url()?>LanguageSwitcher/switchLang/english' class="<?php if($this->session->userdata('site_lang') == 'english') {echo 'active';} ?>">En</a>
			<a href='<?=base_url()?>LanguageSwitcher/switchLang/german' class="<?php if($this->session->userdata('site_lang') == 'german') {echo 'active';} ?>">De</a>
			<a href='<?=base_url()?>LanguageSwitcher/switchLang/italian' class="<?php if($this->session->userdata('site_lang') == 'italian') {echo 'active';} ?>">It</a>-->
			<!-- <a class="language_link" data-lng="fr">Fr</a>
			<a class="language_link active" data-lng="en">En</a>
			<a class="language_link" data-lng="de">De</a>
			<a class="language_link" data-lng="it">It</a> --> */ ?>
        </div>

        <!---- nav langues mobile --->
        <div class="nav-user-mobile">Vous n'êtes pas connecté</div>

        <div class="nav-lang-mobile">

			
          <div class="nav-lang-mobile-button">En</div>
						<div class="lang-mobile-dropdown">
							<a class="mobileLanguage_link" data-lng="fr">En</a>                                                
							<a class="mobileLanguage_link" data-lng="de">De</a>                        
							<a class="mobileLanguage_link" data-lng="it">It</a>                    
						</div>
          </div>
          <!---- end nav langues mobile --->

          <div class="nav-lang disconnect mobile-connect" style="padding: 6px; border: 1px solid #f8cc06;">
              <?php if($this->session->userdata('user_login_session')){?>
                  <a href="<?php echo base_url('frontend/myaccount')?>">My Account</a>
                  <a href="<?php echo base_url('frontend/logout')?>">Logout</a>
              <?php }else{?>
                  <form name="login_form" action="<?php echo base_url('frontend/login_check')?>" method="post">
                      <span style="margin-right: 15px; margin-left: 10px; margin-top: 4px; color: #333; text-transform: uppercase; font-weight:bold;"><?php echo $this->lang->line("login"); ?></span>
                      <input type="email" name="username" placeholder="Username" value="<?=set_value('username'); ?>" required style="padding-left: 5px;">
                      <input type="password" name="password" placeholder="Password" required style="padding-left: 5px;">
                      <input type="submit" name="login_submit_btn" value="ok">
                  </form>
                  <div class="text-center color-red">
                      <?php //echo validation_errors('<span style="display: block;">','</span>');?>
                      <?php if ($this->session->flashdata('login_errors')){
                          echo $this->session->flashdata('login_errors');
                      }?>
                  </div>
              <?php }?>
          </div>

        </div>

      </div>
    </nav>
    <img src="<?php echo base_url(); ?>assets/frontend/img/img-entete.jpg" class="img-responsive"/>

    <!--- Onglets mobile ---->
    <div class="onglets-mobile">
      <a class="login_mobile" onClick="showMobileLogin()"><span><?php echo $this->lang->line("login"); ?></span></a>
      <a href="/en/" class="active"><span>Tee Time</span></a>
    </div>
    <!--- End Onglets mobile ---->

    <script>
      function showMobileLogin(){
        if( $(".mobile-connect").is(":visible")){
          $(".mobile-connect").slideUp("fast");
          $("#login_mobile").css("background", "#f8cc06");
        }
        else{
          $(".mobile-connect").slideDown("slow");
          $("#login_mobile").css("background", "white");
        }

      }
    </script>

  </header>
