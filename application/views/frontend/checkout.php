<div class="gc-onglets clearfix" style=" padding-left: 2px; cursor:pointer; z-index: 100;">
    <a href="/en/" id="home_link" class="gc-onglet clearfix" style="cursor:pointer;padding-top: 15px; float:left; text-align:left; margin-left: 5px; ">
        <div style="float:left; margin-left: 25px; font-weight:normal">
            TEE TIME<br>
            <span>
            Booking        </span>
        </div>
    </a>
    <a style="display:none" href="/admin?locale=en">Entry <span>Members</span> and <br><span>Partners clubs</span></a>
    <a href="/en/checkout" class="checkout-menu-btn" style="margin-right: 5px; display: none;">End my <br><span>Booking</span> (1)</a>
</div>
<div class="content-main">
    <section class="gc-panier">
        <style>
            .remindToConfirm{
            float:left;
            width: 100%;
            height: 50px;
            padding: 10px;
            text-align:right;
            margin-bottom: 25px;
            }
            .error_custom_info{
                border-color: red !important;
            }
            .success_custom_info{
                border-color: green !important;
            }

        </style>
        <script>
            function goToFinish(){
              $('html, body').animate({
                scrollTop: $(".gc-paiement").offset().top
              }, 10);

              $("#btnPay").click();
            }
        </script>
        <div class="remindToConfirm">
            <input type="button" value="Confirm reservation" style="padding: 8px;" onclick="goToFinish()">
        </div>
        <style>
            .recap_td{
            font-weight:normal !important;
            font-family: futura,Helvetica,Arial,sans-serif !important;
            }
        </style>
        <h2>Your booking</h2>
        <fieldset>
            <legend>
                <h3>Booking</h3>
            </legend>
            <table class="table hidden-xs">
                <tbody>
                    <tr>
                        <th>Date</th>
                        <th>Hour</th>
                        <th>Course</th>
                        <th>Options</th>
                    </tr>
                    <tr class="gc-resa">
                        <td><?php echo date('l, F j, Y',strtotime($block_date))?></td>
                        <td>
							<?php
								$tapkoo = date('H:i', strtotime($start_time));
								echo substr($tapkoo,0,2)."H".substr($tapkoo,3,2);
                                $data_start = substr($tapkoo,0,2)."H".substr($tapkoo,3,2);
							?>
                        </td>
                        <td>
                            <?php echo $course_name;?>
                        </td>
                        <!--<td><img src="/img/drapeau.png"></td>-->
                        <td><?php echo $course_holes;?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table">
                <tbody>
                    <tr>
                        <th colspan="2">Players</th>
                        <th> HCP</th>
                        <th>PRICE GROUP</th>
                        <th class="prix text-align-right recap-mobile-min">Price</th>
                    </tr>
                    <?php foreach($player_data as $p => $q){ ?>
                    <tr>
                        <td class="num-joueur"><?php echo $p+1;?></td>
                        <td><?php echo $q['name'];?></td>
                        <td><?php echo $q['hcp'];?></td>
                        <td><?php echo $q['member'];?></td>
                        <td class="text-align-right">
                            <?php echo $q['price'];?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- options header -->
            <table class="table hidden-xs" style="width: 100%">
                <tbody>
                    <tr>
                        <th colspan="3">Options</th>
                        <th class="small-col">Qty</th>
                        <th class="small-col">Price</th>
                    </tr>
                    <?php foreach($option_data as $o_key => $o_value){ ?>
                    <tr>
                        <td class="num-joueur"><?php echo $o_key+1;?></td>
                        <td colspan="2"><?php echo $o_value['option_name'];?></td>
                        <td class="small-col"><?php echo $o_value['qty'];?></td>
                        <td class="text-align-right small-col">
                            <?php echo $o_value['price'];?>
                        </td>
                    </tr>
                    <?php }?>
                    <tr class="gc-resa-total">
                        <td colspan="3">
                            <button type="button" class="btn btn-default hidden-xs" onclick="if(confirm('Do you really wish to remove your order ?')){document.location = '<?php echo base_url().'frontend/delete_order/'.$order_id;?>'; var alredy_checked_bookings = true;}">Delete</button>
                        </td>
                        <td class="text-align-right"><strong>Subtotal</strong></td>
                        <td class="text-align-right"><strong><?php echo $sub_total;?></strong></td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        <div class="gc-panier-total">
            TOTAL <span class="prix-total" data-cost="60"><?php echo $sub_total;?></span>
        </div>
    </section>
    <style>
        .complementary_products{
        margin-top: -100px;
        }
        .table_complementary_products{
        width: 100%;
        }
        .td_comp_prod{
        padding: 10px;
        border: 1px solid #ccc;
        text-align:center;
        }
        .th_comp_prod{
        padding: 10px;
        border: 1px solid #ccc;
        }
        .select-product-complementary{
        float:left;
        padding: 5px;
        cursor:pointer;
        }
        .prod_comp_nb{
        text-align:center;
        min-width: 55px;
        }
        .btnAddProductToOrder{
        float:right;
        margin-top: 10px;
        display:none;
        }
        .label-prix{
        text-transform:uppercase;
        }
        .minusBtn{
        float:left;
        margin-right: 5px;
        background-color: #aaa;
        color:white;
        border: 1px solid #ccc;
        padding: 2px;
        width: 30px;
        text-align:center;
        cursor:pointer;
        }
        .plusBtn{
        float:left;
        margin-left: 5px;
        background-color: #aaa;
        color:white;
        border: 1px solid #ccc;
        padding: 2px;
        width: 30px;
        text-align:center;
        cursor:pointer;
        }
        .minusBtn:hover, .plusBtn:hover{
        background-color: rgba(248,204,6,.7);
        border: 1px solid rgba(248,204,6,.7);
        color:black;
        }
        @media screen and (max-width: 480px) {
        .complementary_products{
        margin-top: 10px;
        }
        }
    </style>
    <section class="gc-paiement" style="margin-top:-40px;">
        <!--<form id="my-checkout-form" method="post" action="" onsubmit="return false;">--> <!-- Define path here -->
        <?php if($sub_total == 0){$form_url = base_url()."frontend/save_booking";}else{$form_url = base_url()."frontend/redirect_to_paypal";}?>
        <form id="my-checkout-form" method="post" action="<?php echo $form_url;?>">
            <div class="gc-form-paiement" id="frame_1">
                <h4>Billing address</h4>
                <!--lastname -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right">
                        <!-- <label><span class="ast">*</span> Payment Amount IN USD</label>-->
                    </div>
                    <div class="col-sm-12">
						<input type="hidden" class="form-control" name="billing_amount" id="billing_amount" type="text" value="<?=$booking_session_data['billing_amount']?>">
						<input type="hidden" class="form-control" name="order_id" id="order_id" type="text" value="<?=$booking_session_data['order_id']?>">
					</div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right">
						<label><span class="ast">*</span> Last name</label>
					</div>
                    <div class="col-sm-12">
						<input class="form-control" name="billing_lastname" id="billing_lastname" type="text" value="<?=$booking_session_data['billing_lastname']?>" required="required" />
					</div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right">
						<label><span class="ast">*</span> First name</label>
					</div>
                    <div class="col-sm-12">
						<input class="form-control" name="billing_firstname" id="billing_firstname" type="text" value="<?=$booking_session_data['billing_firstname']?>" required />
					</div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span>  Country</label></div>
                    <div class="col-sm-12">
                        <select name="billing_country" class="form-control" id="billing_country" required>
                            <option value="">-- Select Country --</option>
                            <?php foreach($country as $a => $b){ ?>
								<option value="<?php echo $b->id; ?>" <?=$booking_session_data['billing_country'] == $b->id ? 'selected':''?> ><?php echo $b->country_name; ?></option>
							<?php } ?>
                        </select>
                    </div>
                </div>
                <!-- adress -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span>  Address</label></div>
                    <div class="col-sm-12"><input type="text" class="form-control" name="billing_address" id="billing_address" value="<?=$booking_session_data['billing_address']?>" placeholder="" required /></div>
                </div>
                <!-- adress second line -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Address</label></div>
                    <div class="col-sm-12"><input type="text" class="form-control" name="billing_address_2" id="billing_address_2" value="<?=$booking_session_data['billing_address_2']?>" placeholder="" /></div>
                </div>
                <!-- city -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> City</label></div>
                    <div class="col-sm-12"><input type="text" class="form-control" name="billing_city" id="billing_city" value="<?=$booking_session_data['billing_city']?>" placeholder="" required /></div>
                </div>
                <!-- Canton -->
                <!--<div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Area</label></div>
                    <div class="col-sm-12">
                        <select class="form-control" id="billing_area">
                            <option value="">-- Select Area --</option>
                            <option value="">Area1</option>
                            <option value="">Area2</option>
                        </select>
                    </div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Area 1</label></div>
                    <div class="checkbox col-sm-6" style="margin-top: 0px">
                        <label style="line-height: 1px"><input type="checkbox" name="billing_area_1" value="1" style="top:0px" />Area1</label>
                    </div>
                    <div class="checkbox col-sm-6" style="margin-top: 0px">
                        <label style="line-height: 1px"><input type="checkbox" name="billing_area_2" value="1" style="top:0px" />Area2</label>
                    </div>
                </div>-->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Area</label></div>
                    <div class="col-sm-12"><input type="text" class="form-control" name="billing_area" value="<?=$booking_session_data['billing_area']?>" placeholder="" /></div>
                </div>
                <!-- Zip -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> Zip</label></div>
                    <div class="col-sm-12"><input type="text" class="form-control" name="billing_npa" id="billing_npa" value="<?=$booking_session_data['billing_npa']?>" placeholder="" required /></div>
                </div>
                <!-- Téléphone -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> Phone</label></div>
                    <div class="col-sm-12"><input type="tel" class="form-control" name="billing_phone" id="billing_phone" value="<?=$booking_session_data['billing_phone']?>" placeholder="" required /></div>
                </div>
                <!-- Email -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> email</label></div>
                    <div class="col-sm-12"><input class="form-control" type="email" name="billing_email" id="billing_email" value="<?=$booking_session_data['billing_email']?>" required /> </div>
                </div>
                <br>
                <!-- checkbox want to save data's -->
                <div class="form-line row clearfix" style="text-align:center;">
                    <div class="col-xs-21 col-sm-23"><input type="checkbox" id="select-create-account" name="create_account" value="1" <?=$booking_session_data['create_account'] == 1 ? 'checked':''?>> I want to create an account to save my data.</div>
                </div>
                <!-- passwords -->
                <div class="gc-create-account">
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> <small>Password </small></label></div>
                        <div class="col-sm-12"><input type="password" class="form-control" name="billing_pwd_1" id="billing_pwd_1" placeholder="" /></div>
                    </div>
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> <small>Repeat password </small></label></div>
                        <div class="col-sm-12"><input type="password" name="billing_pwd_2" class="form-control" id="billing_pwd_2" /> </div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="conditions"><input type="checkbox" id="accept_cond_check" name="accept" value="1" <?=$booking_session_data['accept'] == 1 ? 'checked':''?> style="cursor:pointer;"> <a href="" data-toggle="modal" data-target="#General_Conditions_Modal">I accept the general conditions</a></div>
                    <button type="submit" class="btn btn-primary" id="btnPay"> Confirm Reservation</button>
                </div>
                <div class="payment-choices" style="display:none">
                    <div class="payment-choice active-payment">Online payment</div>
                </div>
            </div>
            <style>
                .pay_on_site_btn{
                height: 45px; line-height: 20px; vertical-align:middle; padding: 10px; padding-left: 25px; padding-right: 25px;
                }
            </style>
            <div class="gc-form-paiement pay_on_site_onglet" style="margin-top: 105px; display:none; text-align:center;">
                <input type="button" value="Confirm reservation" class="pay_on_site_btn" id="pay_on_site_btn">
            </div>
            <div class="gc-form-paiement cc_pay_on_site_onglet" style="margin-top: 105px; display:none; text-align:center;">
                <input type="button" value="Confirm reservation" class="pay_on_site_btn" id="cc_pay_on_site_btn">
            </div>
            <div class="gc-form-paiement admin_pay_by_cash_onglet" style="margin-top: 105px; display:none; text-align:center;">
                <input type="button" value="Confirmer" class="pay_on_site_btn" id="admin_pay_by_cash_btn">
            </div>
            <div class="gc-form-paiement admin_pay_by_card_onglet" style="margin-top: 105px; display:none; text-align:center;">
                <input type="button" value="Confirmer" class="pay_on_site_btn" id="admin_pay_by_card_btn">
            </div>
            <div class="gc-form-paiement pay_on_bill_onglet" style="margin-top: 105px; display:none; text-align:center;">
                <input type="button" value="Confirm reservation" class="pay_on_site_btn" id="pay_on_bill_btn">
            </div>
            <div class="gc-form-paiement saferPay" style="margin-top: 5px; ">
                <h4>Credit card</h4>
                <div id="before_pay_label">To proceed to payment, please enter your personal information and agree with the general terms and conditions of sale</div>
                <div style="width:100%; height:auto; padding-top: -35px; z-index: 2; overflow:hidden;">
                    <iframe src="https://www.saferpay.com/vt2/api/PaymentPage/485394/17885962/y010ui24jy11nln0azfl9jzna" style="width: 90%; min-height: 1000px; overflow:hidden; margin-left: 5%;z-index: 1;  border: 0; display:none;" id="iframe_sp"></iframe>
                </div>
            </div>
            <div class="gc-form-paiement braintree" style="margin-top: 105px;display: none;">
                <h4>Credit card</h4>
                <div class="text-center" style="">
                    <!-- custom form example -->
                    <div class="form-line row clearfix" style="display:none">
                        <div class="col-sm-8 text-align-right"><label for="name-on-card"><span class="ast">*</span> Name of the credit card</label></div>
                        <div class="col-sm-12"><input name="name-on-card" id="name-on-card" value=""></div>
                    </div>
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label for="card-number"><span class="ast">*</span> Card number</label></div>
                        <div class="col-sm-12">
                            <div id="card-number" value="5105105105105100"></div>
                        </div>
                    </div>
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label for="expiration-date"><span class="ast">*</span> Expiration date</label></div>
                        <div class="col-sm-12">
                            <div id="expiration-date" value="05/16"></div>
                        </div>
                    </div>
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label for="cvv"><span class="ast">*</span> CVV</label></div>
                        <div class="col-sm-12">
                            <div id="cvv" value="111"></div>
                        </div>
                        <div class="text-center">
                            <div class="conditions"><input type="checkbox" id="accept_cond_check" style="cursor:pointer;"> <a href="" data-toggle="modal" data-target="#myModal">I accept the general conditions</a></div>
                            <button type="button" class="btn btn-primary" id="btnPay1" style="display:none"> PAY</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>

<!--General Conditions Modal-->
<div id="General_Conditions_Modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $terms_condition->title;?></h4>
            </div>
            <div class="modal-body">
                <?php echo $terms_condition->content;?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End General Conditions Modal-->
