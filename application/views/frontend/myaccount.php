<div class="gc-onglets clearfix" style=" padding-left: 2px; cursor:pointer; z-index: 100;">
    <a href="/en/" id="home_link" class="gc-onglet clearfix" style="cursor:pointer;padding-top: 15px; float:left; text-align:left; margin-left: 5px; ">
        <div style="float:left; margin-left: 25px; font-weight:normal">TEE TIME<br><span>My Account</span></div>
    </a>
</div>
<div class="content-main">

    <section class="gc-paiement" style="margin-top:-40px;">

        <form id="my-account-form" method="post" action="<?php echo base_url()."frontend/save_myaccount";?>">
            <div class="gc-form-paiement" id="frame_1">
                <div class="text-center">
                    <?php if($this->session->flashdata('success_msg')){?>
                    <div class="alert alert-success">
                        <?=$this->session->flashdata('success_msg');?>
                    </div>
                    <?php }?>
                    <?php if($this->session->flashdata('error_msg')){?>
                        <div class="alert alert-danger">
                            <?=$this->session->flashdata('error_msg');?>
                        </div>
                    <?php }?>
                </div>

                <input type="hidden" class="form-control" name="id" value="<?=$user_data['id']?>">

                <!--lastname -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right">
						<label><span class="ast">*</span> Last name</label>
					</div>
                    <div class="col-sm-12">
						<input class="form-control" name="lastname" id="ma_lastname" type="text" value="<?=$user_data['lastname']?>" required />
                        <?=form_error('lastname', '<span class="color-red">', '</span>'); ?>
					</div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right">
						<label><span class="ast">*</span> First name</label>
					</div>
                    <div class="col-sm-12">
						<input class="form-control" name="firstname" id="ma_firstname" type="text" value="<?=$user_data['firstname']?>" required/>
                        <?=form_error('firstname', '<span class="color-red">', '</span>'); ?>
					</div>
                </div>
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span>  Country</label></div>
                    <div class="col-sm-12">
                        <select name="country_id" class="form-control" id="ma_country_id" required>
                            <option value="">-- Select Country --</option>
                            <?php foreach($country as $a => $b){ ?>
								<option value="<?php echo $b->id; ?>" <?=$user_data['country_id'] == $b->id ? 'selected':''?> ><?php echo $b->country_name; ?></option>
							<?php } ?>
                        </select>
                        <?=form_error('country_id', '<span class="color-red">', '</span>'); ?>
                    </div>
                </div>
                <!-- adress -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span>  Address</label></div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="address1" id="ma_address1" value="<?=$user_data['address1']?>" placeholder="" required />
                        <?=form_error('address1', '<span class="color-red">', '</span>'); ?>
                    </div>
                </div>
                <!-- adress second line -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Address</label></div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="address2" id="ma_address2" value="<?=$user_data['address2']?>" placeholder="" />
                    </div>
                </div>
                <!-- city -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> City</label></div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="city" id="ma_city" value="<?=$user_data['city']?>" placeholder="" required />
                        <?=form_error('city', '<span class="color-red">', '</span>'); ?>
                    </div>
                </div>
                <!-- Area -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label>Area</label></div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="area" value="<?=$user_data['area']?>" placeholder="" />
                    </div>
                </div>
                <!-- Zip -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> Zip</label></div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="zip" id="ma_zip" value="<?=$user_data['zip']?>" placeholder="" required />
                        <?=form_error('zip', '<span class="color-red">', '</span>'); ?>
                    </div>
                </div>
                <!-- Téléphone -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> Phone</label></div>
                    <div class="col-sm-12">
                        <input type="tel" class="form-control" name="phone" id="ma_phone" value="<?=$user_data['phone']?>" placeholder="" required />
                        <?=form_error('phone', '<span class="color-red">', '</span>'); ?>
                    </div>
                </div>
                <!-- Email -->
                <div class="form-line row clearfix">
                    <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> email</label></div>
                    <div class="col-sm-12"><input class="form-control" type="email" name="email" id="ma_email" value="<?=$user_data['email']?>" readonly required /> </div>
                </div>
                <br>
                <!-- checkbox want to save data's -->
                <div class="form-line row clearfix" style="text-align:center;">
                    <div class="col-xs-21 col-sm-23"><input type="checkbox" id="ma_change_password" name="change_password" value="1" <?=$_POST['change_password']?'checked':'';?> > I want to change your password.</div>
                </div>
                <!-- passwords -->
                <div class="my_change_password" style="display: none">
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> <small>Password </small></label></div>
                        <div class="col-sm-12">
                            <input type="password" class="form-control" name="user_pass" id="ma_user_pass" placeholder="" />
                            <?=form_error('user_pass', '<span class="color-red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-line row clearfix">
                        <div class="col-sm-8 text-align-right"><label><span class="ast">*</span> <small>Repeat password </small></label></div>
                        <div class="col-sm-12">
                            <input type="password" name="re_user_pass" class="form-control" id="ma_re_user_pass" />
                            <?=form_error('re_user_pass', '<span class="color-red">', '</span>'); ?>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary" id="myaccount_save_btn"> Save</button>
                </div>
            </div>
        </form>
    </section>
</div>
