<?php
	foreach($records as $row)
	{
		$page_id = $row->page_id;
		$name = $row->name;
		$content = $row->content;
		$last_update_time = $row->last_update_time;
	}
?>
<html>
	<head>
		<!------------------      ckeditor     ------------------>
        <script src="<?php echo base_url();?>assets/plugin/ckeditor/ckeditor.js"></script>
	</head>
	<body>
		<?php
			echo form_open('golfclub/cms_page/update_data');
			echo form_hidden('page_id',$page_id);
			echo "<h1>".$name."</h1>";
			echo form_input(array('name'=>'name','value'=>$name));
			echo form_textarea(array('name'=>'content','value'=>$content));
			echo "<b>Last updated at:".$last_update_time."</b><br>";
			echo form_submit(array('name'=>'submit','value'=>'SAVE'));
			echo form_close();
		?>            
		<script>
    		// Replace the <textarea id="editor1"> with a CKEditor
    		// instance, using default configuration.
                CKEDITOR.replace( 'content' );
        </script>            
	</body>
</html>