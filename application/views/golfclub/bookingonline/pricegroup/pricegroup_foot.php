<?php
session_start();
$ajvc= isset($_SESSION["lang"]) && $_SESSION["lang"] != ''?$_SESSION["lang"].".js":'english.js';
?>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/i18n/'.$ajvc);?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/i18n/grid.locale-en.js');?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/jquery.jqGrid.min.js');?>" type="text/javascript"></script>

<script src="<?php echo site_url('assets/js/jquery.validate.js');?>" type="text/javascript"></script>


<script type="text/javascript">
var PAGE_URL_START = "<?php echo site_url('golfclub/bookingonline');?>";
var PAGE_URL_END = "";
<?php if($session['type'] < 2):?>
PAGE_URL_END = "?user=<?=$user['id'];?>";
<?php endif;?>

var childGrid = '';
$(function(){
    // language dropdown
    $("#dipFacilities").multiselect();

    $( "#basicForm" ).submit(function( event ) {
      var data = $(this).serializeArray();
      if(ratesecId != 0){
        var url = PAGE_URL_START+"/save_rate_section/<?=$row->id;?>/"+ratesecId+PAGE_URL_END;
      }else{
        var url = PAGE_URL_START+"/save_rate_section/<?=$row->id;?>"+PAGE_URL_END;
      }
      $.ajax({type: "POST", url: url,data:data, success: function(data) {
        if(data==1){
          $('#first_model').modal('hide');
          $('#pricegrouplist').trigger('reloadGrid');
        }
      }});
      event.preventDefault();
    });

    $( "#childForm" ).submit(function( event ) {
      var data = $(this).serializeArray();
      if(rateId != 0){
        var url = PAGE_URL_START+"/save_rate/"+ratesecId+"/"+rateId+PAGE_URL_END;
      }else{
        var url = PAGE_URL_START+"/save_rate/"+ratesecId+PAGE_URL_END;
      }
      $.ajax({type: "POST", url: url,data:data, success: function(data) {
        if(data==1){
          $('#child_model').modal('hide');
          $('#'+childGrid).trigger("reloadGrid");
        }
      }});
      event.preventDefault();
    });
});
function edit_parent_item(no){

	    $('#first_model').modal('show');
  
} 

var $grid = $("#pricegrouplist");
$(document).ready(function () {
	var url = PAGE_URL_START + '/pricegrouplist';
	$grid.jqGrid({
		url: url,
		datatype: "json",
		colNames: ['Price Group', 'Action'],
		colModel: [
			{
				label: 'Price Group',
				name: 'pricegroup_name',
				index:'name',
				align: "center",
				formatter: formatJson
			},
			{
				label: 'Action',
				name: 'id',
				index:'id',
				sortable: false,
				width: '20',
				align: "center",
				formatter: formatAction1
			}
		],
		rowNum: 50,
		height: 400,
		rowList: [10, 20, 50, 100],
		pager: '#pager',
		sortname: 'pricegroup_name',
		viewrecords: true,
		sortorder: "asc",
		autowidth: true,
		shrinkToFit: true,
		subGrid: true,
		cmTemplate: { sortable: false },
		subGridRowExpanded: showChildGrid,
		loadComplete: function (data) {

		},
		beforeRequest: function () {
			$('#jqNoData').remove();
			responsive_jqgrid($("#gbox_pricegrouplist"));
		}
	});
	$("#pager").before('<div class="dip-table-add" style="text-align:center;"><button type="button" class="btn btn-success btn-block" onclick="edit_parent_item(0)"> + Add Price Group </button> </div>');
	$grid.jqGrid('sortableRows', {
		update: function (e, ui) {
			var id = ui.item[0].id;
			var position = ui.item[0].rowIndex;
			update_parent_position(id, position);
			// console.log(ui.item[0]);
		}
	});
	jQuery("#pricegrouplist").jqGrid('navGrid', '#pager', {edit: false, add: false, del: false});

	function formatJson(cellValue, options, rowObject) {
		var obj = jQuery.parseJSON(cellValue.replace("'", "\'"));
		var dlang = <?=$client->default_language;?>;
		if (obj[dlang].length > 80) {
			return obj[dlang].substr(0, 80) + '...';
		} else {
			return obj[dlang];
		}
	};
	function formatAction1(cellValue, options, rowObject) {
		var e_btn = get_edit_link('#'+BASE_URL + "golfclub/bookingonline/edit/" + cellValue + PAGE_URL_END);
		var d_btn = get_delete_btn('onclick="delete_item(' + cellValue + ');"');
		if( <?php echo $visiteur_label_id; ?> != cellValue && <?php echo $visiteur_pricegroup_id; ?> != cellValue && <?php echo $options_parent_id; ?> != cellValue ){
			return e_btn + ' ' + d_btn;
		} else {
			return e_btn;
		}
	};

	function showChildGrid(parentRowID, parentRowKey) {
		var childGridID = parentRowID + "_table";
		var childGridPagerID = parentRowID + "_pager";
		var template = '<div class="dip-table-add"><a class="btn btn-success btn-block" href="<?php echo site_full_url('golfclub/bookingonline/newsubpricegroup/');?>/'+ parentRowKey +'"> + Add More </button></div>';
		var childGridURL = PAGE_URL_START + '/subpricegrouplist/'+ parentRowKey;
		$('#' + parentRowID).append('<table id=' + childGridID + '></table>' + template);

		$("#" + childGridID).jqGrid({
			url: childGridURL,
			mtype: "GET",
			datatype: "json",
			page: 1,
			colNames: ['Price Group', 'Action'],
			colModel: [
				{
					label: 'Price Group',
					name: 'pricegroup_name',
					index:'name',
					align: "center",
					formatter: formatJson
				},
				{
					label: 'Action',
					name: 'id',
					index:'id',
					sortable: false,
					width: '20',
					align: "center",
					formatter: formatAction2
				}
			],
			// loadonce: true,
			height: '100%',
			rowNum: 500,
			sortname: 'pricegroup_name',
			sortorder: "asc",
			pager: "#" + childGridPagerID,
			pagerpos: 'right',
			autowidth: true,
			shrinkToFit: true,
			cmTemplate: { sortable: false },
			beforeRequest: function () {
				$("#gview_" + childGridID + ' .ui-jqgrid-hdiv').hide();
			}
		});
		$("#" + childGridID).jqGrid('sortableRows', {
			update: function (e, ui) {
				var id = ui.item[0].id;
				var position = ui.item[0].rowIndex;
				var parent = parentRowKey;
				update_child_position(id, position, parent);
				// console.log(ui.item[0]);
			}
		});
	}

	function formatAction2(cellValue, options, rowObject) {
		var e_btn = get_edit_link(BASE_URL + "golfclub/bookingonline/edit_subpricegroup/" + cellValue);
		var d_btn = get_delete_btn('onclick="delete_item(' + cellValue + ');"');
		if( <?php echo $visiteur_label_id; ?> != cellValue && <?php echo $visiteur_pricegroup_id; ?> != cellValue && <?php echo $options_parent_id; ?> != cellValue ){
			return e_btn + ' ' + d_btn;
		} else {
			return e_btn;
		}
	};

});

$(document).ready(function () {
  $('#jqgh_pricegrouplist_name').css({
    'text-align': 'left'
  });
  $('#jqgh_pricegrouplist_name .s-ico').hide();

  $('#jqgh_pricegrouplist_id').css({
    'text-align': 'center'
  });
});
</script>


<style>
  /* page specific css */
  .dip-table-add .btn-block{
    color:#28B62C;
    background: #E7FFEB;
    -webkit-border-radius: 0;
            border-radius: 0;
    border: 2px dashed rgba(179, 203, 255, 0.33);
    padding: 5px;
  }
  .dip-table-add .btn-block:hover{
    background: rgb(202, 236, 171);
    color: black;
  }
  .ui-jqgrid .ui-subgrid td.subgrid-cell {
      background: rgb(102, 130, 75);
  }
  .ui-jqgrid .ui-subgrid td.subgrid-data{
    padding: 10px 0;
    background: rgb(102, 130, 75);
  }
</style>
