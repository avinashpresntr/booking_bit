<section class="dip-dash-sec">
  <h3>New Price Group<? //=$page['desc'];?></h3>
  <?php echo form_open_multipart(current_full_url(), ' id="editsubpricegroup_form" class="dip-form form-horizontal"');?>
    <?php //echo get_alerts('golfclub/courses', 'Course'); ?>
    <p class="error_msg"> <?php //echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body" id="pricegroupform_id">
		<?php echo form_hidden('subpricegroup_id', $pricegroup_data_new[0]['pricegroup_id']);?>
		<?php echo validation_errors(); ?>
		<div class="col-md-12">
			<?php $pricegroup_name = json_decode($pricegroup_name[0]['pricegroup_name']); ?>
			<?php echo form_hidden('pricegroup_id', $pricegroup_id);?>
		</div>
			
        <div class="col-sm-12">
          <ul class="nav nav-tabs">
            <?php 
            $alang = array();
            foreach ($client->languages as $key => $value) {
              $active='';
              if($client->default_language == $value){
                $active='class="active"';
              }
              echo '<li '.$active.'><a class="btn btn-default" data-toggle="tab" href="#'.$langs[$value].'"><i class="flag flag-'.$langs[$value].'" alt="'.$langs[$value].'"></i> '.$langs[$value].'</a></li>';
            }?>
          </ul>


          <!-- Tab panes -->
          <div class="tab-content">

			<?php foreach ($client->languages as $key => $value):
				$active='';
				$required = '';
				if($client->default_language == $value){
					$active='active';
					$required = 'required';
				}
			?>
              <div role="tabpanel" class="tab-pane row <?=$active;?>" id="<?=$langs[$value];?>">
				<div class="col-md-12">
				  <div class="child-box">
					<fieldset>
						<legend></legend>
						<div class="form-group">
							<label class="col-md-2 control-label" for="dipFormat">Price Name</label>
							<div class="col-md-4">
								<?php echo form_input('pricegroup_name['.$value.']', set_value('format['.$value.']',(isset($row->format[$value])?$row->format[$value]:'')), 'class="form-control" id="dipFormat" placeholder="Price Name '.$langs[$value].'" '. $required );?>
							</div>
							<div class="col-md-4">
								<?php echo '<strong>( Price Group : '.$pricegroup_name->{$value}.' )</strong>'; ?>
							</div>
						</div>
					</fieldset>
				  </div>
				</div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>

		<div class="col-md-2">
			<label class="control-label" for="course"><?php echo 'Course';//echo $this->lang->line('EventName'); ?></label>
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<?php /*foreach($cources as $k=>$v){?>
					<div class="col-md-3">
						<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][courses][<?=$k?>]" value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </label><br />
					</div>
				<?php } */?>
				<div class="col-md-3">
					<select name="course_id" class="form-control" required>
						<option value=""> - Select Course - </option>
						<?php foreach($cources as $k=>$v){?>
							<option value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </option>
						<?php } ?>
					</select>
				</div>
			</div><br />
		</div>
		<div id="addmore_pricegroup">	
			<div class="pricegroup_id">
				<div class="col-md-12"><hr style="margin:-10px 0px 10px; border-color:#999;" /></div>
				
				<div class="col-md-2">
					<label class="control-label" for="dipName">Price From</label>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<div class="col-md-3">
							<?php echo form_input('price_groups[0][from_date]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker autocomplete="off" required ');?>
						</div>
						<div class="col-md-1">To</div>
						<div class="col-md-3">
							<?php echo form_input('price_groups[0][to_date]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker autocomplete="off" required ');?>
						</div>
					</div><br />
				</div>
				
				<div class="col-md-2">
					<label class="control-label" for="dipName">Time From</label>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<div class="col-md-3">
							<?php echo form_input('price_groups[0][from_time]', '', 'class="form-control timepicker" placeholder="00:00" ');?>
						</div>
						<div class="col-md-1">To</div>
						<div class="col-md-3">
							<?php echo form_input('price_groups[0][to_time]', '', 'class="form-control timepicker" placeholder="00:00" ');?>
						</div>
						<div class="col-md-12">If you remain these both time text boxes blank, it will be considered as Full Day setting.</div>
					</div><br />
				</div>
				
				<div class="col-md-2">
					<label class="control-label" for="dipName">Price</label>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<div class="col-md-1">
							<?php echo form_input('price_groups[0][price]', '', 'class="form-control" placeholder="0" required ');?>
						</div>
					</div><br />
				</div>
				
				<div class="col-md-2">
					<label class="control-label" for="dipName">Offer</label>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<div class="col-md-12">
							<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][offer]" value="1">&nbsp;</label>
						</div><br />
						<div class="col-md-2">
							<label class="control-label" for="dipName">Price</label>
						</div>
						<div class="col-md-10">
							<div class="form-group">
								<div class="col-md-2">
									<?php echo form_input('price_groups[0][offer_price]', '', 'class="form-control" placeholder="0" ');?>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<label class="control-label" for="dipName">Date</label>
						</div>
						<div class="col-md-10">
							<div class="form-group">
								<div class="col-md-3">
									<?php echo form_input('price_groups[0][offer_startdate]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker autocomplete="off" ');?>
								</div>
								<div class="col-md-1">To</div>
								<div class="col-md-3">
									<?php echo form_input('price_groups[0][offer_enddate]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker autocomplete="off" ');?>
								</div>
							</div>
						</div>
					</div><br />
				</div>

				<div class="col-md-2">
                    <label class="control-label" for="course">Day</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][monday]" value="1"> Mon </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][tuesday]" value="1"> Tue </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][wednesday]" value="1"> Wed </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][thursday]" value="1"> Thu </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][friday]" value="1"> Fri </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][saturday]" value="1"> Sat </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="price_groups[0][sunday]" value="1"> Sun </label><br />
                        </div>
                    </div><br />
                </div>
				
				<div class="col-md-12"><span class="btn btn-default remove_pricegroup hidden" style="float: right; line-height: 15px; margin: 10px 10px 20px;"> Remove </span></div>
			</div>
		</div>
		<div class="col-md-12"><span class="btn btn-default addnew" style="float: right;font-size:30px; line-height: 15px; margin: 10px 10px 20px;"> + </span></div>
		<div>&nbsp;</div>
			
    <div class="dip-form-foot text-center">
		<img style="display:none;" width="200" src="<?php echo base_url();?>assets/img/loading.gif" class="loadingimg"/>
      <?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
      <a href="<?php echo current_full_url(); ?>" class="btn btn-default btncancel" ><?php echo $this->lang->line('Cancel'); ?></a>
    </div>
  <!-- </form> -->
  <?php echo form_close();?>
</section>

<script type="text/javascript">    
  $(document).ready(function(){
		var section_index=0;
		$('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val(getTodaysDate(0));

		$(document).on('click', '.remove_pricegroup', function () {
            $(this).closest('div.pricegroup_id').remove();
        });

		$(document).on('click', '.addnew', function () {
            var section_index = $('div.pricegroup_id').length;
            var newDiv = $(".pricegroup_id:first").clone()
                .find(':input')
                .each(function () {
                    this.name = this.name.replace(/\[(\d+)\]/, function (str) {
                        return '[' + section_index + ']';
                    });
                })
                .end()
                .appendTo("#addmore_pricegroup");
            newDiv.find('.datepicker').each(function () {
                $(this).removeClass('hasDatepicker');
                $(this).attr('id','');
                $(this).removeAttr('data-datepicker');
                initDatePicker($(this));
            });
            newDiv.find('.remove_pricegroup').removeClass('hidden');
        });

		$(document).on('submit', '#newsubpricegroup_form', function () {
			var postdata = $("#newsubpricegroup_form").serialize();
			$('.btn-success').hide();
			$('.btncancel').hide();			
			$('.loadingimg').show();
						
			$.ajax({
			   url: "<?php echo site_url('golfclub/bookingonline/checkConflictBlockPrice/');?>",
			   type: 'POST',
			   data: postdata,
			   success: function(data){				   
					if(data == 1){
						alertify.alert("This setting will Overwrite old setting, so please select date range in which bookings are Not done");						
						
					} else if(data == 2){
						alertify.alert("Date range Overwrite, so please select different date range");						
					} else {
						$.ajax({
						   url: "<?php echo current_full_url(); ?>",
						   type: 'POST',
						   data: postdata,
						   success: function(data){
							   if(data == 1){
									alertify.alert("Price added successfully");
									window.location.href='<?php echo current_full_url(); ?>';
									return false;
								}else{alertify.alert("<?php echo $this->lang->line('error_delete'); ?>");}
						   },
						});
						
					}
					$('.btn-success').show();
					$('.btncancel').show();
					$('.loadingimg').hide();						
			   },
			});
			return false;
		});
		
	});
	function initDatePicker(element) {
        element.datepicker({dateFormat: 'dd-mm-yy'}).val('');
    }
    function getTodaysDate (val) {
        var t = new Date, day, month, year = t.getFullYear();
        if (t.getDate() < 10) {
            day = "0" + t.getDate();
        } else {
            day = t.getDate();
        }
        if ((t.getMonth() + 1) < 10) {
            month = "0" + (t.getMonth() + 1 - val);
        } else {
            month = t.getMonth() + 1 - val;
        }
        return (day + '-' + month + '-' + year);
    }
</script> 

