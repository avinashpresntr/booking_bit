<?php 
session_start();
$ajvc= isset($_SESSION["lang"]) && $_SESSION["lang"] != ''?$_SESSION["lang"].".js":'english.js';
?>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/i18n/'.$ajvc);?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/jquery.jqGrid.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
var PAGE_URL_START = "<?php echo site_url('golfclub/bookingonline/orders');?>";
var PAGE_URL_END = "";

var $grid = $("#dipgrid");
$(document).ready(function () {
    $grid.jqGrid({
        url:"<?=base_url('golfclub/bookingonline/get_orders')?>",
        datatype: "json",
        colNames:[
				'User Email',
				'Course Name' ,
				'Order Date',
				'Start Time',
				'Total Amount',
				'Order Details'
		],
        colModel: [
            { name: 'email',index: 'email',align:"left"},
            { name: 'name',index: 'name',align:"left",formatter: formatJson},
            { name: 'order_date',index: 'order_date',align:"center"},
            { name: 'start_time',index: 'start_time',align:"center"},
            { name: 'total_amount',index: 'total_amount',align:"center"},
            { name: 'id',index: 'id', width: 20,formatter:actionButtonFormatter}
            //{ name: 'id',index: 'deatils', width: 20,formatter: 'showlink',formatoptions:{baseLinkUrl:'javascript:',showAction:  "Link('", addParam: "');"}}
        ],
        rowNum:50,
        height:400,
        rowList:[10,20,50,100,500],
        pager: '#pager',
        //sortname: 'id',
        viewrecords: true,
        sortorder: "asc",
        altRows:true,
        autowidth: true,
        shrinkToFit: true,
        loadComplete:function(data){
            if(data.rows.length == 0){
				var html = "<div id='jqNoData' style='color:red;font-weight:bold;text-align:center;padding:10px;background:ghostwhite;'><?php echo $this->lang->line('Sorry No Records Found'); ?></div>";
                $grid.parent().append(html);
            }
        },
        beforeRequest: function() {
            $('#jqNoData').remove();
            responsive_jqgrid($("#gbox_dipgrid"));
        }
    });
    $grid.jqGrid('sortableRows',{ update : function(e,ui) {
        var id = ui.item[0].id;
        var position = ui.item[0].rowIndex;
        position_item(id,position);
    }});

    $grid.find(">tbody").sortable("disable");
});

function formatJson(cellValue, options, rowObject) {
    var obj = jQuery.parseJSON(cellValue.replace("'", "\'"));
    var dlang = <?=$client->default_language;?>;
    if (obj[dlang].length > 80) {
        return obj[dlang].substr(0, 80) + '...';
    } else {
        return obj[dlang];
    }
};

function formatAction1(cellValue, options, rowObject) {
    var e_btn = get_edit_link(BASE_URL+"golfclub/bookingonline/edit_order/"+cellValue);
    var d_btn = get_delete_btn('onclick="delete_item('+cellValue+');"');
    return e_btn+' '+d_btn;
};


//delete
function delete_item(id) {
    alertify.confirm("<?php echo $this->lang->line('delete_message'); ?>", function(e) {
        if (e) {
            var url = "<?php echo site_url('golfclub/bookingonline/delete_order/');?>";
            $.ajax({type: "POST", data: {id: id}, async: false, url: url, success: function(data) {
                if(data == 1){
                    $grid.trigger('reloadGrid');
                    alertify.alert("<?php echo $this->lang->line('success_delete'); ?>");
                }else{alertify.alert("<?php echo $this->lang->line('error_delete'); ?>");}
            }});
        }
    });
}

function actionButtonFormatter(cellvalue, options, rowObject) {
	//return buttonLink = "<span id=\"order_details_form\" onclick=\"orderDetails("+cellvalue+");\" >Order Deatils</span>";
	/*var url = "<?php echo base_url(); ?>golfclub/bookingonline/get_order_details";
	$.ajax({
			 type: "POST",
			 url: url,
			 data:{id: cellvalue},
			 async:false,
			 success: function(data) {
				 //alert(data);
             }
          });*/
	//return buttonLink = '<span id="order_details_form" onclick="orderDetails("+cellvalue+")" >Order Deatils</span>';
	//return buttonLink = '<span id="order_details_form" data-toggle="modal" data-target="#myModal" >Order Deatils</span> <div class="modal fade" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">x</button><h4 class="modal-title">Order Details</h4></div><div class="modal-body"><p><?php echo "Details To Display"; echo $user_deatils;?></p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
    return buttonLink = '<button type="button" class="btn btn-success" id="order_details_form" data-toggle="modal" data-target="#order_model" onclick="orderDetails('+cellvalue+')">Order Deatils</button>';
    //return buttonLink = '<button type="button" class="btn btn-success" id="order_details_form" data-toggle="modal" data-target="#order_model">Order Deatils</button>';
}

function orderDetails(id){
		var url = "<?php echo base_url(); ?>golfclub/bookingonline/get_order_details";
		 $.ajax({
			 type: "POST",
			 url: url,
			 data:{id: id},
			 async:false,
			 success: function(data) {
				$('#order_model .modal-body').html(data);
             }
          });
	
}

$(document).ready(function () {
  $('#jqgh_dipgrid_name').css({
    'text-align': 'left'
  });
});
</script>
