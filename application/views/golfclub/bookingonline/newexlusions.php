<section class="dip-dash-sec">
  <h3><?=$page['desc'];?></h3>
  <?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
    <?php //echo get_alerts('golfclub/courses', 'Course'); ?>
    <p class="error_msg"> <?php //echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body">
		<?php echo validation_errors(); ?>
        <div class="col-sm-12">
          <!-- Tab panes -->
          <div class="tab-content">

            <?php foreach ($client->languages as $key => $value):
              $active='';
              $required = '';
              if($client->default_language == $value)
              {
                $active='active';
				$required = 'required';
              }
              ?>
              <div role="tabpanel" class="tab-pane row <?=$active;?>" id="<?=$langs[$value];?>">
				<div class="col-sm-6">
				  <div class="child-box">
					<div class="form-group">
						<label class="col-sm-4 control-label" for="dipFormat">Exclusion Name</label>
						<div class="col-sm-8">
							<?php echo form_input('setting_name['.$value.']', set_value('setting_name['.$value.']',(isset($row->format[$value])?$row->format[$value]:'')), 'class="form-control" id="setting_name" placeholder="Exclusion Name '.$langs[$value].'" '. $required );?>
						</div>
					</div>
				  </div>
				</div>
              </div>
            <?php endforeach; ?>
          </div><br />
        </div>

		<div class="col-md-2">
			<label class="control-label" for="course"><?php echo 'Course';//echo $this->lang->line('EventName'); ?></label>
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<?php /*foreach($cources as $k=>$v){?>
					<div class="col-md-3">
						<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][courses][<?=$k?>]" value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </label><br />
					</div>
				<?php } */?>
				<div class="col-md-3">
					<select name="course_id" class="form-control" required>
						<option value=""> - Select Course - </option>
						<?php foreach($cources as $k=>$v){?>
							<option value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </option>
						<?php } ?>
					</select>
				</div>
			</div><br />
		</div>
        
			<div id="addmore_exlusions">
				<div class="exlusions_id">
					<div class="col-md-12"><hr style="margin:-10px 0px 10px; border-color:#999;" /></div>
					
					<div class="col-md-12"><hr style="margin:-10px 0px 10px; border-color:#999;" /></div>
					
					<div class="col-md-2">
						<label class="control-label" for="course">Price Group</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-3">
								<select class="form-control select_member_type" name="player[1][pricegroup_id]" id="booking_form_member_1_type_select">
									<optgroup label="Visiteurs g" class="grp_visitors">
										<option value="2" data-cost="0" data-offer_price="60" data-price="70">Visiteurs</option>
										<option value="2" data-cost="0" data-offer_price="60" data-price="70">Juniours</option>
									</optgroup>
									<optgroup label="Member g" class="grp_visitors">
										<option value="2" data-cost="0" data-offer_price="60" data-price="70">Member</option>
									</optgroup>
									<optgroup label="Options g" class="grp_visitors">
										<option value="2" data-cost="0" data-offer_price="60" data-price="70">Golf cart</option>
										<option value="2" data-cost="0" data-offer_price="60" data-price="70">Trolley</option>
									</optgroup>
								</select>
							</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="dipName">Date</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-3">
								<?php echo form_input('exlusions[0][from_date]', '', 'class="form-control datepicker_form" placeholder="00-00-0000" data-datepicker ');?>
							</div>
							<div class="col-md-1">To</div>
							<div class="col-md-3">
								<?php echo form_input('exlusions[0][end_date]', '', 'class="form-control datepicker_form" placeholder="00-00-0000" data-datepicker ');?>
							</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="dipName">Time</label>
					</div>
					
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-3">
								<?php echo form_input('exlusions[0][starttime]', '', 'class="form-control timepicker_form" placeholder="00:00" data-timepicker');?>
							</div>
							<div class="col-md-1">To</div>
							<div class="col-md-3">
								<?php echo form_input('exlusions[0][endtime]', '', 'class="form-control timepicker_form" placeholder="00:00" ');?>
							</div>
							<div class="col-md-12">If you remain these both time text boxes blank, it will be considered as Full Day setting.</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="course">Day</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-3">
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][monday]" value="1"> Mon </label><br />
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][tuesday]" value="1"> Tue </label>
							</div>
							<div class="col-md-3">
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][wednesday]" value="1"> Wed </label><br />
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][thursday]" value="1"> Thu </label>
							</div>
							<div class="col-md-3">
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][friday]" value="1"> Fri </label><br />
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][saturday]" value="1"> Sat </label>
							</div>
							<div class="col-md-3">
								<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="exlusions[0][sunday]" value="1"> Sun </label><br />
							</div>
						</div><br />
					</div>
					<div class="col-md-12"><span class="btn btn-default remove_exlusions hidden" style="float: right; line-height: 15px; margin: 10px 10px 20px;"> Remove </span></div>
				</div>
			</div>
			<div class="col-md-12"><span class="btn btn-default addnew" style="float: right;font-size:30px; line-height: 15px; margin: 10px 10px 20px;"> + </span></div>
			<div>&nbsp;</div>
			
    <div class="dip-form-foot text-center">
      <?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
      <a href="<?php echo site_full_url('golfclub/courses');?>" class="btn btn-default" ><?php echo $this->lang->line('Cancel'); ?></a>
    </div>
  <!-- </form> -->
  <?php echo form_close();?>
</section>

<script type="text/javascript">    
  $(document).ready(function(){
		$('.datepicker_form').datepicker({ dateFormat: 'dd-mm-yy' }).val(getTodaysDate(0));

	  	$(document).on('click', '.remove_exlusions', function () {
			$(this).closest('div.exlusions_id').remove();
	  	});

		$(document).on('click', '.addnew', function() {
			var section_index = $('div.exlusions_id').length;
			var newDiv = $("div.exlusions_id:first").clone()
			.find(':input')
			.each(function () {
				this.name = this.name.replace(/\[(\d+)\]/, function (str) {
					return '[' + section_index + ']';
				});
			})
			.end()
			.appendTo( "#addmore_exlusions" );
			newDiv.find('.datepicker_form').each(function () {
                $(this).removeClass('hasDatepicker');
                $(this).attr('id','');
                $(this).removeAttr('data-datepicker');
                initDatePicker($(this));
            });
			newDiv.find('.remove_exlusions').removeClass('hidden');
		});
	});
	
	function initDatePicker(element) {
        element.datepicker({dateFormat: 'dd-mm-yy'}).val(getTodaysDate(0));
    }
    function getTodaysDate (val) {
        var t = new Date, day, month, year = t.getFullYear();
        if (t.getDate() < 10) {
            day = "0" + t.getDate();
        } else {
            day = t.getDate();
        }
        if ((t.getMonth() + 1) < 10) {
            month = "0" + (t.getMonth() + 1 - val);
        } else {
            month = t.getMonth() + 1 - val;
        }
        return (day + '-' + month + '-' + year);
    }
</script> 
