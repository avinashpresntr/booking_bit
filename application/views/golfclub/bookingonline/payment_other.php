<!------------------      ckeditor     ------------------>
	<script src="<?php echo base_url();?>assets/plugin/ckeditor/ckeditor.js"></script>
<section class="dip-dash-sec">
  <h3>
		<?=$page['desc'];?> 
	</h3>
  <?php echo form_open_multipart(current_full_url(),array('class'=>"dip-form form-horizontal"));?>
	<input type="hidden" name="save_payment_settings" value="1">
    <?php echo get_alerts('golfclub/events', 'Event'); ?>
    <p class="error_msg"> <?php echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body">
			
			<fieldset>
				<legend>Payment Options</legend>
				<div class="row ">
					<input type="hidden" name="payment_mode_id" value="<?=$payment_mode->payment_mode_id?>">
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-md-2">
								<label class="control-label"
									   for="dipFormat"><?= isset($payment_mode->payment_mode) ? $payment_mode->payment_mode : 'PayPal' ?></label>
							</div>
							<div class="col-md-10">
								<div>
									<label class="checkbox-inline  chk-fac clearfix">
										<input type="checkbox"
											<?= isset($payment_mode->is_active) && $payment_mode->is_active == 1 ?'checked':'';?>
											   name="is_active"
											   value="1">
										<?= isset($payment_mode->payment_mode) ? $payment_mode->payment_mode : 'PayPal' ?>
									</label>
								</div>
								<br/>
								<div>
									<div><label class="control-label" for="dipFormat">Enter your PayPal
											email
											address </label></div>
									<?php echo form_input('payment_email', isset($payment_mode->payment_email) ? $payment_mode->payment_email : '', 'class="form-control" id="dipFormat" placeholder="Enter your PayPal email address "'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-md-10">
								<?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
								<a href="<?php echo site_full_url('golfclub/events');?>" class="btn btn-default" ><?php echo $this->lang->line('Cancel'); ?></a>
							</div>
						</div>
					</div>

				</div>
			</fieldset>
		</div>
  <!-- </form> -->
  <?php echo form_close();?>
	<br />
	<?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
	<input type="hidden" name="save_logo_background" value="1">
    <?php echo get_alerts('golfclub/events', 'Event'); ?>
    <p class="error_msg"> <?php echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body">
			
			<fieldset>
				<legend>Logo & Background</legend>
				<div class="row ">					
					<div class="col-md-2">
						<label class="control-label" for="logo">Logo</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-6">
								<input type="file" name="logoimg" />
							</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="content">Background</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-12">
								<input type="file" name="backgroundimg" />
							</div>
						</div><br />
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-md-10">
								<?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
								<a href="<?php echo site_full_url('golfclub/events');?>" class="btn btn-default" ><?php echo $this->lang->line('Cancel'); ?></a>
							</div>
						</div>
					</div>
					
				</div>
			</fieldset>
		</div>
  <!-- </form> -->
  <?php echo form_close();?>
	<?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
	<input type="hidden" name="save_terms_condition" value="1">
	<input type="hidden" name="terms_condition_id" value="<?=$terms_condition->id;?>">
    <?php echo get_alerts('golfclub/events', 'Event'); ?>
    <p class="error_msg"> <?php echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body">
			
			<fieldset>
				<legend>Terms & Conditions</legend>
				<div class="row ">
					
					<div class="col-md-2">
						<label class="control-label" for="title">Title</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-6">
								<?php echo form_input('title',isset($terms_condition->title)?$terms_condition->title:'', 'class="form-control" id="title" placeholder="" ');?>
							</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="content">Contant</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-12">
								<?php echo form_textarea(array('name'=>'term_content','value'=>isset($terms_condition->title)?$terms_condition->content:'')); ?>
							</div>
						</div><br />
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-md-10">
								<?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
								<a href="<?php echo site_full_url('golfclub/events');?>" class="btn btn-default" ><?php echo $this->lang->line('Cancel'); ?></a>
							</div>
						</div>
					</div>
					
				</div>
			</fieldset>
		</div>
  <!-- </form> -->
  <?php echo form_close();?>
	<br />
	<?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
	<input type="hidden" name="save_contact_us" value="1">
	<input type="hidden" name="contact_us_id" value="<?=$contact_us->id;?>">
    <?php echo get_alerts('golfclub/events', 'Event'); ?>
    <p class="error_msg"> <?php echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body">
			
			<fieldset>
				<legend>Contact Us</legend>
				<div class="row ">
					
					<div class="col-md-2">
						<label class="control-label" for="title">Title</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-6">
								<?php echo form_input('title',isset($contact_us->title)?$contact_us->title:'', 'class="form-control" id="title" placeholder="" ');?>
							</div>
						</div><br />
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="content">Content</label>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<div class="col-md-12">
								<?php echo form_textarea(array('name'=>'contactus_content','value'=>isset($contact_us->content)?$contact_us->content:'')); ?>
							</div>
						</div><br />
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-md-10">
								<?php echo form_submit('submit', $this->lang->line('save'),'class="btn btn-success"'); ?>&nbsp;
								<a href="<?php echo site_full_url('golfclub/events');?>" class="btn btn-default" ><?php echo $this->lang->line('Cancel'); ?></a>
							</div>
						</div>
					</div>

				</div>
			</fieldset>
		</div>
  <!-- </form> -->
  <?php echo form_close();?>
	
</section>
<script>
	CKEDITOR.replace( 'term_content' );
	CKEDITOR.replace( 'contactus_content' );
</script>  
