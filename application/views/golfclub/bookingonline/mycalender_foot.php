<script type="text/javascript">
    var PAGE_URL_START = "<?php echo site_url('golfclub/nexttee');?>";
    var PAGE_URL_END = "";
    <?php if($session['type'] <2):?>
    PAGE_URL_END = "?user=<?=$user['id'];?>";
    <?php endif;?>
    $(document).ready(function () {
        $('#full_calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: '2016-11-14',
            navLinks: true, // can click day/week names to navigate views
            editable: false,
            eventLimit: 6, // allow "more" link when too many events
            //eventLimitText: 'More',
            events: <?=$page['calendar_data'];?>
            /*events: [
                {
                    title: '01:00:00',
                    url: 'javascript:void(0)',
                    start: '2016-11-14'
                },
                {
                    title: 'Long Event',
                    url: 'javascript:void(0)',
                    start: '2016-11-07',
                    end: '2016-11-10'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    url: 'javascript:void(0)',
                    start: '2016-11-09T16:00:00'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-11-16T16:00:00'
                },
                {
                    title: 'Conference',
                    start: '2016-11-11',
                    end: '2016-11-13'
                },
                {
                    title: 'Meeting',
                    start: '2016-11-12T10:30:00',
                    end: '2016-11-12T12:30:00'
                },
                {
                    title: 'Lunch',
                    start: '2016-11-12T12:00:00'
                },
                {
                    title: 'Meeting',
                    start: '2016-11-12T14:30:00'
                },
                {
                    title: 'Happy Hour',
                    start: '2016-11-12T17:30:00'
                },
                {
                    title: 'Happy Hour 123',
                    start: '2016-11-12T17:35:00'
                },
                {
                    title: 'Dinner',
                    start: '2016-11-12T20:00:00'
                },
                {
                    title: 'Birthday Party',
                    start: '2016-11-13T07:00:00'
                },
                {
                    title: 'Click for Google',
                    url: 'http://google.com/',
                    start: '2016-11-28'
                }
            ]*/
        });
    });
</script>
