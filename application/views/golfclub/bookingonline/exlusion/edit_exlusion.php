<section class="dip-dash-sec">
    <h3><?= $page['desc']; ?></h3>
    <?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"'); ?>
    <?php //echo get_alerts('golfclub/courses', 'Course'); ?>
    <p class="error_msg"> <?php //echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body" id="calendarsettingform_id">
        <?php echo validation_errors(); ?>
        <div class="col-md-2">
            <label class="control-label" for="dipName">Setting Name</label>
        </div>
        <div class="col-md-10">
            <div class="form-group">
                <div class="col-md-4">
                    <?php echo form_input('setting_name',$calendarsetting_data[0]->setting_name, 'class="form-control" id="setting_name" placeholder="Setting Name" '); ?>
                </div>
            </div>
            <br/>
        </div>

        <div id="addmore_calendarsetting">
            <?php
                $section_array = array();
                foreach($calendarsetting_data as $calendarsetting_row) {
                    if(!in_array($calendarsetting_row->section,$section_array)) {
                        ?>
                        <div class="calendarsetting_id">
                            <div class="col-md-12">
                                <hr style="margin:-10px 0px 10px; border-color:#999;"/>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label"
                                       for="course"><?php echo 'Course';//echo $this->lang->line('EventName');
                                    ?></label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <?php foreach ($cources as $k => $v) { ?>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox"
                                                                                                    <?= $calendarsetting_row->course_id; ?>checked
                                                                                                    name="courses[0][]"
                                                                                                    value="<?php echo $v['id'] ?>"> <?php echo $v['cource_name'] ?>
                                            </label><br/>
                                        </div>
                                    <?php } ?>
                                </div>
                                <br/>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label" for="dipName">Booking Available</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <?php echo form_input('start_date[0][]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker '); ?>
                                    </div>
                                    <div class="col-md-1">To</div>
                                    <div class="col-md-3">
                                        <?php echo form_input('end_date[0][]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker '); ?>
                                    </div>
                                </div>
                                <br/>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label" for="dipName">Open Time</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <?php echo form_input('start_time[0][]', '', 'class="form-control timepicker" placeholder="00:00" '); ?>
                                    </div>
                                    <div class="col-md-2">Close Time</div>
                                    <div class="col-md-3">
                                        <?php echo form_input('end_time[0][]', '', 'class="form-control timepicker" placeholder="00:00" '); ?>
                                    </div>
                                </div>
                                <br/>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label" for="dipName">Slot Period</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <?php echo form_input('slot_period[0][]', '10', 'class="form-control" placeholder="" '); ?>
                                    </div>
                                    <div class="col-md-1">Minutes</div>
                                </div>
                                <br/>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label" for="dipName">Slot Players</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <?php echo form_input('slot_players[0][]', '4', 'class="form-control" placeholder="" '); ?>
                                    </div>
                                </div>
                                <br/>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label" for="course">Day</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="monday"> Mon
                                        </label><br/>
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="tuesday"> Tue
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="wednesday"> Wed
                                        </label><br/>
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="thursday"> Thu
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="friday"> Fri
                                        </label><br/>
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="saturday"> Sat
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked
                                                                                                name="day[0][]"
                                                                                                value="sunday"> Sun
                                        </label><br/>
                                    </div>
                                </div>
                                <br/>
                            </div>
                            <div class="col-md-12"><span class="btn btn-default remove_calendarsetting"
                                                         style="float: right; line-height: 15px; margin: 10px 10px 20px;"> Remove </span>
                            </div>
                        </div>
                        <?php
                        $section_array[] = $calendarsetting_row->section;
                    }
                }
            ?>
        </div>
        <div class="col-md-12"><span class="btn btn-default addnew"
                                     style="float: right;font-size:30px; line-height: 15px; margin: 10px 10px 20px;"> + </span>
        </div>
        <div>&nbsp;</div>

        <div class="dip-form-foot text-center">
            <?php echo form_submit('submit', $this->lang->line('save'), 'class="btn btn-success"'); ?>&nbsp;
            <a href="<?php echo site_full_url('golfclub/courses'); ?>"
               class="btn btn-default"><?php echo $this->lang->line('Cancel'); ?></a>
        </div>
        <!-- </form> -->
        <?php echo form_close(); ?>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        var section_index = 0;
        $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
        /*$(".timepicker").timepicker({
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });*/

        $(document).on('click', '.remove_calendarsetting', function () {
            $(this).closest('div.calendarsetting_id').remove();
        });

        $(document).on('click', '.addnew', function () {
            //$(".addnew").live('click',function(e) {
            section_index++;
            var newDiv = $(".calendarsetting_id").clone()
                .find(':input')
                .each(function () {
                    this.name = this.name.replace(/\[(\d+)\]/, function (str) {
                        return '[' + section_index + ']';
                    });
                })
                .end()
                .appendTo("#addmore_calendarsetting");
            newDiv.find('.datepicker').each(function () {
                $(this).removeClass('hasDatepicker');
                $(this).attr('id','');
                $(this).removeAttr('data-datepicker');
                initDatePicker($(this));
            });
        });
    });

    function initDatePicker(element) {
        element.datepicker({dateFormat: 'yy-mm-dd'});
    }
</script> 
