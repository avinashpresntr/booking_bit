<?php
session_start();
$ajvc= isset($_SESSION["lang"]) && $_SESSION["lang"] != ''?$_SESSION["lang"].".js":'english.js';
?>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/i18n/'.$ajvc);?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/vendors/jqGrid/js/jquery.jqGrid.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
var PAGE_URL_START = "<?php echo site_url('golfclub/bookingonline');?>";
var PAGE_URL_END = "";
var $grid = $("#dipgrid");
$(document).ready(function () {
    $grid.jqGrid({
        url:"<?php echo base_url('golfclub/bookingonline/get_calendarsetting/');?>",
        datatype: "json",
        colNames:['<?php echo $this->lang->line('CalendarSetting'); ?>', '<?php echo $this->lang->line('Action'); ?> '],
        colModel: [
            /*{ label:'id', name:'id', key: true, width: 25 },*/
            { label:'Settings', name: 'setting_name',align:"left", formatter: formatJson},
            { label:'Action', name: 'id', width: 20,formatter: formatAction1}
        ],
        rowNum:50,
        height:400,
        rowList:[10,20,50,100,500],
        pager: '#pager',
        sortname: 'position',
        viewrecords: true,
        sortorder: "asc",
        altRows:true,
        autowidth: true,
        shrinkToFit: true,
        loadComplete:function(data){
            if(data.rows.length == 0){
				var html = "<div id='jqNoData' style='color:red;font-weight:bold;text-align:center;padding:10px;background:ghostwhite;'><?php echo $this->lang->line('Sorry No Records Found'); ?></div>";
                $grid.parent().append(html);
            }
        },
        beforeRequest: function() {
            $('#jqNoData').remove();
            responsive_jqgrid($("#gbox_dipgrid"));
        }
    });
    $grid.jqGrid('sortableRows',{ update : function(e,ui) {
        var id = ui.item[0].id;
        var position = ui.item[0].rowIndex;
        position_item(id,position);
    }});

    $grid.find(">tbody").sortable("disable");
});

function formatJson(cellValue, options, rowObject) {
    var obj = jQuery.parseJSON(cellValue.replace("'", "\'"));
    var dlang = <?=$client->default_language;?>;
    if (obj[dlang].length > 80) {
        return obj[dlang].substr(0, 80) + '...';
    } else {
        return obj[dlang];
    }
};

function formatAction1(cellValue, options, rowObject) {
    var e_btn = get_edit_link(BASE_URL+"golfclub/bookingonline/edit_calendarsetting/"+cellValue);
    var d_btn = get_delete_btn('onclick="delete_item('+cellValue+');"');
    return e_btn+' '+d_btn;
};


//delete
function delete_item(id) {
    alertify.confirm("<?php echo $this->lang->line('delete_message'); ?>", function(e) {
        if (e) {
            var url = "<?php echo site_url('golfclub/bookingonline/delete_calendarsetting/');?>";
            $.ajax({type: "POST", data: {id: id}, async: false, url: url, success: function(data) {
				if(data == 1){
					$grid.trigger('reloadGrid');
                    alertify.alert("<?php echo $this->lang->line('success_delete'); ?>");
				}else if(data == 2){
					alertify.alert("Some bookings done in this date range, so can Not be removed");
					return false;
				}else{alertify.alert("<?php echo $this->lang->line('error_delete'); ?>");}
            }});
        }
    });
}

$(document).ready(function () {
  $('#jqgh_dipgrid_name').css({
    'text-align': 'left'
  });
});
</script>
