<section class="dip-dash-sec">
	<div class="dip-form-body">
		<div class="col-md-6 col-sm-6">
			<h3><?php echo $page['desc']; ?></h3>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<h3 class="col-md-6 contro-label text-right">Course</h3>
				<div class="col-md-6">
					<select class="form-control input-sm1" id="parcours">
						<?php foreach($page['courses_list'] as $row):?>
							<option <?php echo $this->session->userdata("home_booking_course_id") == $row['id'] ? 'selected':''?> value="<?php echo $row['id'];?>" ><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="<?=base_url()?>golfclub/bookingonline/mycalender">MY CALENDER</a>
				</li>
				<li>
					<a href="<?=base_url()?>golfclub/bookingonline/orders">Orders</a>
				</li>
			</ul>
		</div>	
	</div>

	<?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
	<div class="row dip-form-body">
		<div class="col-sm-12">
			<div class="row">
				<div id='full_calendar'></div>
			</div>
		</div>
	</div>
</section>
