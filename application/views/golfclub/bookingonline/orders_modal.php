<div class="row text-center">
    <div class="col-lg-3">DATE</div>
    <div class="col-lg-3">HOUR</div>
    <div class="col-lg-3">COURSE</div>
    <div class="col-lg-3">OPTIONS</div>
</div>
<div class="row text-center">
    <div class="col-lg-3"><strong><?php echo date('l, F j, Y',strtotime($block_date))?></strong></div>
    <div class="col-lg-3"><strong><?php $tapkoo = date('H:i', strtotime($start_time));echo substr($tapkoo,0,2)."H".substr($tapkoo,3,2);?></strong></div>
    <div class="col-lg-3"><strong><?php echo $course_name;?></strong></div>
    <div class="col-lg-3"><strong><?php echo $course_holes;?></strong></div>
</div>
<hr/>
<div class="row text-center">
    <div class="col-lg-3 text-left">PLAYERS</div>
    <div class="col-lg-3">HCP</div>
    <div class="col-lg-3">PRICE GROUP</div>
    <div class="col-lg-3 text-right">PRICE</div>
</div>
<?php foreach($player_data as $p => $q){ ?>
<div class="row text-center">
    <div class="col-lg-3 text-left"><strong><?php echo $p+1 .'   '. $q['name'];?></strong></div>
    <div class="col-lg-3"><strong><?php echo $q['hcp'];?></strong></div>
    <div class="col-lg-3"><strong><?php echo $q['member'];?></strong></div>
    <div class="col-lg-3 text-right"><strong><?php echo $q['price'];?></strong></div>
</div>
<?php }?>

<hr/>
<div class="row">
    <div class="col-lg-8">OPTIONS</div>
    <div class="col-lg-2 text-center">QTY</div>
    <div class="col-lg-2 text-right">PRICE</div>
</div>
<?php foreach($option_data as $o_key => $o_value){ ?>
<div class="row">
    <div class="col-lg-8"><strong><?php echo $o_key+1 .'   '.$o_value['option_name'];?></strong></div>
    <div class="col-lg-2 text-center"><strong><?php echo $o_value['qty'];?></strong></div>
    <div class="col-lg-2 text-right"><strong><?php echo $o_value['price'];?></strong></div>
</div>
<?php }?>
<hr/>
<div class="row">
    <div class="col-lg-12 text-right"><h4 style="margin: 0;font-weight: bold">Total <?php echo $sub_total;?></h4></div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">User Detail</h3>
            </div>
        </div>
        <table class="table table-user-information">
            <tbody>
            <tr>
                <td><strong>Full Name:</strong></td>
                <td><?php echo $user_data['firstname'].' '.$user_data['lastname'];?></td>
            </tr>
            <tr>
                <td><strong>Country:</strong></td>
                <td><?php echo $user_data['country'];?></td>
            </tr>
            <tr>
                <td><strong>Address 1:</strong></td>
                <td><?php echo $user_data['address1'];?></td>
            </tr>
            <tr>
                <td><strong>Address 2:</strong></td>
                <td><?php echo $user_data['address2'];?></td>
            </tr>
            <tr>
                <td><strong>City:</strong></td>
                <td><?php echo $user_data['city'];?></td>
            </tr>
            <tr>
                <td><strong>Area:</strong></td>
                <td><?php echo $user_data['area'];?></td>
            </tr>
            <tr>
                <td><strong>Zip:</strong></td>
                <td><?php echo $user_data['zip'];?></td>
            </tr>
            <tr>
                <td><strong>Phone:</strong></td>
                <td><?php echo $user_data['phone'];?></td>
            </tr>
            <tr>
                <td><strong>Email Address:</strong></td>
                <td><?php echo $user_data['email'];?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>