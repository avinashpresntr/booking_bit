<section class="dip-dash-sec">
	<div class="dip-form-body">
		<div class="col-md-6 col-sm-6">
			<h3><?php echo $page['desc']; ?></h3>
		</div>
		<div class="col-sm-5 form-inline text-right">
            <input  class="form-control" type="text" id="muSearch" onkeydown="doSearch(arguments[0]||event)" placeholder="<?php echo $this->lang->line('Search'); ?>"/>
		</div>
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li>
					<a href="<?=base_url()?>golfclub/bookingonline/mycalender">MY CALENDER</a>
				</li>
				<li class="active">
					<a href="<?=base_url()?>golfclub/bookingonline/orders">Orders</a>
				</li>
			</ul>
		</div>	
	</div>

	<?php echo form_open_multipart(current_full_url(), 'class="dip-form form-horizontal"');?>
	<div class="row dip-form-body">
		<div class="col-sm-12">
			<br/>
			<table id="dipgrid"></table>
			<div id="pager"></div>
			<div id="popup"></div>
			<br/>
			<br/>
		</div>
	</div>
</section>

<!-- modal-->
<div class="modal fade" id="order_model" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  		<h4 class="modal-title">Order Details</h4>
      		</div>
      		<div class="modal-body">

      		</div>
      		<div class="modal-footer" style="text-align:center;">
        		<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('Cancel'); ?></button>
      		</div>
    	</div>
  	</div>
</div>
<!-- Over ---->
