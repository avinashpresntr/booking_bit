<section class="dip-dash-sec">
	<h3>Price Group<?php //echo $page['desc']; ?></h3>
	<div class="row dip-form-body">
		<div class="col-sm-12">
			<table id="pricegrouplist"></table>
			<div id="pager"></div>
			<br/>
		</div>
	</div>
</section>

<!-- first modal -->
<div class="modal" id="first_model" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="first_modelLabel">Add Price Group</h4>
      </div>
      <?php echo form_open_multipart(base_url().'golfclub/bookingonline/newpricegroup', 'class="dip-form form-horizontal"');?>
      <div class="modal-body">
        <!-- Item Edit Form -->
            <fieldset>
              <table class="dip-lang-table table table-striped"><tbody>
                <tr>
                  <th></th>
                  <th>Price Group </th>
                </tr>
                <?php foreach ($client->languages as $key => $value):?>
                  <?php
                    $default = '';
                    $required = '';
                    if($value==$client->default_language){
                      $default = 'default';
                      $required = 'required';
                    }
									?>
                  <tr class="<?=$default;?>">
                    <td><i class="flag flag-<?php echo $langs[$value];?>" alt="<?php echo $langs[$value];?>"></i></td>
                    <td><?php echo form_input('name['.$value.']','', 'class="form-control" id="dipName'.$value.'" placeholder="Add Price Group ('.$langs[$value].')" '.$required);?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody></table>
            </fieldset>
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="submit" class="btn btn-success"><?php echo $this->lang->line('save'); ?></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('Cancel'); ?></button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
