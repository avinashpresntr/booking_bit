<section class="dip-dash-sec">
    <h3><?= $page['desc']; ?></h3>
    <?php echo form_open_multipart(current_full_url(), ' id="newcalendarsetting_form" class="dip-form form-horizontal"'); ?>
    <?php //echo get_alerts('golfclub/courses', 'Course'); ?>
    <p class="error_msg"> <?php //echo $this->lang->line('FormValidation_fieldrequired'); ?> </p>
    <div class="dip-form-body" id="calendarsettingform_id">
        <?php echo validation_errors(); ?>
        <div class="col-sm-12">
          <!-- Tab panes -->
          <div class="tab-content">

            <?php foreach ($client->languages as $key => $value):
              $active='';
              $required = '';
              if($client->default_language == $value)
              {
                $active='active';
				$required = 'required';
              }
              ?>
              <div role="tabpanel" class="tab-pane row <?=$active;?>" id="<?=$langs[$value];?>">
				<div class="col-sm-6">
				  <div class="child-box">
					<div class="form-group">
						<label class="col-sm-4 control-label" for="dipFormat">Setting Name</label>
						<div class="col-sm-8">
							<?php echo form_input('setting_name['.$value.']', set_value('setting_name['.$value.']',(isset($row->format[$value])?$row->format[$value]:'')), 'class="form-control" id="setting_name" placeholder="Setting Name '.$langs[$value].'" '. $required );?>
						</div>
					</div>
				  </div>
				</div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>

        <div class="col-md-2">
			<label class="control-label" for="course"><?php echo 'Course';//echo $this->lang->line('EventName'); ?></label>
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<?php /*foreach($cources as $k=>$v){?>
					<div class="col-md-3">
						<label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][courses][<?=$k?>]" value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </label><br />
					</div>
				<?php } */?>
				<div class="col-md-3">
					<select name="course_id" class="form-control" required>
						<option value=""> - Select Course - </option>
						<?php foreach($cources as $k=>$v){?>
							<option value="<?php echo $v['id']?>"> <?php echo $v['cource_name']?> </option>
						<?php } ?>
					</select>
				</div>
			</div><br />
		</div>
        
        <div id="addmore_calendarsetting">
            <div class="calendarsetting_id">
                <div class="col-md-12"><hr style="margin:-10px 0px 10px; border-color:#999;" /></div>
                
                <div class="col-md-2">
                    <label class="control-label" for="dipName">Booking Available</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][booking_startdate]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker required autocomplete="off" ');?>
                        </div>
                        <div class="col-md-1">To</div>
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][booking_enddate]', '', 'class="form-control datepicker" placeholder="00-00-0000" data-datepicker required autocomplete="off" ');?>
                        </div>
                    </div><br />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="dipName">Open Time</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][opentime]', '', 'class="form-control timepicker" placeholder="00:00" required ');?>
                        </div>
                        <div class="col-md-2">Close Time</div>
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][closetime]', '', 'class="form-control timepicker" placeholder="00:00" required ');?>
                        </div>
                    </div><br />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="dipName">Slot Period</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][slot_period]', '10', 'class="form-control" placeholder="" required ');?>
                        </div>
                        <div class="col-md-1">Minutes</div>
                    </div><br />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="dipName">Block Players</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <?php echo form_input('calendar_setting[0][slot_player]', '4', 'class="form-control" placeholder="" required ');?>
                        </div>
                    </div><br />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="course">Day</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][monday]" value="1"> Mon </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][tuesday]" value="1"> Tue </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][wednesday]" value="1"> Wed </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][thursday]" value="1"> Thu </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][friday]" value="1"> Fri </label><br />
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][saturday]" value="1"> Sat </label>
                        </div>
                        <div class="col-md-3">
                            <label class="checkbox-inline  chk-fac clearfix"><input type="checkbox" checked name="calendar_setting[0][sunday]" value="1"> Sun </label><br />
                        </div>
                    </div><br />
                </div>
                <div class="col-md-12"><span class="btn btn-default remove_calendarsetting hidden" style="float: right; line-height: 15px; margin: 10px 10px 20px;"> Remove </span></div>
            </div>
        </div>
        <div class="col-md-12"><span class="btn btn-default addnew"
                                     style="float: right;font-size:30px; line-height: 15px; margin: 10px 10px 20px;"> + </span>
        </div>
        <div>&nbsp;</div>

        <div class="dip-form-foot text-center">
			<img style="display:none;" width="200" src="<?php echo base_url();?>assets/img/loading.gif" class="loadingimg"/>
            <?php echo form_submit('submit', $this->lang->line('save'), 'class="btn btn-success"'); ?>&nbsp;
           
            <a href="<?php echo current_full_url(); ?>"
               class="btn btn-default btncancel"><?php echo $this->lang->line('Cancel'); ?></a>
        </div>
        <!-- </form> -->
        <?php echo form_close(); ?>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datepicker({dateFormat: 'dd-mm-yy'}).val(getTodaysDate(0));

        $(document).on('click', '.remove_calendarsetting', function () {
            $(this).closest('div.calendarsetting_id').remove();
        });

        $(document).on('click', '.addnew', function () {
            var section_index = $('div.calendarsetting_id').length;
            var newDiv = $(".calendarsetting_id:first").clone()
                .find(':input')
                .each(function () {
                    this.name = this.name.replace(/\[(\d+)\]/, function (str) {
                        return '[' + section_index + ']';
                    });
                })
                .end()
                .appendTo("#addmore_calendarsetting");
            newDiv.find('.datepicker').each(function () {
                $(this).removeClass('hasDatepicker');
                $(this).attr('id','');
                $(this).removeAttr('data-datepicker');
                initDatePicker($(this));
            });
            newDiv.find('.remove_calendarsetting').removeClass('hidden');
        });

		$(document).on('submit', '#newcalendarsetting_form', function () {
			var postdata = $("#newcalendarsetting_form").serialize();
			$('.btn-success').hide();
			$('.btncancel').hide();			
			$('.loadingimg').show();
						
			$.ajax({
			   url: "<?php echo site_url('golfclub/bookingonline/checkConflictBlockDetaildata/');?>",
			   type: 'POST',
			   data: postdata,
			   success: function(data){				   
					if(data == 1){
						alertify.alert("This setting Overlaps other setting, so please select different date range");						
						$('.btn-success').show();
						$('.btncancel').show();
						$('.loadingimg').hide();
					} else if(data == 2){
						alertify.alert("Bookings are done in this date range, so please select date range in which bookings are Not done");
						$('.btn-success').show();
						$('.btncancel').show();
						$('.loadingimg').hide();
					} else if(data == 3){
						alertify.alert("This setting Overlaps other setting, so please select different date range");
						$('.btn-success').show();
						$('.btncancel').show();
						$('.loadingimg').hide();
					} else {
						$.ajax({
						   url: "<?php echo current_full_url(); ?>",
						   type: 'POST',
						   data: postdata,
						   success: function(data){
							   if(data == 1){
									alertify.alert("Calendar Setting added successfully");
									window.location.href='<?php echo base_url().'golfclub/bookingonline/'; ?>';
									$('.btn-success').show();
									$('.btncancel').show();
									$('.loadingimg').hide();
									return false;
								} else {
									alertify.alert("<?php echo $this->lang->line('error_delete'); ?>");
								}
						   },
						});
					}
			   },
			});
			return false;
		});
        
    });

    function initDatePicker(element) {
        element.datepicker({dateFormat: 'dd-mm-yy'}).val('');
    }
    function getTodaysDate (val) {
        var t = new Date, day, month, year = t.getFullYear();
        if (t.getDate() < 10) {
            day = "0" + t.getDate();
        } else {
            day = t.getDate();
        }
        if ((t.getMonth() + 1) < 10) {
            month = "0" + (t.getMonth() + 1 - val);
        } else {
            month = t.getMonth() + 1 - val;
        }
        return (day + '-' + month + '-' + year);
    }
</script> 
