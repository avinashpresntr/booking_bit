<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookingonline extends DIP_Controller_Golfclub {

	function __construct() {
		parent::__construct();
		$this->load->model('calendar_m');
		$this->load->model('exlusion_m');
		$this->load->model('pricegroup_m');
		$this->load->model('course_m');
		$this->load->model('crud_m');

		$this->visiteur_label_id = $this->config->item('visiteur_label_id');
		$this->visiteur_pricegroup_id = $this->config->item('visiteur_pricegroup_id');
		$this->options_parent_id = $this->config->item('options_parent_id');

		$sp2 = new Setting();
		$sp2->where('user_id', 1)->get();
		foreach ($sp2 as $s) {
			$this->data['settings'][$s->name] = decode_data($s->value);
		}
		$this->lang->load('message', $_SESSION["lang"]);
		$this->data['page'] = array(
			'title' => $this->lang->line('BookingOnline'),
			'slug' => 'bookingonline',
			'nav' => 'bookingonline',
			'desc' =>$this->lang->line('BookingOnline'),
			'main' => '_layout_backend',
		);
	 
		$this->data['page']['subnav'] = array(
			array(
				'title' => $this->lang->line('CalendarSetting'),
				'disabled' => false,
				'url' => site_full_url('golfclub/bookingonline'),
			),
			array(
				'title' => 'Price Group',
				'active' => false,
				'disabled' => false,
				'url' => site_full_url('golfclub/bookingonline/pricegroup'),
			),
			array(
				'title' => $this->lang->line('Exlusions'),
				'active' => false,
				'disabled' => false,
				'url' => site_full_url('golfclub/bookingonline/exlusions'),
			),
			array(
				'title' => $this->lang->line('MyCalendar'),
				'active' => false,
				'disabled' => false,
				'url' => site_full_url('golfclub/bookingonline/mycalender'),
			),
			array(
				'title' => $this->lang->line('PaymentandOtherSetting'),
				'active' => false,
				'disabled' => false,
				'url' => site_full_url('golfclub/bookingonline/payment_other'),
			),
		);

		// change the languages as per bookingonline settings
//		$sp1 = new Setting();
//		$sp1->where(array('user_id' => 1, 'name' => 'nt_languages'))->get();
//		$this->data['client']->languages = decode_data($sp1->value);

		$this->load->model('nexttee_m');

		$nu = new User();
		$userdata = $nu->get_by_id($this->data['user']['id']);
		if($userdata->type==1)
			$nh = new Golfgroup();
		elseif($userdata->type==2)
			$nh = new Golfclub();
		else
			$nh = new Partner();					
		$nh->where(array('user_id' => $this->data['user']['id']))->get();
		$query = $this->db->query("SELECT * from dip_languages WHERE id =".$nh->default_language); 
		$data = $query->result();
		/*$this->config->set_item('language', strtolower($data[0]->name));
		$this->lang->load('message',strtolower($data[0]->name));*/
		if(is_dir(FCPATH."application/language/".strtolower($data[0]->name)))
		{
			$this->config->set_item('language', strtolower($data[0]->name));
			$this->lang->load('message',strtolower($data[0]->name));
		}
		else
		{
			$this->config->set_item('language','english');
			$this->lang->load('message','english');
		}
	}

	public function index() {

		if ($this->input->post()) {
			$rules = array();
			foreach ($this->data['client']->languages as $key => $value) {
				$required_if = $this->input->post('descr')[$value] ? '|required' : '';
				$rules['descr[' . $value . ']'] = array(
					'field' => 'descr[' . $value . ']',
					'label' => 'Description for ' . $this->data['langs'][$value],
					'rules' => 'xss_clean'
				);
			}
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == TRUE) {

				$post = $this->input->post();
				$s = $this->nexttee_m->save_details($post, $this->data['user']['id']);

				//logs for activites
				if ($s == true) {
					if ($this->data['user']['id'] == $this->data['session']['id'])
						$this->user_m->log($this->data['user']['id'], '3');
					$this->session->set_flashdata('success_msg', $this->lang->line('message_save_success'));
					redirect(current_full_url());
				}
				// show($post);
			}
		}

		$this->data['client']->descr = decode_data($this->data['client']->descr);
		$this->data['client']->pics = decode_data($this->data['client']->pics);
		$this->data['client']->logo = decode_data($this->data['client']->logo);
		$this->data['client']->facilities = decode_data($this->data['client']->facilities);

		$this->load->model('xtra_m');
		$this->data['facilities'] = $this->xtra_m->get_all_facilities();

		$this->data['page']['desc'] = $this->lang->line('CalendarSetting');
		$this->data['page']['subview'] = 'golfclub/bookingonline/calendarsetting';
		$this->data['page']['foot'] = 'golfclub/bookingonline/calendar/calendar_foot';
		$this->data['page']['subnav'][0]['active'] = true;
		$this->load->view('_layout_main', $this->data);
		// show($this->data);
	}

	public function checkConflictBlockDetaildata() {
		if($this->input->post()){
			$post = $this->input->post();
			$course_id = $post['course_id'];
			$tempdate = array();
			foreach($post['calendar_setting'] as $key=>$calendar_setting){
				$booking_startdate = date("Y-m-d",strtotime($calendar_setting['booking_startdate']));
				$booking_enddate = date("Y-m-d",strtotime($calendar_setting['booking_enddate']));
				while (strtotime($booking_startdate) <= strtotime($booking_enddate)) {
					if(in_array($booking_startdate,$tempdate)){
						echo 3;
						exit;
					}
					$tempdate[] = $booking_startdate;										
					$booking_startdate = date ("Y-m-d", strtotime("+1 day", strtotime($booking_startdate)));					
				}
			}			
			foreach($post['calendar_setting'] as $key=>$calendar_setting){
				$calendar_detail = $calendar_setting;
				$booking_startdate = date('Y-m-d',strtotime($calendar_detail['booking_startdate']));
				$booking_enddate = date('Y-m-d',strtotime($calendar_detail['booking_enddate']));
				$opentime = date('H:i:00',strtotime($calendar_detail['opentime']));
				$closetime = date('H:i:00',strtotime($calendar_detail['closetime']));
				if(isset($calendar_detail['calendar_detail_id'])){
					$calendar_detail_id = $calendar_detail['calendar_detail_id'];
				} else {
					$calendar_detail_id = 0;
				}
				$conflit_result = $this->calendar_m->checkConflictBlockDetaildata($course_id, $calendar_detail_id, $booking_startdate, $booking_enddate, $opentime, $closetime);
				if($conflit_result == 1 || $conflit_result == 2){ print_r($conflit_result); exit; }
			}
			print_r($conflit_result); exit;
		}
	}
	public function newcalendarsetting() {
		if($this->input->post()){
			$rules = $this->calendar_m->rules;
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == TRUE){
				$post = $this->input->post();
				$setting_name = json_encode($post['setting_name']);
				$course_id = $post['course_id'];
				$last_id = $this->calendar_m->addCalendardata(array('setting_name' => $setting_name, 'course_id' => $course_id, 'last_update_time' => date('Y-m-d H:i:s')));
				$i = 0;
				foreach($post['calendar_setting'] as $key=>$calendar_setting){
					$calendar_detail = $calendar_setting;
					$block_detail = array();
					$booking_startdate = date('Y-m-d',strtotime($calendar_detail['booking_startdate']));
					$booking_enddate = date('Y-m-d',strtotime($calendar_detail['booking_enddate']));
					$opentime = date('H:i:00',strtotime($calendar_detail['opentime']));
					$closetime = date('H:i:00',strtotime($calendar_detail['closetime']));
					$calendar_detail['course_id'] = $course_id;
					$calendar_detail['calender_id'] = $last_id;
					$calendar_detail['booking_startdate'] = $booking_startdate;
					$calendar_detail['booking_enddate'] = $booking_enddate;
					$calendar_detail['opentime'] = $opentime;
					$calendar_detail['closetime'] = $closetime;
					$calendar_detail['section'] = $key+1;

					$week_days_allow = array();
					if(isset($calendar_detail['monday']) && $calendar_detail['monday'] == 1){
						$week_days_allow[] = 'monday';
					}
					if(isset($calendar_detail['tuesday']) && $calendar_detail['tuesday'] == 1){
						$week_days_allow[] = 'tuesday';
					}
					if(isset($calendar_detail['wednesday']) && $calendar_detail['wednesday'] == 1){
						$week_days_allow[] = 'wednesday';
					}
					if(isset($calendar_detail['thursday']) && $calendar_detail['thursday'] == 1){
						$week_days_allow[] = 'thursday';
					}
					if(isset($calendar_detail['friday']) && $calendar_detail['friday'] == 1){
						$week_days_allow[] = 'friday';
					}
					if(isset($calendar_detail['saturday']) && $calendar_detail['saturday'] == 1){
						$week_days_allow[] = 'saturday';
					}
					if(isset($calendar_detail['sunday']) && $calendar_detail['sunday'] == 1){
						$week_days_allow[] = 'sunday';
					}

					$calendar_detail_last_id = $this->calendar_m->addCalendarDetaildata($calendar_detail);
					$block = 0;
					while (strtotime($booking_startdate) <= strtotime($booking_enddate)) {
						$dateday = strtolower(date('l', strtotime($booking_startdate)));
						if(in_array($dateday, $week_days_allow)){
							$start_time = strtotime ($opentime);
							$bt = 0;
							while ($start_time < strtotime ($closetime)){
								$block_detail[$block][$bt]['start_time'] = date ("H:i:s", $start_time);
								//$end_time = $start_time + ($calendar_detail['slot_period'] * 60);
								//$block_detail[$block][$bt]['end_time'] = date ("H:i:s", $end_time);
								$start_time += $calendar_detail['slot_period'] * 60;

								$block_detail[$block][$bt]['course_id'] = $course_id;
								$block_detail[$block][$bt]['calender_id'] = $last_id;;
								$block_detail[$block][$bt]['calendar_detail_id'] = $calendar_detail_last_id;;
								$block_detail[$block][$bt]['block_date'] = $booking_startdate;
								//$block_detail[$block][$bt]['block_period'] = $calendar_detail['slot_period'];
								$block_detail[$block][$bt]['block_player'] = $calendar_detail['slot_player'];
								$this->calendar_m->addBlockDetaildata($block_detail[$block][$bt]);
								$bt++;
							}
						}
						$booking_startdate = date ("Y-m-d", strtotime("+1 day", strtotime($booking_startdate)));
						$block++;
					}
				}
				echo 1; //redirect(base_url().'golfclub/bookingonline/');
				exit;
			}
		}
		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v){
			$name = json_decode($v['name']);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$this->data['page']['desc'] = $this->lang->line('NewCalendarSetting');
		$this->data['page']['subview'] = 'golfclub/bookingonline/newcalendarsetting';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][0]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}

	public function get_calendarsetting() {
		$this->jqgrid->from('t_calender');
		$this->jqgrid->select('id, setting_name, last_update_time');
		$this->jqgrid->searchable('id, setting_name');
		echo escape_quote($this->jqgrid->get_result('json'));
	}

	function delete_calendarsetting(){
		$id = $_POST['id'];
		$data = $this->calendar_m->deleteCalendarsetting($id);
		print_r($data);
		exit;
	}

	function edit_calendarsetting($id){
		if(!isset($id)){
			redirect(base_url().'golfclub/bookingonline/');
			exit;
		}
		if($this->input->post()){
			$rules = $this->calendar_m->rules;
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == TRUE){
				$post = $this->input->post();
				$calendar_id = $post['calender_id'];
				$setting_name = json_encode($post['setting_name']);
				$course_id = $post['course_id'];
				$this->calendar_m->updateCalendardata($calendar_id, array('setting_name' => $setting_name, 'course_id' => $course_id, 'last_update_time' => date('Y-m-d H:i:s')));
				$i = 0;
				foreach($post['calendar_setting'] as $key=>$calendar_setting)
				{
					//echo '<pre>'; print_r($calendar_setting); exit;
					$calendar_detail = $calendar_setting;
					$block_detail = array();
					$booking_startdate = date('Y-m-d',strtotime($calendar_detail['booking_startdate']));
					$booking_enddate = date('Y-m-d',strtotime($calendar_detail['booking_enddate']));
					$opentime = date('H:i:00',strtotime($calendar_detail['opentime']));
					$closetime = date('H:i:00',strtotime($calendar_detail['closetime']));
					$calendar_detail['course_id'] = $course_id;
					$calendar_detail['calender_id'] = $calendar_id;
					$calendar_detail['booking_startdate'] = $booking_startdate;
					$calendar_detail['booking_enddate'] = $booking_enddate;
					$calendar_detail['opentime'] = $opentime;
					$calendar_detail['closetime'] = $closetime;
					$calendar_detail['section'] = $key+1;

					$week_days_allow = array();
					if(isset($calendar_detail['monday']) && $calendar_detail['monday'] == 1){
						$week_days_allow[] = 'monday';
					}
					if(isset($calendar_detail['tuesday']) && $calendar_detail['tuesday'] == 1){
						$week_days_allow[] = 'tuesday';
					}
					if(isset($calendar_detail['wednesday']) && $calendar_detail['wednesday'] == 1){
						$week_days_allow[] = 'wednesday';
					}
					if(isset($calendar_detail['thursday']) && $calendar_detail['thursday'] == 1){
						$week_days_allow[] = 'thursday';
					}
					if(isset($calendar_detail['friday']) && $calendar_detail['friday'] == 1){
						$week_days_allow[] = 'friday';
					}
					if(isset($calendar_detail['saturday']) && $calendar_detail['saturday'] == 1){
						$week_days_allow[] = 'saturday';
					}
					if(isset($calendar_detail['sunday']) && $calendar_detail['sunday'] == 1){
						$week_days_allow[] = 'sunday';
					}
					$this->calendar_m->deleteCalendarDetaildata($calendar_detail['calendar_detail_id']);
					$this->calendar_m->deleteCalendarDetailBlockDetaildata($calendar_detail['calendar_detail_id']);
					unset($calendar_detail['calendar_detail_id']);

					$calendar_detail_last_id = $this->calendar_m->addCalendarDetaildata($calendar_detail);
					$block = 0;
					while (strtotime($booking_startdate) <= strtotime($booking_enddate)) {
						$dateday = strtolower(date('l', strtotime($booking_startdate)));
						if(in_array($dateday, $week_days_allow)){
							$start_time = strtotime ($opentime);
							$bt = 0;
							while ($start_time < strtotime ($closetime)){
								$block_detail[$block][$bt]['start_time'] = date ("H:i:s", $start_time);
								//$end_time = $start_time + ($calendar_detail['slot_period'] * 60);
								//$block_detail[$block][$bt]['end_time'] = date ("H:i:s", $end_time);
								$start_time += $calendar_detail['slot_period'] * 60;

								$block_detail[$block][$bt]['course_id'] = $course_id;
								$block_detail[$block][$bt]['calender_id'] = $calendar_id;;
								$block_detail[$block][$bt]['calendar_detail_id'] = $calendar_detail_last_id;;
								$block_detail[$block][$bt]['block_date'] = $booking_startdate;
								//$block_detail[$block][$bt]['block_period'] = $calendar_detail['slot_period'];
								$block_detail[$block][$bt]['block_player'] = $calendar_detail['slot_player'];
								$this->calendar_m->addBlockDetaildata($block_detail[$block][$bt]);
								$bt++;
							}
						}
						$booking_startdate = date ("Y-m-d", strtotime("+1 day", strtotime($booking_startdate)));
						$block++;
					}
				}
				echo 1; //redirect(base_url().'golfclub/bookingonline/');
				exit;
			}
		}
		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v){
			$name = json_decode($v['name']);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$calendarsetting_data = $this->calendar_m->getCalendarsettingData($id);
		$calendarsetting_data_new = array();
		$section_array = array();
		$section_course_ids = array();
		$i=0;
		foreach($calendarsetting_data as $calendarsetting_row){
			if(!in_array($calendarsetting_row['section'],$section_array)){
				$calendarsetting_data_new[] = $calendarsetting_row;
				$section_array[] = $calendarsetting_row['section'];
			}
			$course_id = $calendarsetting_row['course_id'];
		}
		$this->data['section_array'] = $section_array;
		$this->data['course_id'] = $course_id;
		$this->data['calendarsetting_data_new'] = $calendarsetting_data_new;
		$this->data['page']['desc'] = $this->lang->line('NewCalendarSetting');
		$this->data['page']['subview'] = 'golfclub/bookingonline/calendar/edit_calendarsetting';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][0]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}

	function remove_calendarsettingsectiom(){
		$section = $_POST['section'];
		$calendar_id = $_POST['calendar_id'];
		$data = $this->calendar_m->removeCalendarsettingSectiom($section, $calendar_id);
		print_r($data);
		exit;
	}
	
	
	/**
     * Price Group
     */
		 
	public function newpricegroup() {
		if($this->input->post()){
			$post = $this->input->post();
			$data = array();
			$nowdate = date('Y-m-d H:i:s');
			$data['parent_id'] = 0;
			$data['pricegroup_name'] = json_encode($post['name']);
			$data['last_update_time'] = $nowdate;
			$this->pricegroup_m->addPriceGroup($data);
			redirect(base_url().'golfclub/bookingonline/pricegroup');
			exit;
		}
		$this->data['page']['desc'] = 'Price Group';
		$this->data['page']['subview'] = 'golfclub/bookingonline/pricegroup';
		$this->data['page']['foot'] = 'golfclub/bookingonline/pricegroup/pricegroup_foot';
		$this->data['page']['subnav'][1]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}
		 
	public function pricegroup() {
		$this->data['visiteur_label_id'] = $this->visiteur_label_id;
		$this->data['visiteur_pricegroup_id'] = $this->visiteur_pricegroup_id;
		$this->data['options_parent_id'] = $this->options_parent_id;
		$this->data['page']['desc'] = 'Price Group';
		$this->data['page']['subview'] = 'golfclub/bookingonline/pricegroup';
		$this->data['page']['foot'] = 'golfclub/bookingonline/pricegroup/pricegroup_foot';
		$this->data['page']['subnav'][1]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}

	public function checkConflictBlockPrice() {
		if($this->input->post()){
			$post = $this->input->post();
			$course_id = $post['course_id'];
			$tempdate = array();
			foreach($post['price_groups'] as $key=>$price_group){
				$price_group_detail = $price_group;
				$from_date = date("Y-m-d",strtotime($price_group_detail['from_date']));
				$to_date = date("Y-m-d",strtotime($price_group_detail['to_date']));
				while (strtotime($from_date) <= strtotime($to_date)) {
					if(in_array($from_date,$tempdate)){
						echo 2;
						exit;
					}
					$tempdate[] = $from_date;										
					$from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));					
				}
			}
			foreach($post['price_groups'] as $key=>$price_group){
				$price_group_detail = $price_group;
				$from_date = date('Y-m-d',strtotime($price_group_detail['from_date']));
				$to_date = date('Y-m-d',strtotime($price_group_detail['to_date']));
				if(!empty($price_group_detail['from_time'])){
					$from_time = date('H:i:00',strtotime($price_group_detail['from_time']));
				} else {
					$from_time = '';
				}
				if(!empty($price_group_detail['to_time'])){
					$to_time = date('H:i:00',strtotime($price_group_detail['to_time']));
				} else {
					$to_time = '';
				}
				$conflit_result = $this->calendar_m->checkConflictBlockPrice($course_id, $from_date, $to_date, $from_time, $to_time);
				if($conflit_result == 1 ){ print_r($conflit_result); exit; }
			}
			print_r($conflit_result); exit;
		}
	}
	
	public function newsubpricegroup($pricegroup_id) {
		$options_parent_id = $this->options_parent_id;
		$pricegroup_name = '';
		if(!empty($pricegroup_id)){
			$pricegroup_name = $this->pricegroup_m->getPriceGroupName($pricegroup_id);
		}
		if($this->input->post()){
			$post = $this->input->post();
			//echo '<pre>'; print_r($post); exit;
			$data = array();
			$nowdate = date('Y-m-d H:i:s');
			$data['parent_id'] = $post['pricegroup_id'];
			$data['pricegroup_name'] = json_encode($post['pricegroup_name']);
			$data['course_id'] = $course_id = $post['course_id'];
			$data['last_update_time'] = $nowdate;
			$last_id = $this->pricegroup_m->addPriceGroup($data);
			$i = 0;
			foreach($post['price_groups'] as $key=>$price_group){
				$price_group_detail = $price_group;
				unset($price_group_detail['offer']);
				$from_date = date('Y-m-d',strtotime($price_group_detail['from_date']));
				$to_date = date('Y-m-d',strtotime($price_group_detail['to_date']));
				if(!empty($price_group_detail['from_time'])){
					$from_time = date('H:i:00',strtotime($price_group_detail['from_time']));
				} else {
					$from_time = '';
				}
				if(!empty($price_group_detail['to_time'])){
					$to_time = date('H:i:00',strtotime($price_group_detail['to_time']));
				} else {
					$to_time = '';
				}

				$price_group_detail['course_id'] = $course_id;
				$price_group_detail['pricegroup_id'] = $last_id;
				$price_group_detail['from_date'] = $from_date;
				$price_group_detail['to_date'] = $to_date;
				$price_group_detail['from_time'] = $from_time;
				$price_group_detail['to_time'] = $to_time;
				$price_group_detail['section'] = $key+1;
				$price_group_detail['offer_startdate'] = date('Y-m-d', strtotime($price_group_detail['offer_startdate']));
				$price_group_detail['offer_enddate'] = date('Y-m-d', strtotime($price_group_detail['offer_enddate']));

				$week_days_allow = array();
				if(isset($price_group_detail['monday']) && $price_group_detail['monday'] == 1){
					$week_days_allow[] = 'monday';
				}
				if(isset($price_group_detail['tuesday']) && $price_group_detail['tuesday'] == 1){
					$week_days_allow[] = 'tuesday';
				}
				if(isset($price_group_detail['wednesday']) && $price_group_detail['wednesday'] == 1){
					$week_days_allow[] = 'wednesday';
				}
				if(isset($price_group_detail['thursday']) && $price_group_detail['thursday'] == 1){
					$week_days_allow[] = 'thursday';
				}
				if(isset($price_group_detail['friday']) && $price_group_detail['friday'] == 1){
					$week_days_allow[] = 'friday';
				}
				if(isset($price_group_detail['saturday']) && $price_group_detail['saturday'] == 1){
					$week_days_allow[] = 'saturday';
				}
				if(isset($price_group_detail['sunday']) && $price_group_detail['sunday'] == 1){
					$week_days_allow[] = 'sunday';
				}
				
				$this->pricegroup_m->addPricegroupDetaildata($price_group_detail);
				$checkdata[] = $this->pricegroup_m->checkBlockDetaildata($course_id, $from_date, $to_date, $from_time, $to_time, $price_group_detail['price'], $price_group_detail['pricegroup_id'],  $this->visiteur_pricegroup_id);
				$checkdata[] = $this->pricegroup_m->checkBlockDetailOfferdata($course_id, $price_group_detail['offer_startdate'], $price_group_detail['offer_enddate'], $from_time, $to_time, $price_group_detail['offer_price'], $price_group_detail['pricegroup_id'], $this->visiteur_pricegroup_id);
				//$checkdata[] = $this->pricegroup_m->checkBlockDetailOfferdata(59, '2016-11-19', '2016-11-19', '08:00:00', '08:30:00', 70, 3, 2);
				//echo '<pre>'; print_r($checkdata); exit;
			}
			echo 1; //redirect(base_url().'golfclub/bookingonline/');
			exit;
		}
		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v)
		{
			$name = json_decode($v['name']);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$this->data['pricegroup_id'] = $pricegroup_id;
		$this->data['pricegroup_name'] = $pricegroup_name;
		$this->data['options_parent_id'] = $options_parent_id;
		$this->data['page']['desc'] = 'New Price Group';
		$this->data['page']['subview'] = 'golfclub/bookingonline/newsubpricegroup';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][1]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}

	function edit_subpricegroup($id){
		if(!isset($id)){
			redirect(base_url().'golfclub/bookingonline/pricegroup');
			exit;
		}
		if($this->input->post()){
			$post = $this->input->post();
			echo '<pre>'; print_r($post); exit;
		}
		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v){
			$name = json_decode($v['name']);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$pricegroup_data = $this->calendar_m->getPriceGroupData($id);
		$pricegroup_data_new = array();
		$section_array = array();
		$i=0;
		foreach($pricegroup_data as $pricegroup_row){
			if(!in_array($pricegroup_row['section'],$section_array)){
				$pricegroup_data_new[] = $pricegroup_row;
				$section_array[] = $pricegroup_row['section'];
			}
			$course_id = $pricegroup_row['course_id'];
		}
		$this->data['section_array'] = $section_array;
		$this->data['course_id'] = $course_id;
		$this->data['pricegroup_data_new'] = $pricegroup_data_new;
		$this->data['page']['desc'] = 'Edit Price Group';
		$this->data['page']['subview'] = 'golfclub/bookingonline/calendar/edit_subpricegroup';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][0]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}
	
	public function pricegrouplist(){
		$this->jqgrid->from('t_price_group');
		$this->jqgrid->select('id,pricegroup_name');
		$this->jqgrid->searchable('setting_name');
		$this->jqgrid->where('parent_id=0');
		echo escape_quote($this->jqgrid->get_result('json'));
		exit;
	}
	
	public function subpricegrouplist($pricegroup_id){
		$this->jqgrid->from('t_price_group');
		$this->jqgrid->select('id,pricegroup_name');
		$this->jqgrid->searchable('setting_name');
		$this->jqgrid->where('parent_id='.$pricegroup_id);
		echo escape_quote($this->jqgrid->get_result('json'));
		exit;
	}
	
	/**
     * Exlusions
     */
	public function exlusions() {
		$this->data['page']['desc'] = $this->lang->line('Exlusions');
		$this->data['page']['subview'] = 'golfclub/bookingonline/exlusions';
		$this->data['page']['foot'] = 'golfclub/bookingonline/exlusion/exlusion_foot';
		$this->data['page']['subnav'][2]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}
	public function get_exlusions(){
		$this->jqgrid->from('t_exlusions');
		$this->jqgrid->select('id, exlusions_name, last_update_time');
		$this->jqgrid->searchable('id,exlusions_name');
		echo escape_quote($this->jqgrid->get_result('json'));
	}
	
	public function newexlusions() {
		if($this->input->post()){
			$rules = $this->exlusion_m->rules;
			$this->form_validation->set_rules($rules);
                        // echo "<pre>";print_r($_POST);exit;
			if (true){
				$post = $this->input->post();
                                // echo "<pre>";print_r($post);exit;
                                $exlusions_name = json_encode($post['exlusions_name']);
				$last_id = $this->exlusion_m->addExusiondata(array('exlusions_name' => $exlusions_name, 'last_update_time' => date('Y-m-d H:i:s')));
				$i = 0;
				foreach($post['exlusions'] as $key=>$exlusions)
				{
					$exlusion_detail = $exlusions;
					unset($exlusion_detail['courses']);
					foreach($exlusions['courses'] as $course_id){
						$exlusion_detail['course_id'] = $course_id;
						$exlusion_detail['exlusions_id'] = $last_id;
						$exlusion_detail['section'] = $key+1;
						$exlusion_detail['from_date'] = date('Y-m-d',strtotime($exlusion_detail['from_date']));
						$exlusion_detail['end_date'] = date('Y-m-d',strtotime($exlusion_detail['end_date']));
						$exlusion_detail['starttime'] = date('H:i:00',strtotime($exlusion_detail['starttime']));
						$exlusion_detail['endtime'] = date('H:i:00',strtotime($exlusion_detail['endtime']));
						$this->exlusion_m->addExusionDetaildata($exlusion_detail);
					}
				}
			}
		}

		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v)
		{
			$name = json_decode($v['name']);
			//echo "<pre>";print_r($name);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$this->data['page']['desc'] = $this->lang->line('NewExlusions');
		$this->data['page']['subview'] = 'golfclub/bookingonline/newexlusions';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][2]['active'] = true;
		$this->load->view('_layout_main', $this->data);
		// show($this->data);
	}

	function delete_exlusion(){
		$id = $_POST['id'];
		$this->db->where('exlusions_id',$id);
		$delete_status1 = $this->db->delete('t_exlusions_detail');

		$this->db->where('id',$id);
		$delete_status2 = $this->db->delete('t_exlusions');

		if($delete_status2 && $delete_status1){
			echo 1;
		}else{
			echo 0;
		}

	}

	public function newexlusions_V1() {
		if($this->input->post()){
			$rules = $this->exlusion_m->rules;
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == TRUE){
				$new_submit_data = array();
				$post = $this->input->post();
				//echo "<pre>";print_r($post); echo "</pre>";
				$data = array();
				$nowdate = date('Y-m-d H:i:s');
				$data['exlusions_name'] = $post['exlusions_name'];
				$data['last_update_time'] = $nowdate;
				$lastid = $this->exlusion_m->addExusiondata($data);
				$i = 0;

				foreach($post['start_date'] as $k=> $v)
				{
					//echo $v;
					$sec_courses = count($post['courses'][$k]);
					//echo "Corse   ".$sec_courses;
					$j = 0;
					foreach($post['courses'][$k] as $c => $d)
					{
						$new_submit_data[$i][$j]['exlusions_id'] = $lastid;
						$new_submit_data[$i][$j]['section'] = $k+1;
						$new_submit_data[$i][$j]['course_id'] = $d;
						$new_submit_data[$i][$j]['from_date'] = date("Y-m-d", strtotime($v[0]));
						$new_submit_data[$i][$j]['end_date'] = date("Y-m-d", strtotime($v[0]));
						$new_submit_data[$i][$j]['fullday'] = isset($post['full_day'][$k][0]) ? $post['full_day'][$k][0] : 0;
						$new_submit_data[$i][$j]['starttime'] = date("H:i:s", strtotime($post['start_timeslot'][$k][0]));
						$new_submit_data[$i][$j]['endtime'] = date("H:i:s", strtotime($post['end_timeslot'][$k][0]));
						foreach($post['day'][$k] as $a => $b)
						{
							$new_submit_data[$i][$j][$b] = 1;
						}
						$j++;
					}
					$i++;
				}

				foreach($new_submit_data as $k=> $v)
				{
					foreach($v as $x=> $y)
					{
						$this->exlusion_m->addExusionDetaildata($y);
					}
				}
			}
		}
		$default_lang = $this->data['client']->default_language;
		$cources = $this->calendar_m->getAllCources($this->data['user']['id']);
		foreach($cources as $k=>$v)
		{
			$name = json_decode($v['name']);
			//echo "<pre>";print_r($name);
			$cources[$k]['cource_name'] = $name->$default_lang;
		}
		$this->data['cources'] = $cources;
		$this->data['page']['desc'] = $this->lang->line('NewExlusions');
		$this->data['page']['subview'] = 'golfclub/bookingonline/newexlusions';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][2]['active'] = true;
		$this->load->view('_layout_main', $this->data);
		// show($this->data);
	}
	
	/**
     * My Calender
     */
	public function mycalender() {
		//$this->data['page']['courses_list'] = $this->course_m->get_array_list_by_lang($this->current_language_id);
		$default_lang = $this->data['client']->default_language;
		$this->data['page']['courses_list'] = $this->calendar_m->get_courses_list_by_user_lang($default_lang, $this->data['user']['id']);

		/*if($this->session->userdata("home_booking_course_id")){
			$course_id = $this->session->userdata("home_booking_course_id");
		}else{
			if(!empty($this->data['page']['courses_list'])){
				$course_id = $this->data['page']['courses_list'][0]['id'];
			}
		}*/
		$course_id = 0;
		if(!empty($this->data['page']['courses_list'])){
			$course_id = $this->data['page']['courses_list'][0]['id'];
		}

		$calender_result = $this->calendar_m->getBlockDataWiseCourseID($course_id);
		$calender_data = array();
		if(!empty($calender_result)){
			foreach($calender_result as $key=>$value){
				$calender_data[$key]['title'] = $value['start_time'];
				$calender_data[$key]['url'] = 'javascript:void(0)';
				$calender_data[$key]['start'] = $value['block_date'];
			}
		}
		$this->data['page']['calendar_data'] = json_encode($calender_data);

		$this->data['page']['desc'] = 'My Calender';
		$this->data['page']['subview'] = 'golfclub/bookingonline/mycalender';
		$this->data['page']['foot'] = 'golfclub/bookingonline/mycalender_foot';
		$this->data['page']['subnav'][3]['active'] = true;
		$this->load->view('_layout_main', $this->data);
	}
	public function orders() {
        $this->data['page']['desc'] = 'Orders';
        $this->data['page']['subview'] = 'golfclub/bookingonline/orders';
        $this->data['page']['foot'] = 'golfclub/bookingonline/orders_foot';
        $this->data['page']['subnav'][3]['active'] = true;
        $this->load->view('_layout_main', $this->data);
        // show($this->data);
    }

    public function get_orders() {

		//$this->jqgrid->select('t_users.email, dip_nt_courses.name, t_order_master.order_date, t_block_detail.start_time, t_order_master.total_amount, t_order_master.id, t_order_master.user_id, t_order_master.course_id, t_order_master.block_id');
		$this->jqgrid->select('t_users.email, dip_nt_courses.name, t_order_master.order_date, t_block_detail.start_time, t_order_master.total_amount, t_order_master.id, t_order_master.user_id, t_order_master.course_id, t_order_master.block_id');
		//$this->jqgrid->from('t_order_master, dip_nt_courses, t_users, t_block_detail');
		$this->jqgrid->from('t_order_master, dip_nt_courses, t_block_detail, t_users');
		//$this->jqgrid->where('dip_nt_courses.id=t_order_master.course_id AND t_users.id=t_order_master.user_id AND t_block_detail.id=t_order_master.block_id');
		$this->jqgrid->where('dip_nt_courses.id = t_order_master.course_id AND t_block_detail.id = t_order_master.block_id AND t_users.id=t_order_master.user_id');

		/*$this->jqgrid->select('*');
		$this->jqgrid->from('t_order_master');
		$this->jqgrid->searchable('id');*/

		/*$this->jqgrid->from('t_order_master');
		$this->jqgrid->select('*');
		$this->jqgrid->where('');*/
		//$this->jqgrid->searchable('id');

		//$this->jqgrid->where('dip_nt_courses.id = t_order_master.course_id AND t_users.id = t_order_master.user_id AND t_block_detail.id=t_order_master.block_id');

		/*$this->jqgrid->select('dip_nt_courses.name , t_users.email , t_block_detail.start_time , t_order_master.order_date , t_order_master.total_amount, t_order_master.id , t_order_master.user_id , t_order_master.course_id , t_order_master.block_id');
		$this->jqgrid->from('t_order_master,dip_nt_courses,t_users,t_block_detail');
		$this->jqgrid->where('dip_nt_courses.id=t_order_master.course_id AND t_users.id=t_order_master.user_id AND t_block_detail.id=t_order_master.block_id');*/

		//$query = $this->jqgrid->get_result();
		//print_r($this->jqgrid->get_result());
		
        echo escape_quote($this->jqgrid->get_result('json'));
        
    }
     public function get_order_details() {
		 $id = $_POST['id'];
		 if($id)
		 {
			 $order_data = $this->calendar_m->getOrderDetailDataById($id);
			 if(is_array($order_data))
			 {
				 if(isset($order_data[0]['total_amount']))
				 {
					 $data['price'] = $order_data[0]['total_amount'];
					 $data['order_id'] = $order_data[0]['id'];
					 $block_id = $order_data[0]['block_id'];
					 $order_id = $order_data[0]['id'];
					 $user_id = $order_data[0]['user_id'];
				 }
				 $block_data = $this->calendar_m->getBlockDetailDataById($block_id);
				 $data['block_date'] = $block_data[0]['block_date'];
				 $data['start_time'] = $block_data[0]['start_time'];
				 $data['course_id'] = $block_data[0]['course_id'];

				 $default_language = $this->data['client']->default_language;
				 //$default_language = $this->crud_m->get_column_value_by_id('dip_golfclubs', 'default_language', array('user_id'=>$user_id));
				 $data['course_name'] = $this->course_m->getSingleCourseNameByLang($default_language, $block_data[0]['course_id']);
				 $data['course_holes'] = $this->course_m->getCourseDataByID($block_data[0]['course_id'], "holes");

				 $sub_total = 0;
				 $player_data = $this->calendar_m->getPlayerOrderDetailData($order_id);

				 if(is_array($player_data))
				 {
					 foreach($player_data as $k => $v)
					 {
						 $data['player_data'][$k]['name'] = $v['firstname'].' '.$v['lastname'];
						 $data['player_data'][$k]['hcp'] = $v['hcp'];
						 $data['player_data'][$k]['member'] = $this->calendar_m->getPriceGroupName($v['pricegroup_id']);
						 $data['player_data'][$k]['price'] = $v['price'];
						 $sub_total+=$v['price'];
					 }
				 }
				 $option_data = $this->calendar_m->getOptionOrderDetailData($order_id);
				 if(is_array($option_data))
				 {
					 foreach($option_data as $k => $v)
					 {
						 $sub_total+=$v['price'];
					 }
				 }

				 $data['option_data'] = $option_data;
				 $data['sub_total'] = $sub_total;

				 $user_data = $this->crud_m->get_all_records_with_condition('t_users',array('id'=>$user_id));
				 if(is_array($user_data))
				 {
					 $data['user_data'] = $user_data[0];
					$country_data = $this->crud_m->get_column_value_by_id('dip_countries','country_name', array('id'=>$user_data[0]['country_id']));
					 $data['user_data']['country'] = $country_data;
				 }
			 }
		 }
		 return $this->load->view('golfclub/bookingonline/orders_modal',$data);exit;

		$id = $_POST['id'];
		$query = $this->jqgrid->select('*');
       	$query .= $this->jqgrid->from('t_users');
       	$query .= $this->jqgrid->where('id='.$id);
		$query .= $this->jqgrid->get_result();
	
		$this->data['user_deatils'] = $query;
       	//echo escape_quote($this->jqgrid->get_result('json'));
       	$this->load->view('orders_foot', $this->data);
       	
       	//------ query to fetch data from 4 tables t_block_detail, t_order_player_detail and t_order_options -----------------------
       	/*$query1 = $this->jqgrid->select('id as oid,block_id as bid');
       	$query1 .= $this->jqgrid->from('t_order_master');
       	$query1 .= $this->jqgrid->where('t_order_master.user_id='.$id);
       	$query1 .= $this->jqgrid->get_query();
       	print_r($query1);
       	$order_master_id = $query1->row[0]['oid'];
       	$block_id = $query1->row[0]['bid'];
       	
       	$query2 = $this->jqgrid->select('t_block_detail.price,t_block_detail.offer_price,t_block_detail.block_player,t_block_detail.pricegroup_id,t_price_group.pricegroup_name');
       	$query2 .= $this->jqgrid->from('t_block_detail,t_price_group');
       	$query2 .= $this->jqgrid->where('t_price_group.id = t_block_detail.pricegroup_id AND t_block_detail.id='.$block_id);
       	$query2 .= $this->jqgrid->get_result();
       	$this->data['block_detail_data'] = $query2;
       	
       	$query3 = $this->jqgrid->select('firstname,lastname,hcp,price');
       	$query3 .= $this->jqgrid->from('t_order_player_detail');
       	$query3 .= $this->jqgrid->where('t_order_player_detail.order_master_id='.$order_master_id);
       	$query3 .= $this->jqgrid->get_result();
       	
       	$query4 = $this->jqgrid->select('qty');
       	$query4 .= $this->jqgrid->from('t_order_options');
       	$query4 .= $this->jqgrid->where('t_order_options.order_master_id='.$order_master_id);
       	$query4 .= $this->jqgrid->get_result();*/
       
    }
	/**
	 * Payment And Others
	 */
	public function payment_other() {
		if(!empty($_POST)){
			if(isset($_POST['save_payment_settings'])){
				$payment_modes_data = array(
					'payment_mode'=>$_POST['payment_mode'],
					'is_active'=>isset($_POST['is_active'])?1:0,
					'payment_email'=>$_POST['payment_email'],
				);
				$this->db->where(array('payment_mode_id'=>$_POST['payment_mode_id']));
				$this->db->update('t_payment_modes',$payment_modes_data);
			}
			if(isset($_POST['save_logo_background'])){				  
				$this->db->select('*');
				$this->db->from('t_settings');
				$this->db->where('user_id',$this->data['user']['id']);				
				$query = $this->db->get();
				$rowcount = $query->num_rows();
				if($rowcount == 0)
				{
					//insert
					
				}
				else
				{
					//update
					$this->db->where(array('payment_mode_id'=>$_POST['payment_mode_id']));
					$this->db->update('t_settings',$payment_modes_data);
				}
			}
			if(isset($_POST['save_terms_condition'])){
				$payment_modes_data = array(
					'title'=>$_POST['title'],
					'content'=>$_POST['term_content'],
				);
				$this->db->where(array('id'=>$_POST['terms_condition_id']));
				$this->db->update('t_terms_condition',$payment_modes_data);
			}
			if(isset($_POST['save_contact_us'])){
				$payment_modes_data = array(
					'title'=>$_POST['title'],
					'content'=>$_POST['contactus_content'],
				);
				$this->db->where(array('id'=>$_POST['contact_us_id']));
				$this->db->update('t_contact_us',$payment_modes_data);
			}
		}
		$this->data['payment_modes'] = $this->db->limit(1)->get('t_payment_modes')->result();
		$this->data['payment_mode'] = $this->data['payment_modes'][0];
		$this->data['terms_condition'] = $this->db->limit(1)->get('t_terms_condition')->result();
		$this->data['terms_condition'] = $this->data['terms_condition'][0];
		$this->data['contact_us'] = $this->db->limit(1)->get('t_contact_us')->result();
		$this->data['contact_us'] = $this->data['contact_us'][0];
		$this->data['page']['desc'] = 'Payment And Others';
		$this->data['page']['subview'] = 'golfclub/bookingonline/payment_other';
		$this->data['page']['foot'] = 'golfclub/bookingonline/courses_foot';
		$this->data['page']['subnav'][4]['active'] = true;
		$this->load->view('_layout_main', $this->data);
		// show($this->data);
	}

	function pre_die($data_array = array()){
		if(is_array($data_array)){
			echo "<pre>";
			print_r($data_array);
			die;
		}
		print_r($data_array);
		die;
	}
}

