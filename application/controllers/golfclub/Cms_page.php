<?php
class Cms_page extends DIP_Controller_Golfclub
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_m');
	}
	public function contactus()
	{
		$data['records'] = $this->page_m->select_contactus();
		$this->load->view('golfclub/page',$data);
		//echo "<script>alert('contact us');</script>";
		//redirect('http://localhost/booking_bit/golfclub/profile','refresh');
	}

	public function termsconditions()
	{
		$data['records'] = $this->page_m->select_termsconditions();
		$this->load->view('golfclub/page',$data);
		//echo "<script>alert('terms & conditions');</script>";
		//redirect('http://localhost/booking_bit/golfclub/profile','refresh');
	}

	public function update_data()
	{
		$data['page_id'] = $this->input->post('page_id');
		$data['name'] = $this->input->post('name');
		$data['content'] = $this->input->post('content');
		if($this->page_m->update_page($data)==true)
		{
			echo "<script>alert('record updated successfully');</script>";
			redirect(base_url(),'refresh');
		}
		else
		{
			die('unsuccess');
		}
	}
}
?>