<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LangSwitch
 *
 * @author TIJANI
 */
class LanguageSwitcher extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
    }

    function switchLang($language = "", $language_id = 0) 
    {	
        // get default language value from config.php
        $default_language_id = $this->config->item('default_language_id');
        $default_language_name = $this->config->item('language');        
        
        $language = ($language != "") ? $language : $default_language_name;        
        $language_id = $language_id > 0 ? $language_id : $default_language_id;        
        
        $this->session->set_userdata('site_lang', $language);
        $this->session->set_userdata('site_lang_id', $language_id);
        redirect($_SERVER['HTTP_REFERER']);
    }    

}
