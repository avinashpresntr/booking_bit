<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Frontend
 * @property Crud_m $crud_m
 */
class Frontend extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('layout');          // Load layout library     
        $this->load->library('session');        
        $this->output->enable_profiler(false);
        $this->load->model('calendar_m');
        $this->load->model('course_m');
        $this->load->model('lang_m');
        $this->load->model('frontend_m');
        $this->load->model('crud_m');
        $this->load->helper('date');
        
        // get current language id
        $this->current_language_id = $this->session->userdata("site_lang_id");        
        if(empty($this->current_language_id) || is_null($this->current_language_id))
        {
            $this->current_language_id = $this->config->item('default_language_id');
        }                   
        
        // get current language name
        $this->current_language = $this->session->userdata("site_lang");
        if(empty($this->current_language) || is_null($this->current_language))
        {
            $this->current_language = $this->config->item('language');
        }           
        
        // check directory exists or not if not than use default english dir
        $filePath = APPPATH.DIRECTORY_SEPARATOR."language".DIRECTORY_SEPARATOR.strtolower($this->current_language);
        if(is_dir($filePath))
        {
            $this->lang->load("message", strtolower($this->current_language));
        }
        else
        {
            $this->lang->load("message", $this->config->item('language'));
        }

    }

    public function login_check() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|callback_check_username_exists');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if (!$this->form_validation->run($this)){
            //$this->index();
            $this->session->set_flashdata('login_errors', validation_errors('<span style="display: block;">','</span>'));
            redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
        }
    }

    function check_username_exists()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->frontend_m->authenticate($username,$password);

        if($result){
            $this->session->set_userdata('user_login_session',$result);
            redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
        } else {
            $this->form_validation->set_message('check_username_exists', 'your username or password wrong.');
            return false;
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_login_session');
        redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
    }

    public function payment() {
        $amount = $this->input->post('amount');
        $data_to_insert = array(
            'amount' => $amount,
            'created_at' => date("Y-m-d H:i:s")
        );

        $this->db->insert('t_payment_logs', $data_to_insert);
        $payment_record_id = $this->db->insert_id();

        $paypal_email = "ajay.m_1340704576_per@gmail.com";
        $membership_plan_name = "Booking Payment";
        $notify_url = base_url() . "presntr/notify-paypal?payment_record_id=" . $payment_record_id;
        $success_url = base_url() . "presntr/success-paypal-payment";
        $url = "https://www.sandbox.paypal.com/webscr";
        $paypal_form = '<form name="_xclick" action="' . $url . '" method="post">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="' . $paypal_email . '">
		<input type="hidden" name="currency_code" value="USD">
		<input type="hidden" name="item_name" value="' . $membership_plan_name . ' subscription">
		<input type="hidden" name="notify_url" value="' . $notify_url . '">
		<input type="hidden" name="return" value="' . $success_url . '">
		<input type="hidden" name="amount" value="' . $amount . '">
		</form>';
        echo $paypal_form;
        echo "<script type='text/javascript'>document._xclick.submit();</script>";
    }

    public function index($user_id)
    {
        if(!$this->session->userdata('site_user_id')){
            $this->session->set_userdata('site_user_id', $user_id);
        }

        $data['default_language'] = $this->crud_m->get_column_value_by_id('dip_golfclubs', 'default_language', array('user_id'=>$user_id));
        if(!$this->session->userdata('site_lang_id')){
            $this->session->set_userdata('site_lang_id', $data['default_language']);
        }

        // get course records (id, name) for dropdown
        //$data['courses_list'] = $this->course_m->get_array_list_by_lang($this->current_language_id);
        //$data['courses_list'] = $this->frontend_m->get_ntcourse_array_list_by_lang($this->current_language_id, $user_id);
        $data['courses_list'] = $this->frontend_m->get_ntcourse_array_list_by_lang($data['default_language'], $user_id);

        $data['booking_date'] = date('Y-m-d');
        $data['booking_course_id'] = isset($data['courses_list'][0]['id']) ? $data['courses_list'][0]['id']:0;
        $val = $this->session->userdata("home_booking_date");
        if(!is_null($val) && !empty($val))
        {
            $data['booking_date'] = $this->session->userdata("home_booking_date");
        }
        $val = $this->session->userdata("home_booking_course_id");
        if(!is_null($val) && !empty($val))
        {
            $data['booking_course_id'] = $this->session->userdata("home_booking_course_id");
        }            
        
        if ($this->input->post('date'))
        {            
            $date = $this->input->post('date');
            $data['booking_date'] = date('Y-m-d', strtotime($date));
            $data['booking_course_id'] = $this->input->post("parcours");
        }
        
        // store course and date to session
        $tmpArr = array("home_booking_date" => $data['booking_date'],"home_booking_course_id" => $data['booking_course_id']);
        $this->session->set_userdata($tmpArr);        
        
        
        $data['next_week_date'] = (date('w', strtotime($data['booking_date'])) == 1) ? date("Y-m-d", strtotime($data['booking_date'] . "+1 week")) : date("Y-m-d", strtotime($data['booking_date'] . "last monday +1 week"));
        $data['pre_week_date'] = (date('w', strtotime($data['booking_date'])) == 1) ? date("Y-m-d", strtotime($data['booking_date'] . "-1 week")) : date("Y-m-d", strtotime($data['booking_date'] . "last monday -1 week"));
        //$week_start_end_date = $this->getCurrentweek($data['booking_date']);
        //$data['weeks_date_list'] = $this->getDates($week_start_end_date['start_date'], $week_start_end_date['end_date']);
        $data['weeks_date_list'] = $this->getCurrentweek($data['booking_date']);

        $booking_date = $this->session->userdata("home_booking_date");
        $booking_course_id = $this->session->userdata("home_booking_course_id");
        //$booking_course_id = 59;

        $current_date_lowest_price = array();
        foreach($data['weeks_date_list'] as $w_key=>$w_value){
            $current_date_lowest_price[$w_key] = $this->frontend_m->getCurrentDateLowestPrice($w_value);
        }
        $data['current_date_lowest_price'] = $current_date_lowest_price;

        $new_course_data = $this->calendar_m->getBlockDetailData($booking_date,$booking_course_id);
        //echo "<pre>";print_r($new_course_data);exit;


        $slot_start_time = '00';
        $slot = 0;
        $block_no = 0;
        $front_data = array();
        if(is_array($new_course_data) && count($new_course_data) > 0)
        {
            foreach($new_course_data as $k=>$v)
            {
					$start_time = strtotime($v['start_time']);
					$block_start_time = date('H', $start_time);

					if($slot_start_time != $block_start_time)
					{
							$front_data[$slot]['start_time'] = $block_start_time;
							$front_data[$slot]['end_time'] = $block_start_time + 1;
							//$data[$slot]['block_data']['block_id'] = array($v['id']);
							$slot_start_time = $block_start_time;
							$block_no = 0;
							$slot++;
					}
					/////For Block : Start			
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['block_id'] = $v['id'];
					//$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['block_period'] = $v['block_period'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['block_start_data'] = $v['start_time'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['block_player'] = $v['block_player'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['price'] = $v['price'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['offer_price'] = $v['offer_price'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['holes'] = $v['holes'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['total_booked_players'] = $v['total_booked_players'];
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['order_master_id'] = $v['order_master_id'];

					$block_id = $v['id'];
					$booking_data = array();

					$booking_data = $this->calendar_m->getBlockBookingDetailData($block_id);
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['total_booking_data'] = $booking_data;

					$option_data = array();
					$option_data = $this->calendar_m->getBlockOptionData($block_id);
					$front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['option_data'] = $option_data;

                    $member_data = array();
                    $member_data = $this->calendar_m->getBlockMemberData($block_id);
                    $front_data[$slot - 1]['total_blocks_in_eachslot'][$block_no]['member_data'] = $member_data;

					$block_no++;
					/////For Block : End
			}            
        }    

	//echo "<pre>";print_r($front_data);exit;

        $data['booking_date'] = date('d/m/Y', strtotime($data['booking_date']));
        //$data['pre_week_date'] = date('d/m/Y', strtotime($data['pre_week_date']));
        //$data['next_week_date'] = date('d/m/Y', strtotime($data['next_week_date']));
	    $data['courses_data_new'] = $front_data;
        $this->layout->title('Home');
        $this->layout->description('Home');
        $this->layout->layout_view = 'frontend/layout.php';
        $this->layout->view('frontend/home', $data);
    }

    public function getBookingMemberOptionData(){
        $block_id = $this->input->post('block_id');
        $member_data = array();
        $member_data = $this->calendar_m->getBlockMemberData($block_id);
        echo json_encode($member_data);

    }
    
    function getCurrentweek($date) {
        $monday = (date('w', strtotime($date)) == 1) ? $date : strtotime('last monday', $date);
        for ($i = 0; $i <= 6; $i++) {
            $day = (date('w', strtotime($date)) == 1) ? strtotime($date . "+$i day") : strtotime($date . "last monday +$i day"); //strtotime($date."last monday +$i day");
            $data2[$i] = date("Y-m-d", $day);
        }return $data2;
    }

    function getDates($start_time, $end_time) {
        $startdateObject = new DateTime(date('d-m-Y', $start_time));
        $endateObject = new DateTime(date('d-m-Y', $end_time));
        $datedif = $endateObject->diff($startdateObject);
        $intervald = strtotime($startdateObject->format('d-m-Y'));

        $datearray = array();
        for ($i = 0; $i <= $datedif->days; $i++) {
            $datearray[$i] = date('d-m-Y', $intervald);
            $intervald = $intervald + 86400;
        }
        return $datearray;
    }

    public function your_booking() {
        $data = array();
        $data['order_id'] = '1';
        $data['user_id'] = '1';
        $data['course_id'] = '1';
        $data['order_date'] = date('d-m-Y');
        $data['order_starttime'] = date('h:i:s');
        $data['order_endtime'] = date('h:i:s');
        $data['total_amount'] = '60.00';

        $data['hour'] = '07h00';
        $data['course'] = 'Severiano Ballesteros';
        $data['number_of_players'] = '4';

        $data['player'] = array(
            array('firstname' => 'John', 'lastname' => 'Lowenstein', 'hcp' => 'HCP', 'pricegroup_id' => '4', 'total' => '60.00'),
        );

        $data['option'] = array(
            array('pricegroup_id' => '3', 'checked' => true, 'qty' => '4', 'selected_option' => '2', 'default_price' => '5.00')
        );
        return $data;
    }

    public function paypal_payment() {
        $this->layout->title('Paypal Payment');
        $this->layout->description('Paypal Payment');
        $this->layout->layout_view = 'frontend/layout.php';
        $this->layout->view('frontend/paypal_payment', $data);
    }

    public function continue_booking() {
        $post_data = $this->input->post();
        //echo '<pre>';print_r($post_data);die;

        $user_id = 1;
        $block_id = $post_data['block_id'];
        //$order_master_id = $post_data['order_master_id'];
        $course_id = $post_data['parcours'];
        $total_amount = $post_data['total_amount'];

        /*if($order_master_id !=''){
            $data_to_update = array(
                'user_id' => $user_id,
                'course_id' => $course_id,
                'block_id' => $block_id,
                'order_date' => date('Y-m-d'),
                'total_amount' => $total_amount
            );
            $this->db->where('id', $order_master_id);
            $this->db->update('t_order_master', $data_to_update);

        }else{*/
            $data_to_insert = array(
                'user_id' => $user_id,
                'course_id' => $course_id,
                'block_id' => $block_id,
                'order_date' => date('Y-m-d'),
                'total_amount' => $total_amount
            );

            $this->db->insert('t_order_master', $data_to_insert);
            $order_master_id = $this->db->insert_id();
        //}

        /* Save Player Data */
        $player_data = $post_data['player'];

        foreach($player_data as $p_key=>$p_value){
            $id = trim($player_data[$p_key]['id']);
            $firstname = trim($player_data[$p_key]['firstname']);
            if($id == '' && $firstname !=''){
                $lastname = trim($player_data[$p_key]['lastname']);
                $hcp = trim($player_data[$p_key]['hcp']);
                $pricegroup_id = trim($player_data[$p_key]['pricegroup_id']);
                $price = trim($player_data[$p_key]['price']);

                $p_data_to_insert = array(
                    'order_master_id' => $order_master_id,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'hcp' => $hcp,
                    'pricegroup_id' => $pricegroup_id,
                    'price' => $price
                );
                $this->db->insert('t_order_player_detail', $p_data_to_insert);
            }
        }

        /*for($i=1; $i<=sizeof($player_data);$i++){
            $firstname = trim($player_data[$i]['firstname']);
            if($firstname !=''){
                $lastname = trim($player_data[$i]['lastname']);
                $hcp = trim($player_data[$i]['hcp']);
                $pricegroup_id = trim($player_data[$i]['pricegroup_id']);
                $price = trim($player_data[$i]['price']);

                $p_data_to_insert = array(
                    'order_master_id' => $order_master_id,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'hcp' => $hcp,
                    'pricegroup_id' => $pricegroup_id,
                    'price' => $price
                );
                $this->db->insert('t_order_player_detail', $p_data_to_insert);
            }
        }*/

        /* Save Option Data */
        $option_data = $post_data['option'];
        for($i=1; $i<=sizeof($option_data);$i++){
            $o_pricegroup_id = trim($option_data[$i]['pricegroup_id']);

            if(isset($o_pricegroup_id) && $o_pricegroup_id !=''){
                $o_price = trim($option_data[$i]['price']);
                $o_qty = trim($option_data[$i]['qty']);

                $p_data_to_insert = array(
                    'order_master_id' => $order_master_id,
                    'block_id' => $block_id,
                    'pricegroup_id' => $o_pricegroup_id,
                    'qty' => $o_qty,
                    'price' => $o_price
                );
                $this->db->insert('t_order_options', $p_data_to_insert);
            }
        }
        redirect(base_url('frontend/checkout/'.$order_master_id));
    }

    public function delete_order($id) {
        if($id){
            $this->db->where('id', $id);
            $this->db->delete('t_order_master');
            redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
        }else{
            redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
        }
    }

    public function checkout($id = 0) {
		if($id)
		{
			$order_data = $this->calendar_m->getOrderDetailDataById($id);
			if(is_array($order_data))
			{
				if(isset($order_data[0]['total_amount']))
				{
					$data['price'] = $order_data[0]['total_amount'];
					$data['order_id'] = $order_data[0]['id'];
					$block_id = $order_data[0]['block_id'];
					$order_id = $order_data[0]['id'];
				}
				$block_data = $this->calendar_m->getBlockDetailDataById($block_id);
				$data['block_date'] = $block_data[0]['block_date'];
				$data['start_time'] = $block_data[0]['start_time'];
				$data['course_id'] = $block_data[0]['course_id'];
                $data['course_name'] = $this->course_m->getSingleCourseNameByLang($this->current_language_id, $block_data[0]['course_id']);
                $data['course_holes'] = $this->course_m->getCourseDataByID($block_data[0]['course_id'], "holes");

				$sub_total = 0;
				$player_data = $this->calendar_m->getPlayerOrderDetailData($order_id);


                if(is_array($player_data))
				{
					foreach($player_data as $k => $v)
					{
						$data['player_data'][$k]['name'] = $v['firstname'].' '.$v['lastname'];
						$data['player_data'][$k]['hcp'] = $v['hcp'];
						$data['player_data'][$k]['member'] = $this->calendar_m->getPriceGroupName($v['pricegroup_id']);
						$data['player_data'][$k]['price'] = $v['price'];
						$sub_total+=$v['price'];
					}
				}
                $option_data = $this->calendar_m->getOptionOrderDetailData($order_id);
                //echo '<pre>';print_r($option_data);die;
                if(is_array($option_data))
                {
                    foreach($option_data as $k => $v)
                    {
                        $sub_total+=$v['price'];
                    }
                }

                //echo '<pre>';print_r($option_data);die;
                $data['option_data'] = $option_data;
				$data['sub_total'] = $sub_total;
				//$country_data = $this->calendar_m->getAllCountry();
                $country_data = $this->crud_m->get_selected_all_records('dip_countries','id, country_name');
				$data['country'] = $country_data;
			}
			else
			{
				redirect('presntr/'.$this->session->userdata('site_user_id'));
			}
            $user_login_session = $this->session->userdata('user_login_session');
            $booking_session_data = array(
                'order_id'=>$data['order_id'],
                'billing_amount'=>$data['sub_total'],
                'billing_lastname'=>$user_login_session['lastname'],
                'billing_firstname'=>$user_login_session['firstname'],
                'billing_country'=>$user_login_session['country_id'],
                'billing_address'=>$user_login_session['address1'],
                'billing_address_2'=>$user_login_session['address2'],
                'billing_city'=>$user_login_session['city'],
                'billing_area'=>$user_login_session['area'],
                'billing_npa'=>$user_login_session['zip'],
                'billing_phone'=>$user_login_session['phone'],
                'billing_email'=>$user_login_session['email'],
                'create_account'=>'',
                'accept'=>'');
            if($this->session->userdata('booking_data') && $this->session->userdata('booking_data')['order_id'] == $id){
                $b_session_data = $this->session->userdata('booking_data');
                $booking_session_data['order_id'] = $b_session_data['order_id'];
                $booking_session_data['billing_amount'] = $b_session_data['billing_amount'];
                $booking_session_data['billing_lastname'] = $b_session_data['billing_lastname'];
                $booking_session_data['billing_firstname'] = $b_session_data['billing_firstname'];
                $booking_session_data['billing_country'] = $b_session_data['billing_country'];
                $booking_session_data['billing_address'] = $b_session_data['billing_address'];
                $booking_session_data['billing_address_2'] = $b_session_data['billing_address_2'];
                $booking_session_data['billing_city'] = $b_session_data['billing_city'];
                $booking_session_data['billing_area'] = $b_session_data['billing_area'];
                $booking_session_data['billing_npa'] = $b_session_data['billing_npa'];
                $booking_session_data['billing_phone'] = $b_session_data['billing_phone'];
                $booking_session_data['billing_email'] = $b_session_data['billing_email'];
                $booking_session_data['create_account'] = $b_session_data['create_account'];
                $booking_session_data['accept'] = $b_session_data['accept'];
            }
		}
        $data['booking_session_data'] = $booking_session_data;
        //print_r($booking_session_data);die;
        $terms_condition = $this->db->limit(1)->get('t_terms_condition')->result();
        $data['terms_condition'] = $terms_condition[0];
        //echo '<pre>';print_r($data);die;
        $this->layout->title('Checkout');
        $this->layout->description('Checkout');
        $this->layout->layout_view = 'frontend/layout.php';
        $this->layout->view('frontend/checkout',$data);
    }

    public function redirect_to_paypal() {
        $post_data = $this->input->post();
        //echo '<pre>';print_r($post_data);die;
        $payment_record_id = 0;
        //$amount = $post_data['billing_amount'];
        $order_id = $post_data['order_id'];
        $amount = $this->crud_m->get_column_value_by_id('t_order_master', 'total_amount', array('id'=>$order_id));

        $user_id = 1;
        $data_to_insert = array(
            'user_id' => $user_id,
            'order_master_id' => $order_id,
            'amount' => $amount,
            'created_at' => date("Y-m-d H:i:s")
        );
        $this->db->insert('t_payment_logs', $data_to_insert);
        $payment_log_id = $this->db->insert_id();
        $this->session->set_userdata('payment_log_id', $payment_log_id);

        /* New Code */
        $post_data['user_id'] = $user_id;
        $this->session->set_userdata('booking_data', $post_data);
        /* End New Code */

        /*$booking_data = $this->your_booking();
        $booking_data['user_data'] = $post_data;
        $this->session->set_userdata('booking_data', $booking_data);

        $payment_record_id = $this->db->insert_id();*/

        /* Paypal Website redirect */
        $paypal_email = "ajay.m_1340704576_per@gmail.com";
        $membership_plan_name = "Booking Payment";
        $notify_url = base_url() . "presntr/notify-paypal?payment_log_id=" . $payment_log_id;
        $success_url = base_url() . "presntr/success-paypal-payment";
        $url = "https://www.sandbox.paypal.com/webscr";
        $paypal_form = '<form name="_xclick" action="' . $url . '" method="post">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="' . $paypal_email . '">
		<input type="hidden" name="currency_code" value="USD">
		<input type="hidden" name="item_name" value="' . $membership_plan_name . ' subscription">
		<input type="hidden" name="notify_url" value="' . $notify_url . '">
		<input type="hidden" name="return" value="' . $success_url . '">
		<input type="hidden" name="amount" value="' . $amount . '">
		<input type="hidden" name="cancel_return" value="'.base_url().'frontend/checkout/'.$order_id.'">
		</form>';
        echo $paypal_form;
        echo "<script type='text/javascript'>document._xclick.submit();</script>";
    }


    public function notify_paypal() {
        if ($this->input->get_post('payment_status') == 'Completed') {

            $id = $this->input->get_post('payment_log_id');
            $data = $this->input->post();
            $paypal_response = is_array($data) ? serialize($data) : '';

            // update paypal data in database
            $data_to_update = array(
                'paypal_response' => $paypal_response,
                'payment_status' => $this->input->get_post('payment_status'),
                'updated_at' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('t_payment_logs', $data_to_update);

            // send email notification
            echo "DONE";
        }
        exit("RUN");
    }

    public function success_paypal_payment() {
        $payment_detail['paypal_response'] = $this->input->post();
        $payment_detail['amount'] = $payment_detail['paypal_response']['payment_gross'];
        $payment_status = $payment_detail['paypal_response']['payment_status'];
        $payment_detail['payment_status'] = $payment_status;
        $payment_detail['paypal_response'] = serialize($payment_detail['paypal_response']);

        $payment_log_id = $this->session->userdata('payment_log_id');

        $this->db->where('id', $payment_log_id);
        $this->db->update('t_payment_logs', $payment_detail);


        if (strtolower($payment_status) == 'completed') {
            $booking_data = $this->session->userdata('booking_data');
            $password = '';
            if($booking_data['create_account'] && $booking_data['create_account'] == 1){
                $password = $booking_data['billing_pwd_1'];
            }
            //$booking_user_data = $booking_data['user_data'];
            $user_data = array(
                'firstname' => $booking_data['billing_firstname'],
                'lastname' => $booking_data['billing_lastname'],
                'country_id' => is_numeric($booking_data['billing_country']) ? $booking_data['billing_country'] : 1,
                'address1' => $booking_data['billing_address'],
                'address2' => $booking_data['billing_address_2'],
                'city' => $booking_data['billing_city'],
                'area' => $booking_data['billing_area'],
                'zip' => $booking_data['billing_npa'],
                'phone' => $booking_data['billing_phone'],
                'email' => $booking_data['billing_email'],
                'user_pass' => $password,
                'user_type' => 'guest',
            );

            $user_id = $this->addBillingAddress($user_data);

            /* Update t_payment_logs table */
            $pay_log_data_to_update = array(
                'user_id' => $user_id
            );
            $this->db->where('id', $payment_log_id);
            $this->db->update('t_payment_logs', $pay_log_data_to_update);

            /* Update t_order_master table */
            $data_to_update = array(
                'user_id' => $user_id,
                'payment_status' => 1
            );
            $this->db->where('id', $booking_data['order_id']);
            $this->db->update(' t_order_master', $data_to_update);

            $this->session->unset_userdata('booking_data');

            /*$booking_user_data = $booking_data['user_data'];
            $user_data = array(
                'firstname' => $booking_user_data['billing_firstname'],
                'lastname' => $booking_user_data['billing_lastname'],
                'country_id' => is_numeric($booking_user_data['billing_country']) ? $booking_user_data['billing_country'] : 1,
                'address1' => $booking_user_data['billing_address'],
                'address2' => $booking_user_data['billing_address_2'],
                'city' => $booking_user_data['billing_city'],
                'area_id' => '',
                'zip' => $booking_user_data['billing_npa'],
                'phone' => $booking_user_data['billing_phone'],
                'email' => $booking_user_data['billing_email'],
                'password' => $booking_user_data['billing_pwd_1'],
                'user_type' => 'guest',
            );

            $order_master_data = array(
                'user_id' => $user_id,
                'course_id' => $booking_data['course_id'],
                'order_date' => date('Y-d-m', strtotime($booking_data['order_date'])),
                'order_starttime' => date('H:i:s', strtotime($booking_data['order_starttime'])),
                'order_endtime' => date('H:i:s', strtotime($booking_data['order_endtime'])),
                'total_amount' => $booking_data['total_amount'],
                'last_update_time' => date('Y-m-d H:i:s'),
            );

            $order_master_id = $this->addOrderMaster($order_master_data);
            foreach ($booking_data['option'] as $order_option_detail) {
                $order_options_data = array(
                    'order_id' => $order_master_id,
                    'pricegroup_id' => $order_option_detail['pricegroup_id'],
                    'qty' => $order_option_detail['qty'],
                );
                $this->addOrderOptions($order_options_data);
            }

            foreach ($booking_data['player'] as $order_player_detail) {
                $order_player_data = array(
                    'order_id' => $order_master_id,
                    'firstname' => $order_player_detail['firstname'],
                    'lastname' => $order_player_detail['lastname'],
                    'hcp' => $order_player_detail['hcp'],
                    'pricegroup_id' => $order_player_detail['pricegroup_id'],
                );
                $this->addOrderPlayerDetail($order_player_data);
            }*/
        }
        $this->layout->title('Paypal Payment');
        $this->layout->description('Paypal Payment');
        $this->layout->layout_view = 'frontend/layout.php';
        $data['msg'] = 'Your payment has been received successfully.';
        $this->layout->view('frontend/success_paypal_payment', $data);
    }

    function save_booking() {
        $booking_data = $this->input->post();
        $password = '';
        if($booking_data['create_account'] && $booking_data['create_account'] == 1){
            $password = $booking_data['billing_pwd_1'];
        }
//echo '<pre>';print_r($booking_data);die;

        $user_data = array(
            'firstname' => $booking_data['billing_firstname'],
            'lastname' => $booking_data['billing_lastname'],
            'country_id' => is_numeric($booking_data['billing_country']) ? $booking_data['billing_country'] : 1,
            'address1' => $booking_data['billing_address'],
            'address2' => $booking_data['billing_address_2'],
            'city' => $booking_data['billing_city'],
            'area' => $booking_data['billing_area'],
            'zip' => $booking_data['billing_npa'],
            'phone' => $booking_data['billing_phone'],
            'email' => $booking_data['billing_email'],
            'user_pass' => $password,
            'user_type' => 'guest',
        );

        $user_id = $this->addBillingAddress($user_data);

        /* Update t_order_master table */
        $data_to_update = array(
            'user_id' => $user_id,
            'payment_status' => 1
        );
        $this->db->where('id', $booking_data['order_id']);
        $this->db->update('t_order_master', $data_to_update);

        $this->session->unset_userdata('booking_data');

        $this->layout->title('Booking Payment');
        $this->layout->description('Booking Payment');
        $this->layout->layout_view = 'frontend/layout.php';
        $data['msg'] = 'Your booking has been submitted successfully.';
        $this->layout->view('frontend/success_paypal_payment', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    function addOrderPlayerDetail($data = array()) {
        $order_data = array(
            'order_id' => $data['order_id'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'hcp' => $data['hcp'],
            'pricegroup_id' => $data['pricegroup_id'],
        );
        $this->db->insert('t_order_player_detail', $order_data);
        return $this->db->insert_id();
    }

    /**
     * @param array $data
     * @return mixed
     */
    function addOrderOptions($data = array()) {
        $option_data = array(
            'order_id' => $data['order_id'],
            'pricegroup_id' => $data['pricegroup_id'],
            'qty' => $data['qty'],
        );
        $this->db->insert('t_order_options', $option_data);
        return $this->db->insert_id();
    }

    function addOrderMaster($data = array()) {
        $master_data = array(
            'user_id' => $data['user_id'],
            'course_id' => $data['course_id'],
            'order_date' => date('Y-d-m', strtotime($data['order_date'])),
            'order_starttime' => date('H:i:s', strtotime($data['order_starttime'])),
            'order_endtime' => date('H:i:s', strtotime($data['order_endtime'])),
            'total_amount' => $data['total_amount'],
            'last_update_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('t_order_master', $master_data);
        return $this->db->insert_id();
    }

    /**
     * @param array $data
     * @return mixed
     * UserType = user|guest
     */
    function addBillingAddress($data = array()) {
        $user_data = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'country_id' => $data['country_id'],
            'address1' => $data['address1'],
            'address2' => $data['address2'],
            'city' => $data['city'],
            'area' => $data['area'],
            //'area_1' => $data['area_1'],
            //'area_2' => $data['area_2'],
            'zip' => $data['zip'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'user_type' => 'guest',
            'last_update_time' => date('Y-m-d H:i:s'),
        );

        if (isset($data['user_pass']) && $data['user_pass'] != '') {
            $user_data['user_pass'] = md5($data['user_pass']);
            $user_data['user_type'] = 'user';
        }

        $this->db->insert('t_users', $user_data);
        return $this->db->insert_id();
    }

    public function myaccount() {
        $this->crud_m->isLogin();
        $user_id = $this->session->userdata('user_login_session')['id'];

        $country_data = $this->crud_m->get_selected_all_records('dip_countries','id, country_name');
        $data['country'] = $country_data;

        $data['user_data'] = $this->crud_m->get_all_records_with_condition('t_users', array('id'=>$user_id));
        $data['user_data'] = $data['user_data'][0];

        $this->layout->title('My Account');
        $this->layout->description('My Account');
        $this->layout->layout_view = 'frontend/layout.php';
        $this->layout->view('frontend/myaccount',$data);
    }

    public function save_myaccount() {
        $this->crud_m->isLogin();
        if ($this->input->post()){
            $post_data = $this->input->post();

            /*$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('country_id', 'Country', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|min_length[6]|max_length[6]|numeric');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');*/

            if($post_data['change_password']){
                $this->form_validation->set_rules('user_pass', 'Password', 'trim|required');
                $this->form_validation->set_rules('re_user_pass', 'Repeat password', 'trim|required|matches[user_pass]');

                if (!$this->form_validation->run($this)){
                    $this->myaccount();
                    return false;
                }
            }

            $user_id = $this->session->userdata('user_login_session')['id'];

            if($post_data['change_password']){
                $post_data['user_pass'] = md5($post_data['user_pass']);
                unset($post_data['change_password']);
            }else{
                unset($post_data['user_pass']);
            }

            unset($post_data['id']);
            unset($post_data['email']);
            unset($post_data['re_user_pass']);
            $this->db->where('id', $user_id);
            $this->db->update('t_users', $post_data);
            $this->session->set_flashdata('success_msg', 'Your data has been successfully saved.');
            redirect(base_url('frontend/myaccount'));

        }
        redirect(base_url('frontend/myaccount'));
    }
}

?>
