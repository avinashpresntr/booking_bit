<?php
class Page_m extends Dip_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function select_contactus()
  {
    $query = $this->db->get_where('page',array('page_id'=>'1'));
    return $query->result();
  }

  public function select_termsconditions()
  {
    $query = $this->db->get_where('page',array('page_id'=>'2'));
    return $query->result();
  }

  public function update_page($data)
  {
    date_default_timezone_set('Asia/Kolkata');
    $dt = new DateTime();
    $datetime = $dt->format('Y-m-d H:i:s');
    $this->db->where('page_id',$data['page_id']);
    $this->db->update('page',array('name'=>$data['name'],'content'=>$data['content'],'last_update_time'=>$datetime));
    return true;
  }
}
?>