<?php
class Pricegroup_m extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function addPriceGroup($data)
	{
		if($this->db->insert('t_price_group',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	public function addPricegroupDetaildata($data)
	{
		if($this->db->insert('t_price_group_detail',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}
	
	function getPriceGroupName($pricegroup_id)
	{
		$this->db->select("pricegroup_name");
		$this->db->from('t_price_group');
		$this->db->where('id',$pricegroup_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function checkConflictBlockPrice($course_id, $from_date, $to_date, $from_time, $to_time){
		$this->db->select("id");
		$this->db->from('t_block_detail');
		$this->db->where('course_id',$course_id);
		$this->db->where('pricegroup_id !=','');
		$where = '(block_date>="'.$from_date.'" AND block_date <= "'.$to_date.'")';
       	$this->db->where($where);
       	if(!empty($from_time) && !empty($to_date)){
			$where = '(start_time>="'.$from_time.'" AND start_time < "'.$to_time.'")';
			$this->db->where($where);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}

	function checkBlockDetaildata($course_id, $from_date, $to_date, $from_time, $to_time, $price, $pricegroup_id, $visiteur_pricegroup_id){
		$this->db->select("id");
		$this->db->from('t_block_detail');
		$this->db->where('course_id',$course_id);
		//$this->db->where('pricegroup_id',$pricegroup_id);
		$where = '(block_date>="'.$from_date.'" AND block_date <= "'.$to_date.'")';
       	$this->db->where($where);
       	if(!empty($from_time) && !empty($to_date)){
			$where = '(start_time>="'.$from_time.'" AND start_time < "'.$to_time.'")';
			$this->db->where($where);
		}
		$query = $this->db->get();
       	//echo $this->db->last_query(); exit;

       	$block_info_data['pricegroup_id'] = $pricegroup_id;
        $block_info_data['price'] = $price;
        $block_info_data['offer_price'] = $price;

		if ($query->num_rows() > 0){
			foreach ($query->result_array() as $id){
		        $ids[] = $id['id'];
		        $block_info_data['block_id'] = $id['id'];
		        $this->db->insert('t_block_info',$block_info_data);
		    }
		    $data['price'] = $price;
		    $data['offer_price'] = $price;
		    $data['pricegroup_id'] = $pricegroup_id;
			if($pricegroup_id == $visiteur_pricegroup_id){
				$this->db->where_in('id', $ids);
				$this->db->update("t_block_detail",$data);
				$block_id = $this->db->insert_id();
			}
		}else{
			return false;
		}	
	}

	function checkBlockDetailOfferdata($course_id, $offer_startdate, $offer_enddate, $from_time, $to_time, $offer_price, $pricegroup_id, $visiteur_pricegroup_id){

		if($pricegroup_id == $visiteur_pricegroup_id){
			$this->db->select("id");
			$this->db->from('t_block_detail');
			$this->db->where('course_id',$course_id);
			$this->db->where('pricegroup_id',$pricegroup_id);
			$where = '(block_date>="'.$offer_startdate.'" AND block_date <= "'.$offer_enddate.'")';
			$this->db->where($where);
			if(!empty($from_time) && !empty($to_date)){
				$where = '(start_time>="'.$from_time.'" AND start_time < "'.$to_time.'")';
				$this->db->where($where);
			}
			$query = $this->db->get();
			//echo $this->db->last_query(); exit;
			if ($query->num_rows() > 0){
				foreach ($query->result_array() as $id){
					$ids[] = $id['id'];
				}
				$data['offer_price'] = $offer_price;
				$this->db->where_in('id', $ids);
				$this->db->update("t_block_detail",$data);
			}
		}

		$this->db->select('`t_block_info`.`id`');
		$this->db->from('t_block_info');
		$this->db->join('t_block_detail', 't_block_detail.id = t_block_info.block_id');
		$this->db->where('`t_block_detail`.`course_id`',$course_id);
		$this->db->where('`t_block_info`.`pricegroup_id`',$pricegroup_id);
		$where = '(`t_block_detail`.`block_date`>="'.$offer_startdate.'" AND `t_block_detail`.`block_date` <= "'.$offer_enddate.'")';
       	$this->db->where($where);
		if(!empty($from_time) && !empty($to_date)){
			$where = '(`t_block_detail`.`start_time`>="'.$from_time.'" AND `t_block_detail`.`start_time` < "'.$to_time.'")';
			$this->db->where($where);
		}
       	$query = $this->db->get();
       	//echo $this->db->last_query(); exit;
       	$block_info_data['offer_price'] = $offer_price;
		if ($query->num_rows() > 0){
			foreach ($query->result_array() as $id){
		        $ids[] = $id['id'];
		        $this->db->where_in('id', $ids);
				$this->db->update("t_block_info",$block_info_data);
		    }
		}else{
			return false;
		}	
	}
	
}

