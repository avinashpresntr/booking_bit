<?php
class Calendar_m extends CI_Model{

	public $rules = array(
//			'setting_name' =>  array(
//					'field'=>'setting_name',
//					'label'=>'Setting name',
//					'rules'=>'trim|xss_clean|required'
//			),
			'calendar_setting[0][slot_period]' =>  array(
					'field'=>'calendar_setting[0][slot_period]',
					'label'=>'Slot period',
					'rules'=>'required'
			),
			'calendar_setting[0][slot_player]' =>  array(
					'field'=>'calendar_setting[0][slot_player]',
					'label'=>'Slot players',
					'rules'=>'required'
			),
	);

	function __construct(){
		parent::__construct();
	}

	public function getAllCources($id)
	{
		$this->db->select("id, user_id, name, holes, position");
		$this->db->from('dip_nt_courses');
		$this->db->where('user_id ',$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function checkConflictBlockDetaildata($course_id, $calendar_detail_id, $from_date, $to_date, $from_time, $to_time){
		$this->db->select("id");
		$this->db->from('t_block_detail');
		$this->db->where('course_id',$course_id);
		if($calendar_detail_id != 0){
			$this->db->where('calendar_detail_id !=',$calendar_detail_id);
		}
		$where = '(block_date>="'.$from_date.'" AND block_date <= "'.$to_date.'")';
       	$this->db->where($where);
		$where = '(start_time>="'.$from_time.'" AND start_time < "'.$to_time.'")';
		$this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
 			foreach ($query->result_array() as $id){
 		        $block_detail_ids[] = $id['id'];
 		    }
 		    $this->db->select('id');
 			$this->db->from('t_order_master');
 			$this->db->where_in('block_id',$block_detail_ids);
 			$order_master_query = $this->db->get();
 			if ($order_master_query->num_rows() > 0){
 				return 2;
 				exit;
 			} else {
 				return 1;
 				exit;
 			}
 		}else{
			return 0;
		}
	}

	public function deleteConflictBlockDetaildata($course_id, $from_date, $to_date, $from_time, $to_time){
		$this->db->select("id");
		$this->db->from('t_block_detail');
		$this->db->where('course_id',$course_id);
		$where = '(block_date>="'.$from_date.'" AND block_date <= "'.$to_date.'")';
       	$this->db->where($where);
		$where = '(start_time>="'.$from_time.'" AND start_time < "'.$to_time.'")';
		$this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result_array() as $id){
		        $block_detail_ids[] = $id['id'];
		    }
			if ($block_detail_ids){
				$this->db->where_in('block_id',$block_detail_ids);
				$this->db->delete('t_block_info');
			}
			if($block_detail_ids){
				$this->db->where_in('id',$block_detail_ids);
				$this->db->delete('t_block_detail');
			}
			return 1;
		}else{
			return 0;
		}
	}


	public function addCalendardata($data)
	{
		if($this->db->insert('t_calender',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	public function updateCalendardata($calendar_id, $data)
	{
		$this->db->where('id', $calendar_id);
		if($this->db->update('t_calender',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	public function addCalendarDetaildata($data)
	{
		if($this->db->insert('t_calender_detail',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	public function addBlockDetaildata($data)
	{
		if($this->db->insert('t_block_detail',$data))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	public function deleteCalendarDetaildata($calendar_id)
	{
		$this->db->where('id',$calendar_id);
		$this->db->delete('t_calender_detail');
	}

	public function deleteCalendarDetailBlockDetaildata($calendar_detail_id){
		$this->db->select("id");
		$this->db->from('t_block_detail');
		$this->db->where('calendar_detail_id',$calendar_detail_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result_array() as $id){
		        $block_detail_ids[] = $id['id'];
		    }
			if ($block_detail_ids){
				$this->db->where_in('block_id',$block_detail_ids);
				$this->db->delete('t_block_info');
			}
			if($block_detail_ids){
				$this->db->where_in('id',$block_detail_ids);
				$this->db->delete('t_block_detail');
			}
			return 1;
		}else{
			return 0;
		}
	}

	
	public function deleteCalendarsetting($calendar_id){
		$this->db->select('id');
		$this->db->from('t_calender_detail');
		$this->db->where('calender_id',$calendar_id);
		$query = $this->db->get();
		foreach ($query->result_array() as $calendar_detail_id){
			$calendar_detail_ids[] = $calendar_detail_id['id'];
		}
		$this->db->select('id');
		$this->db->from('t_block_detail');
		$this->db->where('calender_id',$calendar_id);
		$this->db->where_in('calendar_detail_id',$calendar_detail_ids);
		$block_detail_query = $this->db->get();
		foreach ($block_detail_query->result_array() as $block_detail_id){
			$block_detail_ids[] = $block_detail_id['id'];
		}

		$this->db->select('id');
		$this->db->from('t_order_master');
		$this->db->where_in('block_id',$block_detail_ids);
		$order_master_query = $this->db->get();
		if ($order_master_query->num_rows() > 0){
			return 2;
			exit;
		} else {
			if ($block_detail_ids){
				$this->db->where_in('block_id',$block_detail_ids);
				$this->db->delete('t_block_info');

				$this->db->where_in('id',$block_detail_ids);
				$this->db->delete('t_block_detail');
			}
			if($calendar_detail_ids){
				$this->db->where_in('id',$calendar_detail_ids);
				$delete_status1 = $this->db->delete('t_calender_detail');
			}
			$this->db->where('id',$calendar_id);
			$this->db->delete('t_calender');
			return 1;
			exit;
		}
	}

	public function getCalendarsettingData($calender_id){
		$this->db->select('c.setting_name,cd.*');
		$this->db->from('t_calender c');
		$this->db->join('t_calender_detail cd','cd.calender_id = c.id');
		$this->db->where('cd.calender_id',$calender_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function removeCalendarsettingSectiom($section, $calendar_id){
		$this->db->select('id');
		$this->db->from('t_calender_detail');
		$this->db->where('calender_id',$calendar_id);
		$this->db->where('section',$section);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		foreach ($query->result_array() as $calendar_detail_id){
			$calendar_detail_ids[] = $calendar_detail_id['id'];
		}
		$this->db->select('id');
		$this->db->from('t_block_detail');
		$this->db->where('calender_id',$calendar_id);
		$this->db->where_in('calendar_detail_id',$calendar_detail_ids);
		$block_detail_query = $this->db->get();
		foreach ($block_detail_query->result_array() as $block_detail_id){
			$block_detail_ids[] = $block_detail_id['id'];
		}

		$this->db->select('id');
		$this->db->from('t_order_master');
		$this->db->where_in('block_id',$block_detail_ids);
		$order_master_query = $this->db->get();
		if ($order_master_query->num_rows() > 0){
			return 2;
			exit;
		} else {
			if ($block_detail_ids){
				$this->db->where_in('block_id',$block_detail_ids);
				$this->db->delete('t_block_info');

				$this->db->where_in('id',$block_detail_ids);
				$this->db->delete('t_block_detail');
			}
			if($calendar_detail_ids){
				$this->db->where_in('id',$calendar_detail_ids);
				$delete_status1 = $this->db->delete('t_calender_detail');
			}
			return 1;
			exit;
		}
	}

	/*public function getCourseAllDataByCalenderId($id)
	{
		$this->db->select("t_calender_detail.*,dip_nt_courses.holes");
		$this->db->from('t_calender_detail');
		$this->db->join('dip_nt_courses', 't_calender_detail.course_id = dip_nt_courses.id', 'left');
		$this->db->where('t_calender_detail.calender_id ',$id);
		$this->db->where('t_calender_detail.course_id ',59);
		$this->db->where('t_calender_detail.section ',1);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}*/
	public function getCourseAllDataByCalenderId($booking_date,$booking_course_id)
	{
		$this->db->select("t_calender_detail.*,dip_nt_courses.holes");
		$this->db->from('t_calender_detail');
		$this->db->join('dip_nt_courses', 't_calender_detail.course_id = dip_nt_courses.id', 'left');
		$this->db->where('t_calender_detail.booking_startdate >=', $booking_date);
		//$this->db->where('t_calender_detail.booking_enddate <=', $booking_date);
		$this->db->where('t_calender_detail.course_id ',$booking_course_id);
		$this->db->order_by('t_calender_detail.id','desc');
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getBlockPriceFromPriceGroup($startdate,$enddate,$start_time,$end_time,$slot_period,$course_id)
	{
		$this->db->select("*");
		$this->db->from('t_price_group_detail');
		$this->db->where('course_id ',$course_id);
		$this->db->where('from_date >=', $startdate);
		$this->db->where('to_date <=', $enddate);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			$result =  $query->result_array();
			return $result[0]['price'];
		}else{
			return 0;
		}
	}
	public function getSpecificBlockPrice($startdate, $enddate, $slot_start_time_format, $slot_end_time_format, $block_start_time_format, $block_end_time_format,$course_id)
	{
		$this->db->select("*");
		$this->db->from('t_price_group_detail');
		$this->db->where('course_id ',$course_id);
		$this->db->where('from_date >=', $startdate);
		$this->db->where('to_date <=', $enddate);
		$this->db->where('timeslot_starttime >=', $slot_start_time_format);
		$this->db->where('timeslot_endtime <=', $slot_end_time_format);
		$this->db->where('timeblock_starttime >=', $block_start_time_format);
		$this->db->where('timeblock_endtime <=', $block_end_time_format);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
			//return $result[0]['price'];
		}else{
			return 0;
		}
	}
	public function getSpecificBlockPrice_new($startdate,$block_start_time_format, $block_end_time_format,$course_id)
	{
		$this->db->select("*");
		$this->db->from('t_block_detail');
		$this->db->where('course_id ',$course_id);
		$this->db->where('block_date', $startdate);
		$this->db->where('start_time', $block_start_time_format);
		//$this->db->where('end_time <=', $block_end_time_format);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
			//return $result[0]['price'];
		}else{
			return 0;
		}
	}
	public function getBlockDetailData($booking_date,$booking_course_id)
	{
		//$this->db->select("t_block_detail.*,dip_nt_courses.holes,COUNT(t_order_master.block_id) as total_booked_players, t_order_master.id as order_master_id");
		$this->db->select("t_block_detail.*,dip_nt_courses.holes,COUNT(t_order_player_detail.order_master_id) as total_booked_players, t_order_master.id as order_master_id");
		//$this->db->select("t_block_detail.*,dip_nt_courses.holes");
		$this->db->from('t_block_detail');
		$this->db->join('dip_nt_courses', 't_block_detail.course_id = dip_nt_courses.id', 'left');
		$this->db->join('t_order_master', 't_block_detail.id = t_order_master.block_id', 'left');

		$this->db->join('t_order_player_detail', 't_order_master.id = t_order_player_detail.order_master_id', 'left');

		$this->db->where('t_block_detail.block_date', $booking_date);
		$this->db->where('t_block_detail.course_id ',$booking_course_id);
		$this->db->group_by('t_block_detail.id');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getBlockDetailDataById($id)
	{
		$this->db->select("t_block_detail.block_date,t_block_detail.start_time,t_block_detail.course_id");
		$this->db->from('t_block_detail');
		$this->db->where('t_block_detail.id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getOrderDetailDataById($id)
	{
		$this->db->select("*");
		$this->db->from('t_order_master');
		$this->db->where('id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getPlayerOrderDetailData($id)
	{
		$this->db->select("*");
		$this->db->from('t_order_player_detail');
		$this->db->where('order_master_id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getAllCountry()
	{
		$this->db->select("country_name");
		$this->db->from('dip_countries');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getBlockDataWiseCourseID($course_id)
	{
		$this->db->select("*");
		$this->db->from('t_block_detail');
		$this->db->where('course_id ',$course_id);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result_array();
			//return $result[0]['price'];
		}else{
			return 0;
		}
	}

	public function get_courses_list_by_user_lang($languageID, $user_id)
	{
		$this->db->select('*');
		$this->db->from('dip_nt_courses');
		$this->db->where('user_id ',$user_id);
		$query = $this->db->get();
		$rows = $query->result_array();
		$records = array();

		$i = 0;
		foreach($rows as $row)
		{
			$name = "";
			$tmp = $row['name'];

			if(!empty($tmp))
			{
				$arr = json_decode($tmp,1);
				$name = isset($arr[$languageID]) ? $arr[$languageID]:'';
			}

			if(trim($name) != "")
			{
				$records[$i]['id'] = $row['id'];
				$records[$i]['name'] = $name;

				$i++;
			}
		}

		return $records;
	}

	public function getBlockBookingDetailData($block_id)
	{
		$this->db->select("t_order_player_detail.*");
		$this->db->from('t_order_player_detail');
		$this->db->join('t_order_master', 't_order_master.id = t_order_player_detail.order_master_id', 'left');
		//$this->db->join('t_order_master', 't_block_detail.id = t_order_master.block_id', 'left');
		$this->db->where('t_order_master.block_id', $block_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getBlockOptionData($block_id)
	{
		$returndata = array();
		$this->db->select("id,pricegroup_name");
		$this->db->from('t_price_group');
		$this->db->where('parent_id', 3);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$data =  $query->result_array();
			foreach($data as $k=>$v)
			{
				$name = json_decode($v['pricegroup_name']);
				$returndata[$k]['id'] = $v['id'];
				$returndata[$k]['optioin_name'] = $name->{3};
			}
			foreach($returndata as $k=>$v)
			{
				$returndata[$k]['option_price'] = 0;
				$returndata[$k]['option_offer_price'] = 0;
				$option_price_data = $this->getOptionPriceData($v['id'],$block_id);
				if(is_array($option_price_data))
				{
					$returndata[$k]['option_price'] = $option_price_data[0]['price'];
					$returndata[$k]['option_offer_price'] = $option_price_data[0]['offer_price'];
				}
				/*$option_qty_data = $this->getOptionQtyData($v['id']);
				if(is_array($option_price_data))
				{
					$returndata[$k]['total_qty'] = $option_qty_data[0]['option_qty'];
				}*/
				$option_booking_data = $this->getOptionBookingData($v['id'],$block_id);
				if(is_array($option_price_data))
				{
					$returndata[$k]['total_booking_qty'] = $option_booking_data[0]['total_booking_qty'];
				}
			}
		}else{
			return false;
		}
		return $returndata;
	}
	public function getOptionPriceData($id,$block_id)
	{
		//$this->db->select("t_block_info.price,t_block_info.offer_price,COUNT(t_order_options.qty) as total_booked_options");
		$this->db->select("t_block_info.price,t_block_info.offer_price");
		$this->db->from('t_block_info');
		$this->db->where('t_block_info.block_id', $block_id);
		$this->db->where('t_block_info.pricegroup_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			//echo "<pre>";print_r($query->result_array());exit;
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getOptionBookingData($id,$block_id)
	{
		$this->db->select("COUNT(t_order_options.qty) as total_booking_qty");
		$this->db->from('t_order_options');
		$this->db->where('t_order_options.block_id', $block_id);
		$this->db->where('t_order_options.pricegroup_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			//echo "<pre>";print_r($query->result_array());exit;
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getOptionQtyData($id)
	{
		$this->db->select("option_qty");
		$this->db->from('t_price_group_detail');
		$this->db->where('pricegroup_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			//echo "<pre>";print_r($query->result_array());exit;
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getBlockMemberData($block_id)
	{
		$returndata = array();
		$this->db->select("*");
		$this->db->from('t_price_group');
		$this->db->where('parent_id !=', $this->config->item('options_parent_id'));
		$this->db->where('id !=', $this->config->item('options_parent_id'));
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$data =  $query->result_array();
			foreach($data as $k=>$v)
			{
				if($v['parent_id'] == 0){
					$name = json_decode($v['pricegroup_name']);
					$returndata[$k]['id'] = $v['id'];
					$returndata[$k]['parent_id'] = $v['parent_id'];
					$returndata[$k]['member_name'] = $name->{3};

					foreach($data as $child_k=>$child_v)
					{
						if($child_v['parent_id'] == $v['id']){
							$chid_name = json_decode($child_v['pricegroup_name']);
							$returndata[$k]['member_option_data'][$child_k]['id'] = $child_v['id'];
							$returndata[$k]['member_option_data'][$child_k]['parent_id'] = $child_v['parent_id'];
							$returndata[$k]['member_option_data'][$child_k]['member_name'] = $chid_name->{3};

							$member_price_data = $this->getMemberPriceData($child_v['id'],$block_id);

							if(is_array($member_price_data))
							{
								$returndata[$k]['member_option_data'][$child_k]['option_price'] = $member_price_data[0]['price'];
								$returndata[$k]['member_option_data'][$child_k]['option_offer_price'] = $member_price_data[0]['offer_price'];
							}
						}
					}
				}

			}
		}else{
			return false;
		}
		return $returndata;
	}
	public function getMemberPriceData($id,$block_id)
	{
		$this->db->select("t_block_info.price,t_block_info.offer_price");
		$this->db->from('t_block_info');
		$this->db->where('t_block_info.block_id', $block_id);
		$this->db->where('t_block_info.pricegroup_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	public function getPriceGroupName($id)
	{
		$this->db->select("*");
		$this->db->from('t_price_group');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$data =  $query->result_array();
			foreach($data as $k=>$v)
			{
				$name = json_decode($v['pricegroup_name']);
				return $name->{3};
			}
		}else{
			return false;
		}
	}
	public function getOptionOrderDetailData($id)
	{
		$this->db->select("*");
		$this->db->from('t_order_options');
		$this->db->where('order_master_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$returndata = array();
			$data =  $query->result_array();
			foreach($data as $k=>$v)
			{
				$returndata[$k] = $v;
				$returndata[$k]['option_name'] = $this->getPriceGroupName($v['pricegroup_id']);
			}
			return $returndata;
		}else{
			return false;
		}
	}
}
