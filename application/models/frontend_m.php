<?php
class Frontend_m extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	public function get_ntcourse_array_list_by_lang($languageID, $user_id)
	{
		$this->db->select('*');
		$this->db->from('dip_nt_courses');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		$rows = $query->result_array();
		$records = array();

		$i = 0;
		foreach($rows as $row)
		{
			$name = "";
			$tmp = $row['name'];

			if(!empty($tmp))
			{
				$arr = json_decode($tmp,1);
				$name = isset($arr[$languageID]) ? $arr[$languageID]:'';
			}

			if(trim($name) != "")
			{
				$records[$i]['id'] = $row['id'];
				$records[$i]['name'] = $name;

				$i++;
			}
		}

		return $records;
	}

	public function getCurrentDateLowestPrice($date)
	{
		$this->db->select('LEAST(MIN(price), MIN(offer_price)) as lowest_price');
		$this->db->from('t_block_detail');
		$this->db->where('block_date', $date);
		$this->db->group_by('block_date');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			if($query->row('lowest_price') == null){
				return 0;
			}else {
				return $query->row('lowest_price');
			}
		}else{
			return 0;
		}
	}

	function authenticate($username = NULL, $password = NULL)
	{
		$conditions = array("email"=>$username,"user_pass"=>md5($password),'user_type'=>'user');
		$this->db->select('*')->from('t_users')->where($conditions);
		$query = $this->db->get();

		if ($query->num_rows() == 1){
			return $query->result_array()[0];
		} else {
			return 0;
		}
		//return false;
	}
	
}
