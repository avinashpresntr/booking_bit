<?php
class Crud_m extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function get_column_value_by_id($tbl_name,$column_name,$where_array)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_array);
		$query = $this->db->get();
		return $query->row($column_name);
	}

	function get_all_records($table_name,$order_by_column='',$order_by_value=''){
		$this->db->select("*");
		$this->db->from($table_name);
		if($order_by_column !='' && $order_by_value !='') {
			$this->db->order_by($order_by_column, $order_by_value);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function get_all_records_with_condition($table_name, $where_array, $order_by_column='', $order_by_value=''){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		if($order_by_column !='' && $order_by_value !='') {
			$this->db->order_by($order_by_column, $order_by_value);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function get_selected_all_records($table_name,$column, $order_by_column='',$order_by_value=''){
		$this->db->select($column); /* comma separated column list */
		$this->db->from($table_name);
		if($order_by_column !='' && $order_by_value !='') {
			$this->db->order_by($order_by_column, $order_by_value);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function isLogin()
	{
		if(!$this->session->userdata('user_login_session')){
			redirect(base_url('presntr/'.$this->session->userdata('site_user_id')));
		}
		return true;
	}
}
