-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2016 at 09:12 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `booking_bit`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_contact_us`
--

CREATE TABLE IF NOT EXISTS `t_contact_us` (
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_contact_us`
--

INSERT INTO `t_contact_us` (`id`, `title`, `content`, `updated_at`) VALUES
(0, 'CONTACT US', '<p>CONTACT US&nbsp;CONTENTS</p>\r\n', '2016-11-11 08:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `t_payment_modes`
--

CREATE TABLE IF NOT EXISTS `t_payment_modes` (
  `payment_mode_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mode` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-Active 2-Deactive',
  `payment_email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_mode_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_payment_modes`
--

INSERT INTO `t_payment_modes` (`payment_mode_id`, `payment_mode`, `is_active`, `payment_email`, `created_at`, `updated_at`) VALUES
(1, '', 1, 'cshailesh157@gmail.com', '2016-11-11 12:35:27', '2016-11-11 08:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `t_terms_condition`
--

CREATE TABLE IF NOT EXISTS `t_terms_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_terms_condition`
--

INSERT INTO `t_terms_condition` (`id`, `title`, `content`, `updated_at`) VALUES
(1, 'TERMS & CONDITIONS', '<p>TERMS &amp; CONDITIONS CONTENTS</p>\r\n', '2016-11-11 08:11:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
